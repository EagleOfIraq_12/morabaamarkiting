package com.morabaa.team.morabaamarkiting.activities;

import static android.content.ContentValues.TAG;
import static com.google.firebase.auth.FirebaseAuth.getInstance;

import android.Manifest.permission;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.model.User;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends AppCompatActivity {
      
      
      ProgressDialog progress;
      EditText txtPhoneNumberEdit;
      Button btnSendPhoneNumber;
      EditText txtConfirmToken;
      LinearLayout btnConfirm;
      String verificationId;
      private FirebaseAuth mAuth;
      private String accountUID;
      private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
      private boolean mVerificationInProgress;
      private PhoneAuthProvider.ForceResendingToken mResendToken;
      private LinearLayout content_phone_auth_confirm;
      private LinearLayout content_phone_auth;
//    private LinearLayout content_user_details;
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            //region views
            mAuth = getInstance();
            // mAuth.setLanguageCode("iq");
            //    mAuth.useAppLanguage();
            
            txtPhoneNumberEdit = findViewById(R.id.txtPhoneNumberEdit);
            
            if (ActivityCompat
                  .checkSelfPermission(LoginActivity.this,
                        permission.ACCESS_FINE_LOCATION)
                  != PackageManager.PERMISSION_GRANTED && ActivityCompat
                  .checkSelfPermission(LoginActivity.this,
                        permission.ACCESS_COARSE_LOCATION)
                  != PackageManager.PERMISSION_GRANTED) {
                  // TODO: Consider calling
                  //    ActivityCompat#requestPermissions
                  // here to request the missing permissions, and then overriding
                  //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                  //                                          int[] grantResults)
                  // to handle the case where the user grants the permission. See the documentation
                  // for ActivityCompat#requestPermissions for more details.
//                  return;
            }
            
            content_phone_auth_confirm = findViewById(
                  R.id.content_phone_auth_confirm);
//        content_user_details = findViewById(R.id.content_user_details);
            content_phone_auth = findViewById(R.id.content_phone_auth);
            btnSendPhoneNumber = findViewById(R.id.btnSendPhoneNumber);
            txtConfirmToken = findViewById(R.id.txtConfirmToken);
            btnConfirm = findViewById(R.id.btnConfirm);
            
            if (!getIntent().getExtras().getBoolean("TAG")) {
                  content_phone_auth_confirm.setVisibility(View.GONE);
                  content_phone_auth.setVisibility(View.GONE);
                  Intent userIntent = new Intent(LoginActivity.this, EditUserDataActivity.class);
                  User user = User.getCurrentUser(getApplication());
                  user.setPhoneNumber(txtPhoneNumberEdit.getText().toString());
                  String userString = new Gson().toJson(user, User.class);
                  User.saveLoginState(LoginActivity.this, user);
                  userIntent.putExtra("userString", userString);
                  startActivity(userIntent);
                  finish();
            } else if (getIntent().getExtras().getBoolean("TAG")) {
                  content_phone_auth_confirm.setVisibility(View.GONE);
                  content_phone_auth.setVisibility(View.VISIBLE);
                  mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                        
                        @Override
                        public void onVerificationCompleted(PhoneAuthCredential credential) {
                              // This callback will be invoked in two situations:
                              // 1 - Instant verification. In some cases the phone number can be instantly
                              //     verified without needing to send or enter a verification code.
                              // 2 - Auto-retrieval. On some devices Google Play services can automatically
                              //     detect the incoming verification SMS and perform verificaiton without
                              //     user action.
                              Log.d(TAG, "onVerificationCompleted:" + credential);
                              
                              signInWithPhoneAuthCredential(credential);
                        }
                        
                        @Override
                        public void onVerificationFailed(FirebaseException e) {
                              // This callback is invoked in an invalid request for verification is made,
                              // for instance if the the phone number format is not valid.
                              Log.w(TAG, "onVerificationFailed", e);
                              Toast.makeText(getApplicationContext(), "يرجى ادخال رقم هاتف صالح",
                                    Toast.LENGTH_SHORT).show();
                              
                              if (e instanceof FirebaseAuthInvalidCredentialsException) {
                                    // Invalid request
                                    // ...
                              } else if (e instanceof FirebaseTooManyRequestsException) {
                                    // The SMS quota for the project has been exceeded
                                    // ...
                              }
                              
                              // Show a message and update the UI
                              // ...
                        }
                        
                        @Override
                        public void onCodeSent(String vId,
                              PhoneAuthProvider.ForceResendingToken token) {
                              // The SMS verification code has been sent to the provided phone number, we
                              // now need to ask the user to enter the code and then construct a credential
                              // by combining the code with a verification ID.
                              Log.d(TAG, "onCodeSent:" + vId);
                              // Save verification ID and resending token so we can use them later
                              verificationId = vId;
                              Toast.makeText(getApplicationContext(), "الية التاكيد verificationId",
                                    Toast.LENGTH_LONG).show();
                              content_phone_auth_confirm.setVisibility(View.VISIBLE);
                              progress.dismiss();
                              content_phone_auth.setVisibility(View.GONE);
                              mResendToken = token;
                        }
                  };
            }
            
      }
      
      private void verifyPhoneNumberWithCode(String verificationId, String code) {
            // [START verify_with_code]
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
            // [END verify_with_code]
            signInWithPhoneAuthCredential(credential);
      }
      
      private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
            mAuth.signInWithCredential(credential)
                  .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                              if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    Log.d(TAG, "signInWithCredential:success");
                                    FirebaseUser user = task.getResult().getUser();
//                                    Toast.makeText(getBaseContext(), user.getUid() + "",
//                                          Toast.LENGTH_SHORT).show();
//                                    accountUID = String.valueOf(user.getUid());
                                    
                                    // send user data to api
                                    userLoginData();
                                    // ...
                              } else {
                                    progress.dismiss();
                                    Toast.makeText(getBaseContext(),
                                          "خطاء في المصادقة حاول مرة اخرى", Toast.LENGTH_SHORT)
                                          .show();
                                    // Sign in failed, display a message and update the UI
                                    Log.w(TAG, "signInWithCredential:failure", task.getException());
                                    if (task
                                          .getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                          // The verification code entered was invalid
                                          Toast.makeText(getBaseContext(),
                                                "رمز التاكيد الذي ادخلته خاطئ يرجى التاكد",
                                                Toast.LENGTH_SHORT).show();
                                          
                                    }
                              }
                        }
                  });
      }
      
      private void userLoginData() {
            final User user
                  = new User();
            user.setPhoneNumber(txtPhoneNumberEdit.getText().toString());
            new HttpRequest<User>(this, API.USER.LOGIN, new Gson().toJson(user, User.class),
                  new TypeToken<List<User>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<User> users) {
                        User user = users.get(0);
                        progress.dismiss();
                        if (user != null) {
                              user.setPhoneNumber(txtPhoneNumberEdit.getText().toString());
                              Intent userIntent = new Intent(LoginActivity.this,
                                    EditUserDataActivity.class);
                              User.saveLoginState(LoginActivity.this, user);
                              userIntent
                                    .putExtra("userString", new Gson().toJson(user, User.class));
                              startActivity(userIntent);
                              finish();
                        }
                  }
                  
                  @Override
                  public void onError(String error) {
                        progress.dismiss();
                        System.out.println("LogIn Error: " + error);
                        Toast.makeText(getBaseContext(),
                              "حدث خطأ في عملية التسجيل \n حاول مره اخرى ", Toast.LENGTH_SHORT)
                              .show();
                  }
            };
      }
      
      
      private void startPhoneNumberVerification(String phoneNumber) {
            // [START start_phone_auth]
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                  phoneNumber,        // Phone number to verify
                  60,                 // Timeout duration
                  TimeUnit.SECONDS,   // Unit of timeout
                  this,               // Activity (for callback binding)
                  mCallbacks);        // OnVerificationStateChangedCallbacks
            // [END start_phone_auth]
            
            mVerificationInProgress = true;
      }
      
      public void sendPhNumber(View view) {
            try {
                  progress = ProgressDialog
                        .show(new ContextThemeWrapper(this, R.style.progressDialog), "تسجيل الدخول",
                              "يرجى الانتظار قليلاً ....", true);
                  progress.setCancelable(true);
//                  Toast.makeText(this, SV.CURRENT_LOCATION, Toast.LENGTH_SHORT).show();
                  int numLength = txtPhoneNumberEdit.getText().toString().length();
                  String num = "";
                  if (numLength > 9) {
                        num = txtPhoneNumberEdit.getText().toString()
                              .substring(numLength - 10, numLength);
                        num = "+964" + num;
                        startPhoneNumberVerification(num);
                  } else {
                        Toast.makeText(this, " صيغة رقم الهاتف غير صحيحه ",
                              Toast.LENGTH_SHORT)
                              .show();
                        
                  }
                  
            } catch (Exception e) {
                  e.printStackTrace();
            }
      }
      
      public void confirmLogin(View view) {
            progress = ProgressDialog
                  .show(new ContextThemeWrapper(this, R.style.progressDialog), "تأكيد الدخول",
                        "يرجى الانتظار قليلاً ....", true);
            progress.setCancelable(true);
            verifyPhoneNumberWithCode(verificationId, txtConfirmToken.getText().toString());
            content_phone_auth.setVisibility(View.GONE);
            progress.dismiss();
      }
      
      @Override
      public void onBackPressed() {
            super.onBackPressed();
            finish();
      }
      
}