package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;

@Entity
public class ImageUrl {
      
      public static final int USER = 1;
      public static final int SHOP = 2;
      public static final int ITEM = 3;
      public static final int CATEGORY = 4;
      public static final int AD = 5;
      public static final int OFFER = 6;
      
      @PrimaryKey
      private long id;
      private String url;
      private boolean main;
      private long itemId;
      private long idOfType;
      private int type;
      @Ignore
      private byte[] image;
      
      public ImageUrl() {
      }
      
      public int getType() {
            return type;
      }
      
      public void setType(int type) {
            this.type = type;
      }
      
      public long getIdOfType() {
            return idOfType;
      }
      
      public void setIdOfType(long idOfType) {
            this.idOfType = idOfType;
      }
      
      public byte[] getImage() {
            return image;
      }
      
      public void setImage(byte[] image) {
            this.image = image;
      }
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public String getUrl() {
            return url;
      }
      
      public void setUrl(String url) {
            this.url = url;
      }
      
      public boolean isMain() {
            return main;
      }
      
      public void setMain(boolean main) {
            this.main = main;
      }
      
      public long getItemId() {
            return itemId;
      }
      
      public void setItemId(long itemId) {
            this.itemId = itemId;
      }
      
      @Dao
      public interface ImageUrlDao {
            
            @Query("SELECT * FROM ImageUrl")
            List<ImageUrl> IMAGE_URLS();
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insert(ImageUrl... imageUrls);
            
            
            @Delete
            void delete(ImageUrl... imageUrls);
            
            @Update
            void Update(ImageUrl... imageUrls);
            
      }
}
