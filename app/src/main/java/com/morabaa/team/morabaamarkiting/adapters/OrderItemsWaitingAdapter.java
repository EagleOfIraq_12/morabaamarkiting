package com.morabaa.team.morabaamarkiting.adapters;

import static android.support.v4.content.ContextCompat.startActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.activities.ItemDetailsActivity;
import com.morabaa.team.morabaamarkiting.model.Item;
import com.morabaa.team.morabaamarkiting.model.Order;
import com.morabaa.team.morabaamarkiting.model.OrderItem;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.API.CART;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.morabaa.team.morabaamarkiting.utils.SV.ScreenDimensions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eagle on 1/22/2018.
 */

public class OrderItemsWaitingAdapter extends
        RecyclerView.Adapter<OrderItemsWaitingViewHolder> {
    int status;
    private List<OrderItem> orderItems;

    private Context ctx;
    private Activity activity;
    private OnOrderItemsCountChangeListener onOrderItemsCountChangeListener;

    public void setOnOrderItemsCountChangeListener(OnOrderItemsCountChangeListener onOrderItemsCountChangeListener) {
        this.onOrderItemsCountChangeListener = onOrderItemsCountChangeListener;
    }

    public OrderItemsWaitingAdapter(Context ctx, List<OrderItem> orderItems,
                                    Activity activity, int status) {
        this.orderItems = orderItems;
        this.ctx = ctx;
        this.activity = activity;
        this.status = status;
    }

    @Override
    public OrderItemsWaitingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.order_item_layout_wide, parent, false);
        return new OrderItemsWaitingViewHolder(view);
    }

    @SuppressLint({"StaticFieldLeak", "SetTextI18n"})
    @Override
    public void onBindViewHolder(final OrderItemsWaitingViewHolder holder, final int position) {

        int w = ScreenDimensions.getWidth(activity);
        holder.imgItemImage.getLayoutParams().height = w / 5;

        holder.view.getLayoutParams().width = w;

        holder.view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx,
                      ItemDetailsActivity.class);
                intent.putExtra("itemId", orderItems.get(position).getItem().getId());
                intent.putExtra("itemString",
                      new Gson().toJson(orderItems.get(position).getItem(), Item.class));
                intent.putExtra("imageUrl", orderItems.get(position).getItem().getImageUrl());
                intent.putExtra("itemNameHeader", orderItems.get(position).getItem().getName());
    
                ImageView imageView = holder.imgItemImage;
                TextView textView = holder.txtItemName;
                List<Pair<View, String>> pairs = new ArrayList<>();
    
                pairs.add(new Pair<View, String>(textView,
                      ItemDetailsActivity.VIEW_NAME_HEADER_TITLE));
                pairs.add(new Pair<View, String>(imageView,
                      ItemDetailsActivity.VIEW_NAME_HEADER_IMAGE));
    
                pairs.add(new Pair<View, String>(holder.txtItemPrice,
                      ItemDetailsActivity.VIEW_PRICE_TEXT));
    
                pairs.add(new Pair<View, String>(holder.txtItemPrice,
                      ItemDetailsActivity.VIEW_DISCOUNT_PRICE_TEXT));
                pairs.add(new Pair<View, String>(holder.txtItemPrice,
                      ItemDetailsActivity.VIEW_RATING));
    
                Pair<View, String>[] pairs1 = new Pair[pairs.size()];
                for (int i = 0; i < pairs.size(); i++) {
                    pairs1[i] = pairs.get(i);
                }
                ActivityOptionsCompat activityOptions = ActivityOptionsCompat
                      .makeSceneTransitionAnimation(
                            activity, pairs1
                      );
                startActivity(ctx, intent, activityOptions.toBundle());
//                        startDetailActivity(dataSet.get(position).getId()+"",holder.view);
//                Intent intent = new Intent(ctx,
//                        ItemDetailsActivity.class);
//                intent.putExtra("itemId", orderItems.get(position).getItem().getId());
//                ctx.startActivity(intent);

            }
        });

        holder.txtItemName.setText(orderItems.get(position).getItem().getName());
        holder.txtItemDescription.setText(orderItems.get(position).getItem().getDescription());
        holder.txtItemDescription.setVisibility(View.GONE);
        holder.txtItemPrice
                .setText(orderItems.get(position).getItem().getPrice() + " د.ع");

        holder.txtItemCount.setText(orderItems.get(position).getCount() + " قطعه");
//        holder.txtNote.setText(orderItems.get(position).getNotes());

        holder.btnCartPosAction.setText("تأكيد");

        holder.btnCartPosAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                new HttpRequest<OrderItem>(ctx,
                        CART.CONFIRM_ORDER + orderItems.get(position).getId(), "") {
                    @Override
                    public void onFinish(List<OrderItem> c) {//
                        if (orderItems.size() != 0) {
                            orderItems.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, orderItems.size());
                            onOrderItemsCountChangeListener
                                    .onOrderItemsCountChange(orderItems.size());
                        }
                    }

                    @Override
                    public void onError(String error) {

                    }
                };
            }
        });


        Picasso.with(ctx)
                .load(API.IMG_ROOT + orderItems.get(position).getItem().getImageUrl())
                .placeholder(ctx.getResources().getDrawable(R.drawable.default_item_image))
                .error(ctx.getResources().getDrawable(R.drawable.error_item_image))
                .into(holder.imgItemImage);
        switch (status) {
            case Order.Status.WAITING: {
                holder.btnCartNegAction.setText("إلغاء");
                holder.btnCartNegAction.setVisibility(View.VISIBLE);
                holder.btnCartNegAction.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new HttpRequest<OrderItem>(ctx,
                                API.ORDER.REMOVE_ORDER_ITEM(orderItems.get(position).getId()), "") {
                            @Override
                            public void onFinish(List<OrderItem> c) {
                                if (orderItems.size() != 0) {
                                    orderItems.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, orderItems.size());
                                    onOrderItemsCountChangeListener
                                            .onOrderItemsCountChange(orderItems.size());
                                }
                            }

                            @Override
                            public void onError(String error) {

                            }
                        };
                    }
                });


                holder.btnCartPosAction.setVisibility(View.GONE);
                break;
            }
            case Order.Status.NOT_DELIVERED: {
                holder.btnCartNegAction.setVisibility(View.GONE);
                holder.btnCartPosAction.setVisibility(View.GONE);
                break;
            }
            case Order.Status.DELIVERED: {
                holder.btnCartNegAction.setVisibility(View.GONE);
                holder.btnCartPosAction.setVisibility(View.GONE);
                break;
            }

        }
    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
//            notifyAll();
        notifyDataSetChanged();
    }

    public interface OnOrderItemsCountChangeListener {
        void onOrderItemsCountChange(int ordersCount);
    }

    public void removeOrderItem(OrderItem cartItem) {
        if (orderItems.size() != 0) {
            orderItems.remove(cartItem);
            notifyDataSetChanged();
            onOrderItemsCountChangeListener
                    .onOrderItemsCountChange(orderItems.size());
        }
    }
}

class OrderItemsWaitingViewHolder extends RecyclerView.ViewHolder {

    CardView view;
    TextView txtItemCount;
    Button btnCartPosAction;
    Button btnCartNegAction;
    TextView txtNote;
    ImageView imgItemImage;
    TextView txtItemName;
    TextView txtItemDescription;
    TextView txtItemPrice;


    public OrderItemsWaitingViewHolder(View itemView) {
        super(itemView);
        view = (CardView) itemView;
        imgItemImage = itemView.findViewById(R.id.imgItemImage);
        txtItemCount = itemView.findViewById(R.id.txtItemCount);
        btnCartPosAction = itemView.findViewById(R.id.btnCartPosAction);
        btnCartNegAction = itemView.findViewById(R.id.btnCartNegAction);
        txtNote = itemView.findViewById(R.id.txtNote);
        imgItemImage = itemView.findViewById(R.id.imgItemImage);
        txtItemName = itemView.findViewById(R.id.txtItemName);
        txtItemDescription = itemView.findViewById(R.id.txtItemDescription);
        txtItemPrice = itemView.findViewById(R.id.txtItemPrice);

    }
}