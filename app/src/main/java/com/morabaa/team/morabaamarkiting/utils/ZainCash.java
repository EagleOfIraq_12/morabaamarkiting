package com.morabaa.team.morabaamarkiting.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class ZainCash {


    public static void pay(final Context ctx, long amount, long msisdn, long orderId, String serviceType,
                           String merchantId, String secret) {

        Map<String, Object> data = new HashMap<>();
        data.put("amount", amount);
        data.put("msisdn", msisdn);
        data.put("orderId", orderId);
        data.put("redirectUrl", "https://morabaa.com/");
        data.put("iat", new Date().getTime());
        data.put("exp", new Date().getTime() + 60 * 60 * 4);
        data.put("serviceType", serviceType);

        String newToken = generateJWT(secret, data);

        System.out.println("newToken: " + newToken);

        Map<String, String> parms = new HashMap<>();

        parms.put("token", newToken);
        parms.put("merchantId", merchantId);
        parms.put("lang", "ar");

        new ZainHttpRequest(
                "https://api.zaincash.iq/transaction/init", "", parms) {
            @Override
            public void onFinish(String response) {
                System.out.println("response: " + response);
                String id;
                try {
                    JSONObject object = new JSONObject(response);
                    id = object.getString("id");
                    String newUrl =
                            "https://api.zaincash.iq/transaction/pay?id=" + id;
                    System.out.println("url :" + newUrl);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(newUrl));
                    ctx.startActivity(intent);
                } catch (JSONException ignored) {

                }

            }


            @Override
            public void onError(String error) {
                System.out.println("Error: " + error);
            }
        };


    }

    static String generateJWT(String secret, Map<String, Object> data) {
        secret = new String(Base64.encodeBase64(secret.getBytes()));

        return Jwts.builder()
                .addClaims(data)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    abstract static class ZainHttpRequest {

        String url;
        String jsonBodyString;
        Map<String, String> parms;

        public ZainHttpRequest(String url, String jsonBodyString,
                               Map<String, String> parms) {
            this.url = url;
            this.jsonBodyString = jsonBodyString;
            this.parms = parms;
            postToServer();

        }

        private void postToServer() {
            new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();
                            okhttp3.Request.Builder builder = new okhttp3.Request.Builder()
                                    .url(url);
                            if (parms != null) {
                                okhttp3.FormBody.Builder postData = new okhttp3.FormBody.Builder();
                                for (String key : parms.keySet()) {
                                    postData.add(key, parms.get(key));
                                }
                                builder.post(postData.build());
                            }
                            okhttp3.Request request = builder.build();
                            okhttp3.Response response;
                            try {
                                response = client.newCall(request).execute();
                                if (!response.isSuccessful()) {
                                    onError(response.message() + " " + response.toString());
                                } else {
                                    if (response.body() != null) {
                                        onFinish(response.body().string());
                                    }
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ).start();
        }

        public abstract void onFinish(String response);

        public abstract void onError(String error);

    }

}
