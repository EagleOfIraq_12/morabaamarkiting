package com.morabaa.team.morabaamarkiting.adapters;

/**
 * Created by eagle on 1/22/2018.
 */

import static android.support.v4.content.ContextCompat.startActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.activities.ItemDetailsActivity;
import com.morabaa.team.morabaamarkiting.activities.OfferDetailsActivity;
import com.morabaa.team.morabaamarkiting.activities.ShopDetailsActivity;
import com.morabaa.team.morabaamarkiting.model.Ad;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;


public class AdsAdapter extends PagerAdapter {
      
      private Context ctx;
      private LayoutInflater layoutInflater;
      private List<Ad> ads;
      
      
      public AdsAdapter(Context ctx, List<Ad> ads) {
            this.ctx = ctx;
            this.ads = ads;
            layoutInflater = (LayoutInflater) this.ctx
                  .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            
      }
      
      @Override
      public int getCount() {
            return ads.size();
      }
      
      @Override
      public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
      }
      
      @NonNull
      @Override
      public Object instantiateItem(@NonNull ViewGroup container, final int position) {
            @SuppressLint("InflateParams") final CardView page = (CardView) layoutInflater
                  .inflate(R.layout.image_with_titel_layout, null);
            final ImageView imageView = page.findViewById(R.id.img);
            final TextView textView = page.findViewById(R.id.txtTitle);
            page.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        
                        switch (ads.get(position).getContentType()) {
                              case Ad.ITEM: {
                                    Intent intent = new Intent(ctx,
                                          ItemDetailsActivity.class);
                                    intent.putExtra("itemId", ads.get(position).getIdOfType());
                                    intent.putExtra("itemNameHeader", "");
                                    
                                    List<Pair<View, String>> pairs = new ArrayList<>();
                                    
                                    pairs.add(
                                          new Pair<View, String>(/*textView,*/ new TextView(ctx),
                                                ItemDetailsActivity.VIEW_NAME_HEADER_TITLE));
                                    pairs.add(new Pair<View, String>(imageView,
                                          ItemDetailsActivity.VIEW_NAME_HEADER_IMAGE));
                                    
                                    ActivityOptionsCompat activityOptions = ActivityOptionsCompat
                                          .makeSceneTransitionAnimation(
                                                (Activity) ctx, pairs.get(0), pairs.get(1)
                                          );
                                    startActivity(ctx, intent, activityOptions.toBundle());
                                    break;
                              }
                              case Ad.SHOP: {
                                    Intent intent = new Intent(ctx,
                                          ShopDetailsActivity.class);
                                    intent.putExtra("shopId", ads.get(position).getIdOfType());
                                    ctx.startActivity(intent);
                                    break;
                              }
                              case Ad.OFFER: {
                                    Intent intent = new Intent(ctx,
                                          OfferDetailsActivity.class);
                                    intent.putExtra("offerId", ads.get(position).getIdOfType());
                                    intent.putExtra("offerImageUrl", ads.get(position).getImageUrl());
                                    ctx.startActivity(intent);
                                    break;
                              }
                        }
                  }
            });
            
            textView.setText(ads.get(position).getImageUrl());
            textView.setVisibility(View.GONE);
            Picasso.with(ctx)
                  .load(API.IMG_ROOT + ads.get(position).getImageUrl())
                  .placeholder(ctx.getResources()
                        .getDrawable(R.drawable.default_item_image))
                  .error(ctx.getResources()
                        .getDrawable(R.drawable.error_item_image))
                  .into(imageView);
            
            container.addView(page);
            
            return page;
      }
      
      @Override
      public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object page) {
            container.removeView((CardView) page);
      }
}