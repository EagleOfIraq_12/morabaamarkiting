package com.morabaa.team.morabaamarkiting.adapters;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.internal.BottomNavigationItemView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.activities.MainActivity;
import com.morabaa.team.morabaamarkiting.activities.ShopDetailsActivity;
import com.morabaa.team.morabaamarkiting.fragments.ItemsFragment;
import com.morabaa.team.morabaamarkiting.model.Shop;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.SV;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eagle on 10/21/2017.
 */

public class ShopsAdapter extends RecyclerView.Adapter<ShopHolder> {
      
      private List<Shop> shops;
      private int layout;
      private Context ctx;
      
      public ShopsAdapter(Context ctx, List<Shop> shops, int layout,
            FragmentActivity activity) {
            this.shops = shops;
            this.layout = layout;
            this.ctx = ctx;
      }
      
      @NonNull
      @Override
      public ShopHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                  layout, parent, false);
            return new ShopHolder(view);
      }
      
      @RequiresApi(api = VERSION_CODES.LOLLIPOP)
      @SuppressLint("SetTextI18n")
      @Override
      public void onBindViewHolder(@NonNull final ShopHolder holder,
            @SuppressLint("RecyclerView") final int position) {
            View.OnClickListener goToItems = new OnClickListener() {
                  @SuppressLint("RestrictedApi")
                  @Override
                  public void onClick(View view) {
                        MainActivity.visitedShops.add(shops.get(position).getId());
                        ((BottomNavigationItemView) ((Activity) ctx)
                              .findViewById(R.id.navigation_shops)).setChecked(false);
                        
                        ((BottomNavigationViewEx) ((Activity) ctx).findViewById(R.id.navMain))
                              .setSelectedItemId(R.id.navigation_items);
                        
                        ((BottomNavigationItemView) ((Activity) ctx)
                              .findViewById(R.id.navigation_items)).setChecked(true);
                        
                        ItemsFragment itemsFragment = new ItemsFragment();
                        Bundle bundle = new Bundle();
                        SV.SHOP_ID = shops.get(position).getId();
                        SV.SEARCH_NAME = shops.get(position).getName();
                        SV.CATEGORY_ID = 0;
                        bundle.putLong("shopId", shops.get(position).getId());
                        itemsFragment.setArguments(bundle);
                        ((AppCompatActivity) ctx).getSupportFragmentManager().beginTransaction()
                              .replace(R.id.mainFrame, itemsFragment)
                              .commit();
                  }
            };
            holder.txtShopName.setText(shops.get(position).getName());
            holder.shopAddress.setText(shops.get(position).getAddress());
            holder.txtShopDescription.setText(shops.get(position).getDescription());
            holder.txtShopGov.setText(shops.get(position).getGovernate().getGovernate());
            Picasso.with(ctx)
                  .load(API.IMG_ROOT + shops.get(position).getImageUrl())
                  .placeholder(ctx.getResources()
                        .getDrawable(R.drawable.default_item_image))
                  .error(ctx.getResources()
                        .getDrawable(R.drawable.error_item_image))
                  .into(holder.imgShopImage);
            holder.txtShopPhone.setText(shops.get(position).getPhoneNum() + "");
            holder.txtShopPhone.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + shops.get(position).getPhoneNum()));
                        if (ActivityCompat.checkSelfPermission(ctx,
                              Manifest.permission.CALL_PHONE)
                              != PackageManager.PERMISSION_GRANTED) {
                              // TODO: Consider calling
                              //    ActivityCompat#requestPermissions
                              // here to request the missing permissions, and then overriding
                              //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                              //                                          int[] grantResults)
                              // to handle the case where the user grants the permission. See the documentation
                              // for ActivityCompat#requestPermissions for more details.
                              return;
                        }
                        ctx.startActivity(callIntent);
                  }
            });
            if (layout == R.layout.shop_layout_wide) {
                  holder.btnItems.setOnClickListener(goToItems);
                  holder.btnInfo.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                              Intent intent = new Intent(ctx,
                                    ShopDetailsActivity.class);
                              intent.putExtra("shopId", shops.get(position).getId());
                              intent.putExtra("shopName", shops.get(position).getName());
                              ctx.startActivity(intent);
                              
                        }
                  });
            } else {
                  holder.view.setOnClickListener(goToItems);
            }
            
            holder.shopImagesRecyclerView.setVisibility(View.GONE);
            try {
                  
                  ImageAdapter imageAdapter = new ImageAdapter(ctx);
                  holder.shopImagesRecyclerView.setAdapter(imageAdapter);
                  holder.shopImagesRecyclerView.setLayoutManager(
                        new LinearLayoutManager(ctx, LinearLayoutManager.HORIZONTAL, true));
                  
            } catch (Exception ignored) {
            }
      }
      
      @Override
      public int getItemCount() {
            return shops.size();
      }
      
      public List<Shop> getShops() {
            return shops;
      }
      
      public void setShops(List<Shop> shops) {
            this.shops = shops;
            notifyDataSetChanged();
      }
      
      public void clear() {
            shops = new ArrayList<>();
            notifyDataSetChanged();
      }
}


class ShopHolder extends RecyclerView.ViewHolder {
      
      CardView view;
      RecyclerView shopImagesRecyclerView;
      
      TextView txtShopName;
      TextView txtShopGov;
      TextView shopAddress;
      TextView txtShopDescription;
      Button btnItems;
      Button btnInfo;
      ImageView imgShopImage;
      TextView txtShopPhone;
      
      ShopHolder(View itemView) {
            super(itemView);
            this.view = (CardView) itemView;
            shopImagesRecyclerView = view.findViewById(R.id.shopImagesRecyclerView);
            
            txtShopName = view.findViewById(R.id.txtShopName);
            txtShopGov = view.findViewById(R.id.txtShopGov);
            shopAddress = view.findViewById(R.id.shopAddress);
            txtShopDescription = view.findViewById(R.id.txtShopDescription);
            btnItems = view.findViewById(R.id.btnItems);
            btnInfo = view.findViewById(R.id.btnInfo);
            imgShopImage = view.findViewById(R.id.imgShopImage);
            txtShopPhone = view.findViewById(R.id.txtShopPhone);
      }
}