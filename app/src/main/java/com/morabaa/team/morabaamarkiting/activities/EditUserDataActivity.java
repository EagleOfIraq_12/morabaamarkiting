package com.morabaa.team.morabaamarkiting.activities;

import static android.content.ContentValues.TAG;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.model.Governate;
import com.morabaa.team.morabaamarkiting.model.ImageUrl;
import com.morabaa.team.morabaamarkiting.model.User;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.morabaa.team.morabaamarkiting.utils.SV;
import com.squareup.picasso.Picasso;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class EditUserDataActivity extends AppCompatActivity implements LocationListener {
      
      private static final int READ_REQUEST_CODE = 42;
      protected LocationManager locationManager;
      
      
      ImageView ImageViewCurrentImage;
      EditText txtCurrentUserName;
      Button btnSaveCurrentChanges;
      EditText txtCurrentPhoneNumber;
      EditText txtCurrentGovernate;
      Spinner spinnerCurrentGovernate;
      String CurrentGpsLocation;
      EditText txtCurrentDistrict;
      EditText txtCurrentAddress;
      User user = new User();
      private Uri localImageUri;
      private List<Governate> governates = new ArrayList<>();
      private Governate governate = new Governate();
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_edit_user_data);
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            String userString = getIntent().getStringExtra("userString");
            user = new Gson().fromJson(userString, User.class);
            
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                  != PackageManager.PERMISSION_GRANTED && ActivityCompat
                  .checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                  != PackageManager.PERMISSION_GRANTED) {
                  // TODO: Consider calling
                  //    ActivityCompat#requestPermissions
                  // here to request the missing permissions, and then overriding
                  //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                  //                                          int[] grantResults)
                  // to handle the case where the user grants the permission. See the documentation
                  // for ActivityCompat#requestPermissions for more details.
//                  return;
            }
            locationManager
                  .requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                        EditUserDataActivity.this);
            
            ImageViewCurrentImage = findViewById(R.id.ImageViewCurrentImage);
            txtCurrentUserName = findViewById(R.id.txtCurrentUserName);
            btnSaveCurrentChanges = findViewById(R.id.btnSaveCurrentChanges);
            txtCurrentPhoneNumber = findViewById(R.id.txtCurrentPhoneNumber);
            txtCurrentPhoneNumber.setEnabled(false);
            txtCurrentPhoneNumber.setText(user.getPhoneNumber());
            
            txtCurrentGovernate = findViewById(R.id.txtCurrentGovernate);
            
            txtCurrentDistrict = findViewById(R.id.txtCurrentDistrict);
            txtCurrentAddress = findViewById(R.id.txtCurrentAddress);
            
            fillUserUserDetailsControls(user);
            ImageViewCurrentImage.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        performFileSearch();
                  }
            });
            btnSaveCurrentChanges.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        saveCurrentChanges();
                  }
            });
            spinnerCurrentGovernate = findViewById(R.id.spinnerCurrentGovernate);
            new HttpRequest<Governate>(this, API.GOVERNATE.GET_ALL, new JSONObject().toString(),
                  new TypeToken<List<Governate>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Governate> gs) {
                        governates = gs;
                        List<String> strings = new ArrayList<>();
                        for (Governate governate : governates) {
                              strings.add(governate.getGovernate());
                        }
                        ArrayAdapter<String> governatesAdapter = new ArrayAdapter(
                              EditUserDataActivity.this,
                              android.R.layout.simple_spinner_item, strings);
                        governatesAdapter
                              .setDropDownViewResource(
                                    android.R.layout.simple_spinner_dropdown_item);
                        spinnerCurrentGovernate.setAdapter(governatesAdapter);
                        
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            spinnerCurrentGovernate
                  .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i,
                              long l) {
                              String governateName = spinnerCurrentGovernate.getSelectedItem()
                                    .toString();
                              try {
                                    setGovernateByName(governateName);
                              } catch (Exception ignored) {
                              
                              }
                        }
                        
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        
                        }
                  });
      }
      
      public Governate getGovernate() {
            return governate;
      }
      
      public void setGovernateByName(String name) {
            for (Governate governate : governates) {
                  if (governate.getGovernate().equals(name)) {
                        Governate g = governate;
                        EditUserDataActivity.this.governate = g;
                  }
            }
      }
      
      private void fillUserUserDetailsControls(User user) {
            try {
                  txtCurrentAddress.setText(user.getAddress());
                  txtCurrentGovernate.setText(user.getGovernate().getGovernate());
                  txtCurrentPhoneNumber.setText(user.getPhoneNumber());
                  txtCurrentPhoneNumber.setEnabled(false);
                  CurrentGpsLocation = user.getGpsLocation();
                  txtCurrentUserName.setText(user.getName());
                  txtCurrentDistrict.setText(user.getDistrict());
                  
                  int i = 0;
                  for (Governate governate : governates) {
                        i++;
                        if (governate == User.getCurrentUser(EditUserDataActivity.this)
                              .getGovernate()) {
                              break;
                        }
                  }
                  
                  spinnerCurrentGovernate.setSelection(i);
                  Picasso.with(getBaseContext())
                        .load(User.getCurrentUser(EditUserDataActivity.this).getImageUrl())
                        .placeholder(
                              getApplication().getResources()
                                    .getDrawable(R.drawable.default_item_image))
                        .error(getApplication().getResources()
                              .getDrawable(R.drawable.error_item_image))
                        .into(ImageViewCurrentImage);
            } catch (Exception ignored) {
                  System.out.println("User:\t" + new Gson().toJson(user, User.class));
            }
            
      }
      
      private void showImage(Uri uri) {
            try {
                  localImageUri = uri;
                  ImageViewCurrentImage.setImageBitmap(getBitmapFromUri(uri));
            } catch (IOException e) {
                  e.printStackTrace();
            }
      }
      
      private Bitmap getBitmapFromUri(Uri uri) throws IOException {
            ParcelFileDescriptor parcelFileDescriptor =
                  getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            parcelFileDescriptor.close();
            return image;
      }
      
      @Override
      public void onActivityResult(int requestCode, int resultCode,
            Intent resultData) {
            
            // The ACTION_OPEN_DOCUMENT intent was sent with the request code
            // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
            // response to some other intent, and the code below shouldn't run at all.
            
            if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                  // The document selected by the user won't be returned in the intent.
                  // Instead, a URI to that document will be contained in the return intent
                  // provided to this method as a parameter.
                  // Pull that URI using resultData.getData().
                  Uri uri = null;
                  if (resultData != null) {
                        uri = resultData.getData();
                        Log.i(TAG, "Uri: " + uri.toString());
                        showImage(uri);
                  }
            }
      }
      
      
      @Override
      public void onLocationChanged(Location location) {
            SV.CURRENT_LOCATION = location.getLatitude() + ", " + location.getLongitude();
            System.out.println("CURRENT_LOCATION :\t" + SV.CURRENT_LOCATION);
//            Toast.makeText(LoginActivity.this,
//                  "Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude(),
//                  Toast.LENGTH_SHORT).show();
      }
      
      @Override
      public void onProviderDisabled(String provider) {
            Log.d("Latitude", "disable");
      }
      
      @Override
      public void onProviderEnabled(String provider) {
            Log.d("Latitude", "enable");
      }
      
      @Override
      public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d("Latitude", "onSubscriptionsReceived");
      }
      
      public void performFileSearch() {
            
            // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
            // browser.
            Intent intent = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                  intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            }
            
            // Filter to only show results that can be "opened", such as a
            // file (as opposed to a list of contacts or timezones)
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            
            // Filter to show only images, using the image MIME data type.
            // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
            // To search for all documents available via installed storage providers,
            // it would be "*/*".
            intent.setType("image/*");
            
            startActivityForResult(intent, READ_REQUEST_CODE);
      }
      
      
      public void ChangeCurrentGPS(View view) {
      }
      
      public void saveCurrentChanges() {
            final User user = new User();
//            //update user Image
//            //region userImage upLoad
//            Bitmap bitmap = ((BitmapDrawable) ImageViewCurrentImage.getDrawable()).getBitmap();
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//            final byte[] imageInByte = baos.toByteArray();
//            ImageUrl imageUrl = new ImageUrl();
//            imageUrl.setImage(imageInByte);
//            imageUrl.setIdOfType(User.getCurrentUser(getApplication()).getId());
//
//            Gson mgson = new Gson();
//
            //endregion
            user.setName(txtCurrentUserName.getText().toString());
            user.setId(User.getCurrentUser(getApplication()).getId());
            user.setAddress(txtCurrentAddress.getText().toString());
            user.setDistrict(txtCurrentDistrict.getText().toString());
            user.setGpsLocation(SV.CURRENT_LOCATION);
            user.setPhoneNumber(txtCurrentPhoneNumber.getText().toString());
            user.setGovernate(getGovernate());
            new HttpRequest<User>(EditUserDataActivity.this, API.USER.UPDATE,
                  new Gson().toJson(user)) {
                  @Override
                  public void onFinish(List<User> users) {
                        try {
                              user.setImageUrl(localImageUri.toString());
                        } catch (Exception e) {
                              user.setImageUrl(
                                    User.getCurrentUser(EditUserDataActivity.this).getImageUrl());
                        }
                        User.saveLoginState(getApplication(), user);
                        MainActivity.mainNavLayout.setVisibility(View.VISIBLE);
                        MainActivity.btnLogOut.setVisibility(View.VISIBLE);
                        MainActivity.btnLogin.setVisibility(View.GONE);
                        try {
                              Picasso.with(getBaseContext())
                                    .load(user.getImageUrl())
                                    .placeholder(getApplication().getResources()
                                          .getDrawable(R.drawable.default_item_image))
                                    .error(getApplication().getResources()
                                          .getDrawable(R.drawable.error_item_image))
                                    .into(MainActivity.imageViewUserImage);
                        } catch (Resources.NotFoundException e) {
                              
                        }
                        Drawable d = MainActivity.imageViewUserImage
                              .getDrawable(); // the drawable (Captain Obvious, to the rescue!!!)
                        Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        byte[] bitmapdata = stream.toByteArray();
                        ImageUrl imageUrl1 = new ImageUrl();
                        imageUrl1.setIdOfType(user.getId());
                        imageUrl1.setImage(bitmapdata);
                        
//                        new HttpRequest<ImageUrl>(EditUserDataActivity.this,
//                              API.IMAGE_URL.ADD_USER_IMAGE,
//                              new Gson().toJson(imageUrl1)) {
//                              @Override
//                              public void onFinish(List<ImageUrl> imageUrls) {
//                              }
//
//                              @Override
//                              public void onError(String error) {
//
//                              }
//                        };
//                onBackPressed();
                        Intent mainIntent = new Intent(EditUserDataActivity.this,
                              MainActivity.class);
                        startActivity(mainIntent);
                        finish();
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            
      }
      
}
