package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;

@Entity
public class Shop {
      
      @PrimaryKey
      private long id;
      private String name;
      private String gpsLocation;
      private String address;
      private String type;
      private String description;
      @Ignore
      private List<Item> items;
      @Ignore
      private List<ImageUrl> imageUrls;
      @Ignore
      private List<Category> categories;
      @Ignore
      private Governate governate;
      private int visitCount;
      private int saleCount;
      private float rating;
      private String phoneNumber;
      private String imageUrl;
      private String zainCashWallet;
      private String zainCashSecret;
      
      public Shop() {
      }
      
      public List<ImageUrl> getImageUrls() {
            return imageUrls;
      }
      
      public void setImageUrls(List<ImageUrl> imageUrls) {
            this.imageUrls = imageUrls;
      }
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public String getName() {
            return name;
      }
      
      public void setName(String name) {
            this.name = name;
      }
      
      public String getGpsLocation() {
            return gpsLocation;
      }
      
      public void setGpsLocation(String gpsLocation) {
            this.gpsLocation = gpsLocation;
      }
      
      public String getAddress() {
            return address;
      }
      
      public void setAddress(String address) {
            this.address = address;
      }
      
      public String getType() {
            return type;
      }
      
      public void setType(String type) {
            this.type = type;
      }
      
      public String getDescription() {
            return description;
      }
      
      public void setDescription(String description) {
            this.description = description;
      }
      
      public List<Item> getItems() {
            return items;
      }
      
      public void setItems(List<Item> items) {
            this.items = items;
      }
      
      public List<Category> getCategories() {
            return categories;
      }
      
      public void setCategories(List<Category> categories) {
            this.categories = categories;
      }
      
      public Governate getGovernate() {
            return governate;
      }
      
      public void setGovernate(Governate governate) {
            this.governate = governate;
      }
      
      public int getVisitCount() {
            return visitCount;
      }
      
      public void setVisitCount(int visitCount) {
            this.visitCount = visitCount;
      }
      
      public int getSaleCount() {
            return saleCount;
      }
      
      public void setSaleCount(int saleCount) {
            this.saleCount = saleCount;
      }
      
      public float getRating() {
            return rating;
      }
      
      public void setRating(float rating) {
            this.rating = rating;
      }
      
      public String getPhoneNumber() {
            return phoneNumber;
      }
      
      public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
      }
      
      public String getImageUrl() {
            return imageUrl;
      }
      
      public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
      }
      
      public String getZainCashWallet() {
            return zainCashWallet;
      }
      
      public void setZainCashWallet(String zainCashWallet) {
            this.zainCashWallet = zainCashWallet;
      }
      
      public String getZainCashSecret() {
            return zainCashSecret;
      }
      
      public void setZainCashSecret(String zainCashSecret) {
            this.zainCashSecret = zainCashSecret;
      }
      
      @Dao
      public interface ShopDao {
            
            @Query("SELECT * FROM Shop")
            List<Shop> SHOPS();
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insert(Shop... shops);
            
            
            @Delete
            void delete(Shop... shops);
            
            @Update
            void Update(Shop... shops);
            
      }
}
