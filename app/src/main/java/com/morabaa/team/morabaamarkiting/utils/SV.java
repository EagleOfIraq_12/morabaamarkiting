package com.morabaa.team.morabaamarkiting.utils;

import android.app.Activity;
import android.util.DisplayMetrics;


/**
 * Created by eagle on 1/16/2018.
 */

public class SV {
      
      public static long CURRENT_ITEM = 0;
      public static String CURRENT_FRAGMENT = "";
      public static long SHOP_ID;
      public static String SEARCH_NAME;
      public static long CATEGORY_ID;
      public static String CURRENT_LOCATION = "";
      
      public static class ScreenDimensions {
            
            public static int getHeight(Activity activity) {
                  DisplayMetrics displayMetrics = new DisplayMetrics();
                  activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                  return displayMetrics.heightPixels;
            }
            
            public static int getWidth(Activity activity) {
                  DisplayMetrics displayMetrics = new DisplayMetrics();
                  activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                  return displayMetrics.widthPixels;
            }
      }
      
}
