package com.morabaa.team.morabaamarkiting.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.adapters.ItemsAdapter;
import com.morabaa.team.morabaamarkiting.model.Item;
import com.morabaa.team.morabaamarkiting.model.Offer;
import com.morabaa.team.morabaamarkiting.model.OfferItem;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class OfferDetailsActivity extends AppCompatActivity {
      
      ProgressBar progressOfferWaiting;
      private long offerId;
      private String offerName;
      private String offerImageUrl;
      private RecyclerView offerItemsRecyclerView;
      private ImageView imgOfferImage;
      private TextView txtOfferName;
      private TextView txtOfferDescription;
      // 07810850799
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_offer_details);
            getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            offerId = getIntent().getLongExtra("offerId", 0);
            offerName = getIntent().getStringExtra("offerName");
            offerImageUrl = getIntent().getStringExtra("offerImageUrl");
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        System.out.println("Back from arrow");
                        onBackPressed();
                  }
            });
            setSupportActionBar(toolbar);
            setTitle(offerName);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            
            offerItemsRecyclerView = findViewById(R.id.offerItemsRecyclerView);
            imgOfferImage = findViewById(R.id.imgOfferImage);
            txtOfferName = findViewById(R.id.txtOfferName);
            txtOfferDescription = findViewById(R.id.txtOfferDescription);
            progressOfferWaiting = findViewById(R.id.prograssOfferWating);
            
            Picasso.with(this)
                  .load(API.IMG_ROOT + offerImageUrl)
                  .placeholder(this.getResources()
                        .getDrawable(R.drawable.default_item_image))
                  .error(this.getResources()
                        .getDrawable(R.drawable.error_item_image))
                  .into(imgOfferImage);
            offerItemsRecyclerView.setLayoutManager(
                  new GridLayoutManager(OfferDetailsActivity.this, 2, GridLayoutManager.VERTICAL,
                        false));
            String url = API.OFFER.GET_BY_ID(offerId);
            new HttpRequest<Offer>(OfferDetailsActivity.this, url,
                  new JSONObject().toString(),
                  new TypeToken<List<Offer>>() {
                  }.getType()) {
                  @SuppressLint("SetTextI18n")
                  @Override
                  public void onFinish(List<Offer> offers) {
                        progressOfferWaiting.setVisibility(View.GONE);
                        Offer offer = offers.get(0);
                        txtOfferName.setText(offer.getName());
                        setTitle(offer.getName());
                        txtOfferDescription.setText(offer.getDiscription() +
                              " العرض ساري من " + offer.getStartDate().substring(0, 10) + " الى "
                              + offer.getEndDate().substring(0, 10));
                        List<Item> items = new ArrayList<>();
                        if (offer.getOfferItems() != null) {
                              for (OfferItem offerItem : offer.getOfferItems()) {
                                    items.add(offerItem.getItem());
                              }
                              ItemsAdapter itemAdapter = new ItemsAdapter(
                                    OfferDetailsActivity.this, items,
                                    R.layout.item_layout, OfferDetailsActivity.this);
                              offerItemsRecyclerView.setAdapter(itemAdapter);
                        }
                  }
                  
                  @Override
                  public void onError(String error) {
                        System.out.println("Error: " + error);
                  }
            };
            
//    new ZainHttpRequest<OfferItem>(OfferDetailsActivity.this, API.OFFER.GET_OFFER_ITEMS(offerId),
//                new JSONObject(),
//                 "OfferItem") {
//            @Override
//            public void onFinish(List<OfferItem> offsetTimes) {
//                List<Item> items = new ArrayList<>();
//                for (OfferItem offerItem : offsetTimes) {
//                    items.add(offerItem.getItem());
//                }
//                ItemsAdapter itemAdapter = new ItemsAdapter(
//                        OfferDetailsActivity.this, items,
//                        R.layout.item_layout_wide, OfferDetailsActivity.this);
//                offerItemsRecyclerView.setAdapter(itemAdapter);
//            }
//
//            @Override
//            public void onError(String error) {
//
//            }
//        };
//
//
      }
      
      @Override
      public void onBackPressed() {
            System.out.println("Back from back button");
            super.onBackPressed();
      }
      
      @Override
      public boolean onCreateOptionsMenu(Menu menu) {
            return super.onCreateOptionsMenu(menu);
      }
      
      @Override
      public boolean onOptionsItemSelected(MenuItem item) {
            if (item.getItemId() == android.R.id.home) {
                  onBackPressed();
                  System.out.println("Back from arrow");
            }
            return super.onOptionsItemSelected(item);
      }
}
