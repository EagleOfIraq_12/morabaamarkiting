package com.morabaa.team.morabaamarkiting.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.activities.ItemDetailsActivity;
import com.morabaa.team.morabaamarkiting.model.CartItem;
import com.morabaa.team.morabaamarkiting.model.Item;
import com.morabaa.team.morabaamarkiting.model.User;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.API.CART;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.morabaa.team.morabaamarkiting.utils.SV.ScreenDimensions;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by eagle on 1/22/2018.
 */

public class CartItemsDeliveredAdapter extends
        RecyclerView.Adapter<CartItemsNotDeliveredViewHolder> {

    private List<CartItem> cartItems;
    private Context ctx;
    private Activity activity;

    public CartItemsDeliveredAdapter(Context ctx, List<CartItem> cartItems,
                                     Activity activity) {
        this.cartItems = cartItems;
        this.ctx = ctx;
        this.activity = activity;

    }

    @Override
    public CartItemsNotDeliveredViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.order_item_layout_wide, parent, false);
        return new CartItemsNotDeliveredViewHolder(view);
    }

    @SuppressLint({"StaticFieldLeak", "SetTextI18n"})
    @Override
    public void onBindViewHolder(final CartItemsNotDeliveredViewHolder holder,
                                 final int position) {

        int w = ScreenDimensions.getWidth(activity);
        holder.imgItemImage.getLayoutParams().height = w / 4;

        holder.view.getLayoutParams().width = w;

        holder.view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

//                        startDetailActivity(dataSet.get(position).getId()+"",holder.view);
                Intent intent = new Intent(ctx,
                        ItemDetailsActivity.class);
                // pass the item information
//                        intent.getExtras().putLong("id", dataSet.get(position).getId());
                ctx.startActivity(intent);

            }
        });
        holder.txtItemName.setText(cartItems.get(position).getItem().getName());
        holder.txtItemDescription.setText(cartItems.get(position).getItem().getDescription());
        holder.txtItemPrice
                .setText(cartItems.get(position).getItem().getPrice() + " دينار عراقي");

        holder.txtItemCount.setText(cartItems.get(position).getCount() + "");
        holder.txtNote.setText(cartItems.get(position).getNotes());

        holder.btnCartPosAction.setText("تكرار");
        holder.btnCartNegAction.setText("حذف");

        holder.btnCartPosAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                CartItem cartItem = new CartItem();
                Item item = new Item();
                item.setId(cartItems.get(position).getId());
                cartItem.setItem(item);
                cartItem.setNotes(cartItems.get(position).getNotes());
                cartItem.setCount(cartItems.get(position).getCount());
                cartItem.setUserId(User.getCurrentUser(ctx).getId());
                cartItem.setStatus(CartItem.Status.WAITING);
                String data = new Gson().toJson(cartItem, CartItem.class);
                new HttpRequest<CartItem>(ctx, API.CART.ADD_ORDER,
                        data,
                        new TypeToken<List<CartItem>>() {
                        }.getType()) {
                    @Override
                    public void onFinish(List<CartItem> cartItems) {
                        Toast.makeText(ctx, "تمَ ارسال الطلب." + " " +
                                        cartItems.get(0).getId()
                                , Toast.LENGTH_SHORT).show();
//                pay(cartItems.get(0).getId());
                    }

                    @Override
                    public void onError(String error) {
                        Toast.makeText(ctx, "فشلَ ارسال الطلب."+ " "+error,
                                Toast.LENGTH_SHORT).show();
                    }
                };

//                new ZainHttpRequest<CartItem>(ctx,
//                        CART.DUPLICATE_ORDER + cartItems.get(position).getId(),"") {
//                    @Override
//                    public void onFinish(List<CartItem> c) {
//
//                    }
//
//                    @Override
//                    public void onError(String error) {
//
//                    }
//                };
            }
        });
        holder.btnCartNegAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                new HttpRequest<CartItem>(ctx,
                        CART.DELETE_ORDER + cartItems.get(position).getId(), "") {
                    @Override
                    public void onFinish(List<CartItem> c) {
                        if (cartItems.size() != 0) {
                            cartItems.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, cartItems.size());
                        }
                    }

                    @Override
                    public void onError(String error) {

                    }
                };
            }
        });

        Picasso.with(ctx)
                .load(API.IMG_ROOT + cartItems.get(position).getItem().getImageUrl())
                .placeholder(ctx.getResources().getDrawable(R.drawable.default_item_image))
                .error(ctx.getResources().getDrawable(R.drawable.error_item_image))
                .into(holder.imgItemImage);

    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
//            notifyAll();
        notifyDataSetChanged();
    }
}

class CartItemsViewHolder extends RecyclerView.ViewHolder {

    CardView view;
    TextView txtItemCount;
    Button btnCartPosAction;
    Button btnCartNegAction;
    TextView txtNote;
    ImageView imgItemImage;
    TextView txtItemName;
    TextView txtItemDescription;
    TextView txtItemPrice;


    public CartItemsViewHolder(View itemView) {
        super(itemView);
        view = (CardView) itemView;
        imgItemImage = itemView.findViewById(R.id.imgItemImage);
        txtItemCount = itemView.findViewById(R.id.txtItemCount);
        btnCartPosAction = itemView.findViewById(R.id.btnCartPosAction);
        btnCartNegAction = itemView.findViewById(R.id.btnCartNegAction);
        txtNote = itemView.findViewById(R.id.txtNote);
        imgItemImage = itemView.findViewById(R.id.imgItemImage);
        txtItemName = itemView.findViewById(R.id.txtItemName);
        txtItemDescription = itemView.findViewById(R.id.txtItemDescription);
        txtItemPrice = itemView.findViewById(R.id.txtItemPrice);

    }
}
