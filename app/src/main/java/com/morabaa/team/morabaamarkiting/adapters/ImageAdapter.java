package com.morabaa.team.morabaamarkiting.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.adapters.ImageAdapter.ImageHolder;
import com.morabaa.team.morabaamarkiting.model.ImageUrl;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.SV;
import com.squareup.picasso.Picasso;
import java.util.List;

/**
 * Created by eagle on 2/17/2018.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageHolder> {
      
      Context ctx;
      List<ImageUrl> imageUrls;
      
      public ImageAdapter(Context ctx) {
            this.ctx = ctx;
      }
      
      public ImageAdapter(Context ctx,
            List<ImageUrl> imageUrls) {
            this.ctx = ctx;
            this.imageUrls = imageUrls;
      }
      
      @Override
      public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            
            return new ImageHolder(new ImageView(ctx));
      }
      
      @Override
      public void onBindViewHolder(ImageHolder holder, int position) {
            if (imageUrls != null) {
                  holder.imageView
                        .setMinimumWidth(SV.ScreenDimensions.getWidth((Activity) ctx));
                  holder.imageView
                        .setMinimumHeight(SV.ScreenDimensions.getWidth((Activity) ctx) / 2);
                  
                  Picasso.with(ctx)
                        .load(API.IMG_ROOT + imageUrls.get(position).getUrl())
                        .placeholder(ctx.getResources().getDrawable(R.drawable.default_item_image))
                        .error(ctx.getResources().getDrawable(R.drawable.error_item_image))
                        .into(holder.imageView);
                  
            }
            holder.imageView.setMinimumWidth(SV.ScreenDimensions.getWidth((Activity) ctx) / 5);
            holder.imageView.setMinimumHeight(SV.ScreenDimensions.getWidth((Activity) ctx) / 5);
            holder.imageView
                  .setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_menu_send));
            
      }
      
      @Override
      public long getItemId(int i) {
            return 0;
      }
      
      @Override
      public int getItemCount() {
            if (imageUrls != null) {
                  return imageUrls.size();
            }
            return 30;
      }
      
      class ImageHolder extends RecyclerView.ViewHolder {
            
            ImageView imageView;
            
            public ImageHolder(View itemView) {
                  super(itemView);
                  imageView = (ImageView) itemView;
            }
      }
}
