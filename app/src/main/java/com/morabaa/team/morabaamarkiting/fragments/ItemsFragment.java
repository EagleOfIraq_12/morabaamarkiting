package com.morabaa.team.morabaamarkiting.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.SearchView.OnCloseListener;
import android.widget.Spinner;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.activities.MainActivity;
import com.morabaa.team.morabaamarkiting.adapters.CategoryOfShopAdapter;
import com.morabaa.team.morabaamarkiting.adapters.ItemsAdapter;
import com.morabaa.team.morabaamarkiting.model.Category;
import com.morabaa.team.morabaamarkiting.model.Governate;
import com.morabaa.team.morabaamarkiting.model.Item;
import com.morabaa.team.morabaamarkiting.model.Item.SearchItems;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.API.ITEM;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.morabaa.team.morabaamarkiting.utils.SV;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class ItemsFragment extends Fragment {
      
      RecyclerView itemsRecyclerView;
      ItemsAdapter itemsAdapter;
      String searchKey = "-";
      String sortType = "rating";
      String orderType = "DESC";
      Spinner spinnerGovernates;
      RecyclerView categoriesRecyclerView;
      CardView shopCategoriesContainer;
      private LinearLayout layoutSort;
      private boolean waiting = true;
      private long shopId = SV.SHOP_ID;
      private long categoryId = SV.CATEGORY_ID;
      private long governateId = 0;
      private ProgressBar bottomProgressBar;
      private List<Item> items;
      private List<Long> lastSentItemId = new ArrayList<>();
      private SearchView txtSearchItemsSearch;
      private List<Category> categories;
      private CategoryOfShopAdapter categoryOfShopAdapter;
      private TextView txtNoItems;
      private List<Governate> governates = new ArrayList<>();
      private Governate governate = new Governate();
      
      public ItemsFragment() {
            // Required empty public constructor
      }
      
      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
            if (getArguments() != null) {
                  shopId = getArguments().getLong("shopId", 0);
                  categoryId = getArguments().getLong("categoryId", 0);
                  
            }
//        Toast.makeText(getContext(), shopId + " " + categoryId, Toast.LENGTH_SHORT).show();
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.fragment_items, container, false);
      }
      
      @Override
      public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            categoriesRecyclerView = view.findViewById(R.id.shopCategoriesRecyclerView);
            shopCategoriesContainer = view.findViewById(R.id.shopCategoriesContainer);
            itemsRecyclerView = view.findViewById(R.id.itemsRecyclerView);
            bottomProgressBar = view.findViewById(R.id.bottomProgressBar);
            spinnerGovernates = view.findViewById(R.id.spinnerGovernates);
            layoutSort = view.findViewById(R.id.layoutSort);
            txtNoItems = view.findViewById(R.id.txtNoItems);
            MainActivity.homeFragment = false;
            items = new ArrayList<>();
            if (shopId == 0 || SV.SHOP_ID == 0) {
                  shopCategoriesContainer.setVisibility(View.GONE);
            }
            
            itemsAdapter = new ItemsAdapter(getContext(), items,
                  R.layout.item_layout_wide,
                  getActivity());
            itemsRecyclerView.setLayoutManager(
                  new LinearLayoutManager(getContext(),
                        LinearLayoutManager.VERTICAL,
                        false));
            itemsRecyclerView.setAdapter(itemsAdapter);
            categoriesRecyclerView.setLayoutManager(
                  new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false)
            );
            categories = new ArrayList<>();
            categoryOfShopAdapter = new CategoryOfShopAdapter(
                  getContext(), categories);
            categoriesRecyclerView.setLayoutManager(
                  new GridLayoutManager(getContext(), 3, GridLayoutManager.VERTICAL, false));
            categoriesRecyclerView.setAdapter(categoryOfShopAdapter);
            bottomProgressBar.setVisibility(View.VISIBLE);
            new HttpRequest<Category>(getContext(),
                  API.CATEGORY.GET_CATEGORIES_BY_SHOP_ID(shopId), new JSONObject().toString(),
                  new TypeToken<List<Category>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Category> newCategories) {
                        categories.addAll(newCategories);
                        System.out.println("downloaded: " +
                              new Gson().toJson(categories) + "\n" +
                              categories.getClass().getName()
                        );
                        categoryOfShopAdapter.setCategories(categories);
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            
            txtSearchItemsSearch = view.findViewById(R.id.txtSearchItemsSearch);
            if (shopId != 0 || categoryId != 0) {
                  txtSearchItemsSearch.setQueryHint("ابحث في " + SV.SEARCH_NAME);
            }
            txtSearchItemsSearch.setOnCloseListener(new OnCloseListener() {
                  @Override
                  public boolean onClose() {
                        layoutSort.setVisibility(View.GONE);
                        return false;
                  }
            });
            txtSearchItemsSearch.setOnSearchClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        layoutSort.setVisibility(View.VISIBLE);
                  }
            });
            txtSearchItemsSearch
                  .setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String query) {
                              itemsAdapter.clear();
                              bottomProgressBar.setVisibility(View.VISIBLE);
                              items = new ArrayList<>();
                              layoutSort.setVisibility(View.GONE);
                              
                              governateId = governateId == 18 ? 0 : governateId;
                              
                              new HttpRequest<Item>(getContext(),
                                    ITEM.GET_LAST_50_ITEM_NOT_IN
                                    , new SearchItems(governateId,
                                    shopId,
                                    categoryId, searchKey,
                                    sortType, orderType, new ArrayList<Long>()).toString(),
                                    new TypeToken<List<Item>>() {
                                    }.getType()) {
                                    @Override
                                    public void onFinish(List<Item> newItems) {
                                          bottomProgressBar.setVisibility(View.GONE);
                                          items.addAll(newItems);
                                          checkAvailability();
                                          
                                          System.out.println("downloaded: " +
                                                new Gson().toJson(items) + "\n" +
                                                items.getClass().getName()
                                          );
                                          itemsAdapter.setItems(items);
                                          checkAvailability();

//                                          itemsAdapter = new ItemsAdapter(getContext(), items,
//                                                R.layout.item_layout_wide,
//                                                getActivity());
//                                          itemsRecyclerView.setLayoutManager(
//                                                new LinearLayoutManager(getContext(),
//                                                      LinearLayoutManager.VERTICAL,
//                                                      false));
//                                          itemsRecyclerView.setAdapter(itemsAdapter);
                                    }
                                    
                                    @Override
                                    public void onError(String error) {
                                          bottomProgressBar.setVisibility(View.GONE);
                                          checkAvailability();
                                    }
                              };
                              return false;
                        }
                        
                        @Override
                        public boolean onQueryTextChange(String newText) {
                              searchKey = newText;/* new String(
                                    Base64.encodeBase64(newText.getBytes()), "utf-8"
                              );*/
                              if (newText.equals("")) {
                                    this.onQueryTextSubmit("");
                                    txtSearchItemsSearch.setIconified(true);
                              }
                              return false;
                        }
                  });
            
            ((RadioButton) view.findViewById(R.id.radRate)).setOnCheckedChangeListener(
                  new RadioButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                              if (isChecked) {
                                    sortType = "rating";
                              }
                        }
                  });
            ((RadioButton) view.findViewById(R.id.radPrice)).setOnCheckedChangeListener(
                  new RadioButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                              if (isChecked) {
                                    sortType = "price";
                              }
                        }
                  });
            ((RadioButton) view.findViewById(R.id.radSaleCount)).setOnCheckedChangeListener(
                  new RadioButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                              if (isChecked) {
                                    sortType = "saleCount";
                              }
                        }
                  });
            ((RadioButton) view.findViewById(R.id.radTypeAES)).setOnCheckedChangeListener(
                  new RadioButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                              if (isChecked) {
                                    orderType = "AES";
                              }
                        }
                  });
            
            ((RadioButton) view.findViewById(R.id.radTypeDES)).setOnCheckedChangeListener(
                  new RadioButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                              if (isChecked) {
                                    orderType = "DESC";
                              }
                        }
                  });
            
            view.findViewById(R.id.btnItemsSort).setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        if (layoutSort.getVisibility() == View.GONE) {
                              layoutSort.setVisibility(View.VISIBLE);
                        } else {
                              layoutSort.setVisibility(View.GONE);
                        }
                        
                  }
            });
            view.findViewById(R.id.btnItemsSortOK).setOnClickListener(
                  new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                              itemsAdapter.clear();
                              items = new ArrayList<>();
                              new HttpRequest<Item>(getContext(),
                                    ITEM.GET_LAST_50_ITEM_NOT_IN
                                    , new SearchItems(governateId,
                                    shopId,
                                    categoryId, searchKey,
                                    sortType, orderType, new ArrayList<Long>()).toString(),
                                    new TypeToken<List<Item>>() {
                                    }.getType()) {
                                    @Override
                                    public void onFinish(List<Item> newItems) {
                                          
                                          items.addAll(newItems);
                                          checkAvailability();
                                          
                                          System.out.println("downloaded: " +
                                                new Gson().toJson(items) + "\n" +
                                                items.getClass().getName()
                                          );
                                          itemsAdapter.setItems(items);

//                                          itemsAdapter = new ItemsAdapter(getContext(), items,
//                                                R.layout.item_layout_wide,
//                                                getActivity());
//                                          itemsRecyclerView.setLayoutManager(
//                                                new LinearLayoutManager(getContext(),
//                                                      LinearLayoutManager.VERTICAL,
//                                                      false));
//                                          itemsRecyclerView.setAdapter(itemsAdapter);
                                    }
                                    
                                    @Override
                                    public void onError(String error) {
                                          bottomProgressBar.setVisibility(View.GONE);
                                          checkAvailability();
                                    }
                              };
                              
                        }
                  });
            
            new HttpRequest<Governate>(getContext(), API.GOVERNATE.GET_ALL,
                  new JSONObject().toString(),
                  new TypeToken<List<Governate>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Governate> gs) {
                        governates = gs;
                        List<String> strings = new ArrayList<>();
                        for (Governate governate : governates) {
                              strings.add(governate.getGovernate());
                        }
                        if (getContext() != null) {
                              ArrayAdapter<String> governatesAdapter = new ArrayAdapter(
                                    getContext(),
                                    android.R.layout.simple_spinner_item, strings);
                              governatesAdapter
                                    .setDropDownViewResource(
                                          android.R.layout.simple_spinner_dropdown_item);
                              spinnerGovernates.setAdapter(governatesAdapter);
                              spinnerGovernates.setSelection(governates.size() - 1, true);
                        }
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            spinnerGovernates.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        String governateName = spinnerGovernates.getSelectedItem().toString();
                        try {
                              setGovernateByName(governateName);
                        } catch (Exception ignored) {
                        
                        }
                  }
                  
                  @Override
                  public void onNothingSelected(AdapterView<?> adapterView) {
                  
                  }
            });
            String x = new SearchItems(governateId,
                  shopId,
                  categoryId, searchKey,
                  sortType, orderType, new ArrayList<Long>()).toString();
            System.out.println(x);
            new HttpRequest<Item>(getContext(),
                  ITEM.GET_LAST_50_ITEM_NOT_IN
                  , x,
                  new TypeToken<List<Item>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Item> newItems) {
                        items.addAll(newItems);
                        checkAvailability();
                        System.out.println("downloaded: " +
                              new Gson().toJson(items) + "\n" +
                              items.getClass().getName()
                        );
                        itemsAdapter.setItems(items);

//                        itemsAdapter = new ItemsAdapter(getContext(), items,
//                              R.layout.item_layout_wide,
//                              getActivity());
//                        itemsRecyclerView.setLayoutManager(
//                              new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,
//                                    false));
//                        itemsRecyclerView.setAdapter(itemsAdapter);
                  }
                  
                  @Override
                  public void onError(String error) {
                        bottomProgressBar.setVisibility(View.GONE);
                        checkAvailability();
                  }
            };
            
            bottomProgressBar.setVisibility(View.GONE);
            itemsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                  @Override
                  public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        
                        if (!recyclerView.canScrollVertically(1) && waiting) {
                              waiting = false;
                              final List<Long> currentIds = new ArrayList<>();
                              for (Item item : items) {
                                    currentIds.add(item.getId());
                              }
                              
                              bottomProgressBar.setVisibility(View.VISIBLE);
                              new HttpRequest<Item>(getContext(),
                                    ITEM.GET_LAST_50_ITEM_NOT_IN
                                    , new SearchItems(governateId,
                                    shopId,
                                    categoryId, searchKey,
                                    sortType, orderType, currentIds).toString(),
                                    new TypeToken<List<Item>>() {
                                    }.getType()) {
                                    @Override
                                    public void onFinish(List<Item> newItems) {
                                          bottomProgressBar.setVisibility(View.GONE);
                                          lastSentItemId = currentIds;
                                          items.addAll(newItems);
                                          checkAvailability();
                                          itemsAdapter.setItems(items);
                                          
                                          waiting = true;
                                          
                                          bottomProgressBar.setVisibility(View.GONE);
                                    }
                                    
                                    @Override
                                    public void onError(String error) {
                                          bottomProgressBar.setVisibility(View.GONE);
                                          checkAvailability();
                                    }
                              };
                        } else {
                              bottomProgressBar.setVisibility(View.GONE);
                        }
                  }
            });
      }
      
      public Governate getGovernate() {
            return governate;
      }
      
      public void setGovernateByName(String name) {
            for (Governate gov : governates) {
                  if (gov.getGovernate().equals(name)) {
                        governate = gov;
                        governateId = gov.getId();
                  }
            }
      }
      
      private void checkAvailability() {
            if (items != null && items.size() > 0) {
                  itemsRecyclerView.setVisibility(View.VISIBLE);
                  txtNoItems.setVisibility(View.GONE);
            } else {
                  itemsRecyclerView.setVisibility(View.GONE);
                  txtNoItems.setVisibility(View.VISIBLE);
            }
      }
}