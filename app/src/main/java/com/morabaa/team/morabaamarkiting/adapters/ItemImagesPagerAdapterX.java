package com.morabaa.team.morabaamarkiting.adapters;

/**
 * Created by eagle on 1/22/2018.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.model.ImageUrl;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.squareup.picasso.Picasso;
import java.util.List;


public class ItemImagesPagerAdapterX extends PagerAdapter {
      
      Context ctx;
      List<ImageUrl> imageUrls;
      LayoutInflater mLayoutInflater;
      private int[] mResources = {
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_foreground,
            R.drawable.ic_menu_camera,
            R.drawable.ic_menu_gallery,
            R.drawable.ic_menu_send,
            R.drawable.ic_menu_share
      };
      
      public ItemImagesPagerAdapterX(Context ctx, List<ImageUrl> imageUrls) {
            this.ctx = ctx;
            this.imageUrls = imageUrls;
            
            mLayoutInflater = (LayoutInflater) ctx
                  .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            
      }
      
      @Override
      public int getCount() {
            return mResources.length;
      }
      
      @Override
      public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
      }
      
      @Override
      public Object instantiateItem(ViewGroup collection, final int position) {
            
            LayoutInflater inflater = LayoutInflater.from(ctx);
            RelativeLayout relativeLayout = (RelativeLayout) inflater
                  .inflate(R.layout.image_with_titel_layout,
                        collection, false);
            
            relativeLayout.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        Toast.makeText(ctx, "" + position, Toast.LENGTH_SHORT).show();
                  }
            });
            ImageView img = relativeLayout.findViewById(R.id.img);
            TextView txtTitle = relativeLayout.findViewById(R.id.txtTitle);
            txtTitle.setText(imageUrls.get(position).getUrl());
            Picasso.with(ctx)
                  .load(API.IMG_ROOT + imageUrls.get(position).getUrl())
                  .placeholder(ctx.getResources()
                        .getDrawable(R.drawable.default_item_image))
                  .error(ctx.getResources()
                        .getDrawable(R.drawable.error_item_image))
                  .into(img);
            
            collection.addView(relativeLayout);
            
            return relativeLayout;
      }
      
      @Override
      public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
      }
}