package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.content.Context;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.morabaa.team.morabaamarkiting.utils.LocalData;
import java.util.List;

@Entity
public class User {
      
      @PrimaryKey
      private long id;
      private String name;
      private String imageUrl;
      private String phoneNumber;
      private String Email;
      private String password;
      private String address;
      private String district;
      private String gpsLocation;
      private String postalCode;
      @Ignore
      private Cart cart;
      @Ignore
      private Governate governate;
      
      public User() {
      }
      
      public static String getClassName() {
            return "User";
      }
      
      public static void saveLoginState(Context ctx, User user) {
            FirebaseMessaging.getInstance().subscribeToTopic("User" + user.getId());
            LocalData.add(ctx, getClassName(), new Gson().toJson(user, User.class));
      }
      
      public static void signOut(Context ctx) {
            User user = new Gson().fromJson(LocalData.get(ctx, getClassName()), User.class);
            FirebaseMessaging.getInstance().unsubscribeFromTopic("user" + user.getId());
            user = new User();
            Gson gson = new Gson();
            
            LocalData.add(ctx, getClassName(), gson.toJson(user));
      }
      
      public static User getCurrentUser(Context ctx) {
            String userString = LocalData.get(ctx, User.getClassName());
            return new Gson().fromJson(userString, User.class);
      }
      
      public static boolean checkUserDetails(Context ctx) {
            User user = new Gson().fromJson(LocalData.get(ctx, User.getClassName()), User.class);
            return !(user.name == null || user.address == null || user.district == null
                  /* || user.governate == null*/);
      }
      
      public static boolean IsLoggedIn(Context ctx) {
            return getCurrentUser(ctx) != null;
      }
      
      //region prop
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public String getName() {
            return name;
      }
      
      public void setName(String name) {
            this.name = name;
      }
      
      public String getImageUrl() {
            return imageUrl;
      }
      
      public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
      }
      
      public String getPhoneNumber() {
            return phoneNumber;
      }
      
      public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
      }
      
      public String getEmail() {
            return Email;
      }
      
      public void setEmail(String email) {
            Email = email;
      }
      
      public String getPassword() {
            return password;
      }
      
      public void setPassword(String password) {
            this.password = password;
      }
      
      public String getAddress() {
            return address;
      }
      
      public void setAddress(String address) {
            this.address = address;
      }
      
      public Governate getGovernate() {
            return governate;
      }
      
      public void setGovernate(Governate governate) {
            this.governate = governate;
      }
      
      public String getDistrict() {
            return district;
      }
      
      public void setDistrict(String district) {
            this.district = district;
      }
      
      //endregion
      
      public String getGpsLocation() {
            return gpsLocation;
      }
      
      public void setGpsLocation(String gpsLocation) {
            this.gpsLocation = gpsLocation;
      }
      
      public String getPostalCode() {
            return postalCode;
      }
      
      public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
      }
      
      public Cart getCart() {
            return cart;
      }
      
      public void setCart(Cart cart) {
            this.cart = cart;
      }
      
      @Dao
      public interface UserDao {
            
            @Query("SELECT * FROM User")
            List<User> USERS();
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insert(User... users);
            
            
            @Delete
            void delete(User... users);
            
            @Update
            void Update(User... users);
            
      }
}