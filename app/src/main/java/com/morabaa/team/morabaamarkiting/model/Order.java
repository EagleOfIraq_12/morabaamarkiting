package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.Date;
import java.util.List;

/**
 * Created by eagle on 2/12/2018.
 */
@Entity
public class Order {

    @PrimaryKey
    private long id;
    @Ignore
    private List<OrderItem> orderItems;
    private float price;
    private String date;
    private int status;
    private long userId;
    @Ignore
    private User user;
    private long shopId;
    @Ignore
    private Shop shop;
    private String note;
    private int paymentMethod;

    private boolean paid;

    public Order() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(int paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    @Dao
    public interface OrderDao {

        @Query("SELECT * FROM `Order`")
        List<Order> ORDERS();

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(Order... orders);


        @Delete
        void delete(Order... orders);

        @Update
        void Update(Order... orders);

    }
    public static class Status {

        public static final int WAITING = 0;
        public static final int NOT_DELIVERED = 2;
        public static final int DELIVERED = 1;
    }
}
