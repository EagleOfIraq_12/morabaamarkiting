package com.morabaa.team.morabaamarkiting.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.activities.MainActivity;
import com.morabaa.team.morabaamarkiting.adapters.CategoryAdapter;
import com.morabaa.team.morabaamarkiting.model.CartItem;
import com.morabaa.team.morabaamarkiting.model.Category;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesFragment extends Fragment {
      
      
      List<Category> categories;
      CategoryAdapter categoryAdapter;
      SearchView txtSearchCategoriesSearch;
      private RecyclerView categoriesRecyclerView;

      public CategoriesFragment() {
            // Required empty public constructor
      }
      
      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.fragment_categories, container, false);
      }
      
      @Override
      public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            categoriesRecyclerView = view.findViewById(R.id.categoryRecyclerView);
            categories = new ArrayList<>();
            categoryAdapter = new CategoryAdapter(getContext(), categories);
            categoriesRecyclerView.setLayoutManager(
                  new GridLayoutManager(getContext(), 3, GridLayoutManager.VERTICAL, false));
            categoriesRecyclerView.setAdapter(categoryAdapter);
            MainActivity.homeFragment=false;

            txtSearchCategoriesSearch = view.findViewById(R.id.txtSearchCategoriesSearch);
            
            txtSearchCategoriesSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                  
                  @Override
                  public boolean onQueryTextSubmit(String query) {
                        onSearch(query);
                        return false;
                  }
                  
                  @Override
                  public boolean onQueryTextChange(String newText) {
                        onSearch(newText);
                        return false;
                  }
            });
            
            new HttpRequest<Category>(getContext(),
                  API.CATEGORY.GET_ALL, new JSONObject().toString(),
                    new TypeToken<List<Category>>() {}.getType()) {
                  @Override
                  public void onFinish(List<Category> newCategories) {
                        categories.addAll(newCategories);
                        System.out.println("downloaded: " +
                              new Gson().toJson(categories) + "\n" +
                              categories.getClass().getName()
                        );
                        categoryAdapter.setCategories(categories);
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
      }
      
      void onSearch(String searchKey) {
            List<Category> newCategories = new ArrayList<>();
            for (Category category : categories) {
                  if (category.getName().trim().toLowerCase()
                        .contains(searchKey.toLowerCase().trim())) {
                        newCategories.add(category);
                  }
            }
            categoryAdapter.setCategories(newCategories);
      }
      
}
