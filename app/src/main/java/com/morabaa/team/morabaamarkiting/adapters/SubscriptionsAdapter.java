package com.morabaa.team.morabaamarkiting.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.model.Shop;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.squareup.picasso.Picasso;
import java.util.List;
import org.json.JSONObject;

/**
 * Created by eagle on 1/22/2018.
 */

public class SubscriptionsAdapter extends RecyclerView.Adapter<SubscriptionsViewHolder> {
      
      ProgressDialog progress;
      boolean subscribed = true;
      private List<String> subscriptions;
      private Context ctx;
      
      public SubscriptionsAdapter(Context ctx, List<String> subscriptions) {
            this.subscriptions = subscriptions;
            this.ctx = ctx;
            System.out.println("subscriptions.size: " + this.subscriptions.size());
            
      }
      
      public List<String> getSubscriptions() {
            return subscriptions;
      }
      
      public void setSubscriptions(List<String> subscriptions) {
            this.subscriptions = subscriptions;
            notifyDataSetChanged();
      }
      
      @Override
      public SubscriptionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LinearLayout view = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(
                  R.layout.subscription_layout, parent, false);
            progress = ProgressDialog
                  .show(new ContextThemeWrapper(ctx, R.style.progressDialog), " ",
                        "يرجى الانتظار قليلاً ....", true);
            progress.setCancelable(true);
            progress.dismiss();
            return new SubscriptionsViewHolder(view);
      }
      
      @Override
      public void onBindViewHolder(@NonNull final SubscriptionsViewHolder holder,
            final int position) {
            
            long id = 0;
            id = Long.parseLong(
                  subscriptions.get(position).substring(4, subscriptions.get(position).length())
            );
            
            final long finalId = id;
            new HttpRequest<Shop>(ctx, API.SHOP.GET_SHOP_BY_ID(id), new JSONObject().toString(),
                  new TypeToken<List<Shop>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Shop> shops) {
                        if (shops.size() > 0) {
                              holder.imgSubscriptionImage.setVisibility(View.VISIBLE);
                              Shop shop = shops.get(0);
                              holder.txtSubscriptionName.setText(shop.getName());
                              holder.btnUnsubscribe.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                          progress.show();
                                          subscribe(v, finalId);
                                    }
                              });
                              
                              holder.btnUnsubscribe.setBackgroundColor(
                                    ctx.getResources()
                                          .getColor(R.color.pugnotification_color_white));
                              holder.btnUnsubscribe
                                    .setImageDrawable(ctx.getResources()
                                          .getDrawable(R.drawable.mm_unfollow_but));
                              
                              Picasso.with(ctx)
                                    .load(API.IMG_ROOT + shop.getImageUrl())
                                    .placeholder(ctx.getResources()
                                          .getDrawable(R.drawable.default_item_image))
                                    .error(ctx.getResources()
                                          .getDrawable(R.drawable.error_item_image))
                                    .into(holder.imgSubscriptionImage);
                        }
                  }
                  
                  @Override
                  public void onError(String error) {
                        System.out.println(error);
                        if (holder.getAdapterPosition() > 0
                              && holder.getAdapterPosition() < subscriptions.size()) {
                              subscriptions.remove(holder.getAdapterPosition());
                              notifyItemRemoved(holder.getAdapterPosition());
                              notifyItemRangeChanged(holder.getAdapterPosition(),
                                    subscriptions.size());
                        }
                  }
            };
      }
      
      @Override
      public int getItemCount() {
            return subscriptions.size();
      }
      
      public void addSubscription(String subscription) {
            subscriptions.add(0, subscription);
            notifyItemInserted(0);
      }
      
      public void subscribe(final View view, final long shopId) {
            
            if (subscribed) {
                  FirebaseMessaging.getInstance().unsubscribeFromTopic("Shop" + shopId);
                  FirebaseMessaging.getInstance().unsubscribeFromTopic("Shop" + 0);
            } else {
                  FirebaseMessaging.getInstance().subscribeToTopic("Shop" + shopId);
                  
            }
            view.setEnabled(false);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                  @Override
                  public void run() {
                        new HttpRequest.CheckSubscription(ctx, shopId) {
                              @Override
                              public void status(boolean sub) {
                                    if (sub) {
                                          ((ImageButton) view).setImageDrawable(ctx.getResources()
                                                .getDrawable(R.drawable.mm_unfollow_but));
                                          view.setBackgroundColor(ctx.getResources()
                                                .getColor(R.color.pugnotification_color_white));
                                    } else {
                                          ((ImageButton) view).setImageDrawable(ctx.getResources()
                                                .getDrawable(R.drawable.mm_follow_but));
                                          view.setBackgroundColor(
                                                ctx.getResources().getColor(R.color.colorAccent));
                                    }
                                    subscribed = sub;
                                    view.setEnabled(true);
                                    progress.dismiss();
                              }
                        };
                  }
            }, 2000);
      }
      
}

class SubscriptionsViewHolder extends RecyclerView.ViewHolder {
      
      LinearLayout view;
      TextView txtSubscriptionName;
      ImageView imgSubscriptionImage;
      ImageButton btnUnsubscribe;
      
      public SubscriptionsViewHolder(View itemView) {
            super(itemView);
            view = (LinearLayout) itemView;
            txtSubscriptionName = itemView.findViewById(R.id.txtSubscriptionName);
            imgSubscriptionImage = itemView.findViewById(R.id.imgSubscriptionImage);
            btnUnsubscribe = itemView.findViewById(R.id.btnUnsubscribe);
            
      }
}
