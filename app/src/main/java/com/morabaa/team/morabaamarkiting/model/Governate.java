package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by eagle on 3/10/2018.
 */
@Entity
public class Governate {
      
      @PrimaryKey
      long id;
      String governate;
      
      public Governate() {
      }
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public String getGovernate() {
            return governate;
      }
      
      public void setGovernate(String governate) {
            this.governate = governate;
      }
}
