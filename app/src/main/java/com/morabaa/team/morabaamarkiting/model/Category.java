package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;

@Entity
public class Category {
      
      @PrimaryKey
      private long id;
      private String name;
      private String imageUrl;
      @Ignore
      private List<Item> items;
      
      
      public Category() {
      }
      
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public String getName() {
            return name;
      }
      
      public void setName(String name) {
            this.name = name;
      }
      
      public String getImageUrl() {
            return imageUrl;
      }
      
      public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
      }
      
      public List<Item> getItems() {
            return items;
      }
      
      public void setItems(List<Item> items) {
            this.items = items;
      }
      
      @Dao
      public interface CategoryDao {
            
            
            @Query("SELECT * FROM Category")
            List<Category> CATEGORIES();

            @Query("SELECT * FROM Category WHERE id = :id")
            Category getCategoryByID(long id);

            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insertCategory(Category... categories);
            
            
            @Delete
            void deleteCategory(Category... categories);
            
            @Update
            void UpdateCategory(Category... categories);
            
      }
}
