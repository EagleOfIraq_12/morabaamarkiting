package com.morabaa.team.morabaamarkiting;

import android.support.v17.leanback.widget.AbstractDetailsDescriptionPresenter;

/**
 * Created by eagle on 2/11/2018.
 */

public class DetailsDescriptionPresenter
      extends AbstractDetailsDescriptionPresenter {
      
      @Override
      protected void onBindDescription(ViewHolder viewHolder, Object itemData) {
           // MyMediaItemDetails details = (MyMediaItemDetails) itemData;
            // In a production app, the itemData object contains the information
            // needed to display details for the media item:
            // viewHolder.getTitle().setText(details.getShortTitle());
            
            // Here we provide static data for testing purposes:
            viewHolder.getTitle().setText(itemData.toString());
            viewHolder.getSubtitle().setText("2014   Drama   TV-14");
            viewHolder.getBody().setText("Lorem ipsum dolor sit amet, consectetur "
                  + "adipisicing elit, sed do eiusmod tempor incididunt ut labore "
                  + " et dolore magna aliqua. Ut enim ad minim veniam, quis "
                  + "nostrud exercitation ullamco laboris nisi ut aliquip ex ea "
                  + "commodo consequat.");
            
      }
      
}
