package com.morabaa.team.morabaamarkiting.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.activities.ItemDetailsActivity;
import com.morabaa.team.morabaamarkiting.activities.ShopDetailsActivity;
import com.morabaa.team.morabaamarkiting.utils.DB;

import static com.morabaa.team.morabaamarkiting.activities.MainActivity.db;

/**
 * Created by eagle on 12/30/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        rateItemBackground(remoteMessage);

//            String title = remoteMessage.getNotification().getTitle();
//            String message = remoteMessage.getNotification().getBody();
//            String action = remoteMessage.getNotification().getClickAction();
//
//            Intent intent = new Intent(this, MainActivity.class);
//            intent.putExtra(ACTION, action);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//            PendingIntent pendingIntent = PendingIntent
//                  .getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
//            notificationBuilder.setContentTitle(title);
//            notificationBuilder.setContentText(message);
//            notificationBuilder.setSmallIcon(R.drawable.ic_launcher_background);
//            notificationBuilder.setAutoCancel(true);
//            notificationBuilder.setContentIntent(pendingIntent);
//
//            NotificationManager notificationManager = (NotificationManager) getSystemService(
//                  Context.NOTIFICATION_SERVICE);
//            assert notificationManager != null;
//            notificationManager.notify(0, notificationBuilder.build());
//            showNotification(remoteMessage);
    }

    Vibrator vibrator;

    public void rateItemBackground(RemoteMessage remoteMessage) {
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        assert vibrator != null;
        vibrator.vibrate(300);
        try {
            Thread.sleep(400);
            vibrator.vibrate(300);
        } catch (InterruptedException e) {
        }
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
        }

        if (db == null) {
            db = android.arch.persistence.room.Room.databaseBuilder(
                    this, DB.class,
                    "marketing.db"
            ).allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        long id = Long.parseLong(remoteMessage.getData().get("id"));
        String type = remoteMessage.getData().get("type");
        int notiId = (int) (System.currentTimeMillis() % Integer.MAX_VALUE);

        System.out.println("id:\t" + remoteMessage.getData().get("id"));
        if (type.equals("Item")) {
            Intent intent = new Intent(this, ItemDetailsActivity.class);
            intent.putExtra("itemId", id);
            intent.putExtra("notiId", notiId);
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.shop)
                            .setContentTitle(remoteMessage.getData().get("title"))
                            .setContentText(remoteMessage.getData().get("body"))
                            .setAutoCancel(true)
                            .setContentIntent(PendingIntent
                                    .getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));

            NotificationManager notificationManager = (NotificationManager) getSystemService(
                    Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.notify("messageId",
                    (int) (System.currentTimeMillis() % Integer.MAX_VALUE)
                    , builder.build());
        } else {

            Intent intent = new Intent(this, ShopDetailsActivity.class);
            intent.putExtra("shopId", id);
            intent.putExtra("notiId", notiId);
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.shop)
                            .setContentTitle(remoteMessage.getData().get("title"))
                            .setContentText(remoteMessage.getData().get("body"))
                            .setAutoCancel(true)
                            .setContentIntent(PendingIntent
                                    .getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));

            NotificationManager notificationManager = (NotificationManager) getSystemService(
                    Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.notify("messageId",
                    notiId
                    , builder.build());

        }
    }
}
