package com.morabaa.team.morabaamarkiting.services;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.morabaa.team.morabaamarkiting.activities.MainActivity;

/**
 * Created by eagle on 3/8/2018.
 */

public class NotificationActivity extends Activity {
      
      
      public static final String NOTIFICATION_ID = "NOTIFICATION_ID";
      public static final String FRAGMENT_TO_START = "FRAGMENT_TO_START";
      @SuppressLint("StaticFieldLeak")
      public static Context ctx;
      
      public static PendingIntent getDismissIntent(int notificationId, Context context) {
            Intent intent = new Intent(context, NotificationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(NOTIFICATION_ID, notificationId);
            return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
      }
      
      public static PendingIntent getActionIntent(String notificationFragment, Context context) {
            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra(FRAGMENT_TO_START, notificationFragment);
            ctx.startActivity(intent);
            return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
      }
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            ctx = NotificationActivity.this;
            NotificationManager manager = (NotificationManager) getSystemService(
                  NOTIFICATION_SERVICE);
            assert manager != null;
            manager.cancel(getIntent().getIntExtra(NOTIFICATION_ID, -1));
            finish(); // since finish() is called in onCreate(), onDestroy() will be called immediately
      }
}

