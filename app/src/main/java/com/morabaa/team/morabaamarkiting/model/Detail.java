package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Detail {
      
      @PrimaryKey
      private long id;
      private String name;
      private String value;
      private long parentId;
      
      public Detail() {
      }
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public String getName() {
            return name;
      }
      
      public void setName(String name) {
            this.name = name;
      }
      
      public String getValue() {
            return value;
      }
      
      public void setValue(String value) {
            this.value = value;
      }
      
      public long getParentId() {
            return parentId;
      }
      
      public void setParentId(long parentId) {
            this.parentId = parentId;
      }
}
