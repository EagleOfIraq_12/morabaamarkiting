package com.morabaa.team.morabaamarkiting.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.activities.MainActivity;
import com.morabaa.team.morabaamarkiting.adapters.ShopsAdapter;
import com.morabaa.team.morabaamarkiting.model.Shop;
import com.morabaa.team.morabaamarkiting.utils.API.SHOP;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopsFragment extends Fragment {


    RecyclerView shopsRecyclerView;
    LinearLayout layoutShopSort;
    TextView txtNoShops;
    ProgressBar shopsProgressBar;

    public ShopsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shops, container, false);
    }

    String searchKey = "-";
    long lastShopId;
    ShopsAdapter shopAdapter;
    List<Shop> shops;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        shopsRecyclerView = view.findViewById(R.id.shopsRecyclerView);
        txtNoShops = view.findViewById(R.id.txtNoShops);
        shopsProgressBar = view.findViewById(R.id.shopsProgressBar);
        layoutShopSort = view.findViewById(R.id.layoutShopSort);
        MainActivity.homeFragment = false;

        shops = new ArrayList<>();
        shopAdapter = new ShopsAdapter(getContext(), shops,
                R.layout.shop_layout_wide, getActivity());
        shopsRecyclerView.setLayoutManager(
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        shopsRecyclerView.setAdapter(shopAdapter);

        new HttpRequest<Shop>(getContext(),
                SHOP.GET_LAST_50_SHOP(lastShopId,
                        searchKey)
                , new JSONObject().toString(), new TypeToken<List<Shop>>() {
        }.getType()) {
            @Override
            public void onFinish(List<Shop> newShops) {
                shops.addAll(newShops);
                shopsProgressBar.setVisibility(View.GONE);
                if (shops != null && shops.size() > 0) {
                    txtNoShops.setVisibility(View.GONE);
                    shopsRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    txtNoShops.setVisibility(View.VISIBLE);
                    shopsRecyclerView.setVisibility(View.GONE);
                }
                System.out.println("downloaded: " +
                        new Gson().toJson(shops) + "\n" +
                        shops.getClass().getName()
                );
                shopAdapter.setShops(shops);
            }

            @Override
            public void onError(String error) {
                System.out.println("error: " + error);
                shopsProgressBar.setVisibility(View.GONE);
                if (shops != null && shops.size() > 0) {
                    txtNoShops.setVisibility(View.GONE);
                    shopsRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    txtNoShops.setVisibility(View.VISIBLE);
                    shopsRecyclerView.setVisibility(View.GONE);
                }
            }
        };

        ((SearchView) view.findViewById(R.id.txtSearchShopsSearch))
                .setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        shopAdapter.clear();
                        shops = new ArrayList<>();
                        shopsProgressBar.setVisibility(View.VISIBLE);
                        new HttpRequest<Shop>(getContext(),
                                SHOP.GET_LAST_50_SHOP(lastShopId,
                                        searchKey)
                                , new JSONObject().toString(), new TypeToken<List<Shop>>() {
                        }.getType()) {
                            @Override
                            public void onFinish(List<Shop> newShops) {
                                shopsProgressBar.setVisibility(View.GONE);
                                shops.addAll(newShops);
                                if (shops != null && shops.size() > 0) {
                                    txtNoShops.setVisibility(View.GONE);
                                    shopsRecyclerView.setVisibility(View.VISIBLE);
                                } else {
                                    txtNoShops.setVisibility(View.VISIBLE);
                                    shopsRecyclerView.setVisibility(View.GONE);
                                }
                                System.out.println("downloaded: " +
                                        new Gson().toJson(shops) + "\n" +
                                        shops.getClass().getName()
                                );
                                shopAdapter.setShops(shops);
                            }

                            @Override
                            public void onError(String error) {
                                shopsProgressBar.setVisibility(View.GONE);
                                if (shops != null && shops.size() > 0) {
                                    txtNoShops.setVisibility(View.GONE);
                                    shopsRecyclerView.setVisibility(View.VISIBLE);
                                } else {
                                    txtNoShops.setVisibility(View.VISIBLE);
                                    shopsRecyclerView.setVisibility(View.GONE);
                                }
                            }
                        };

                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        searchKey = newText;
                        if (newText.equals("")) {
                            this.onQueryTextSubmit("");
                        }
                        return false;
                    }
                });


    }

}
