package com.morabaa.team.morabaamarkiting.utils;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eagle on 2/10/2018.
 */

public abstract class HttpRequest<T> {
    
    private Context ctx;
    private String url;
    private String jsonBodyString;
    private HashMap<String, String> headers = new HashMap<>();
    private Type type;
    
    
    public HttpRequest(Context ctx, String url, String jsonBody,
          Type type) {
        this.ctx = ctx;
        this.url = url;
        this.jsonBodyString = jsonBody;
        this.headers = new HashMap<>();
        this.type = type;
        getData();
    }
    
    public HttpRequest(Context ctx, String url, String jsonBodyString) {
        this.ctx = ctx;
        this.url = url;
        this.jsonBodyString = jsonBodyString;
        System.out.print(jsonBodyString);
        setData();
    }
    
    public HttpRequest() {
    }
    
    public void getData() {
        try {
            final String requestBody = jsonBodyString;
            RequestQueue requestQueue = Volley.newRequestQueue(ctx);
            RetryPolicy retryPolicy = new DefaultRetryPolicy(
                  5000,
                  1,
                  DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            );
            
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                  new Response.Listener<String>() {
                      @Override
                      public void onResponse(String s) {
                          System.out.println("download: " + s);
                          if (!s.trim().substring(0, 1).equals("[")) {
                              s = "[" + s + "]";
                              System.out.println("download[]: " + s);
                          }
                          List<T> ts = new Gson()
                                .fromJson(s, type);
                          onFinish(ts);
                      }
                  },
                  new Response.ErrorListener() {
                      @Override
                      public void onErrorResponse(VolleyError error) {
                          Log.e("VOLLEY", error.toString());
                          onError(url + "\t" +
                                error.toString());
                      }
                  }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null
                              : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog
                              .wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                                    requestBody, "utf-8");
                        return null;
                    }
                }
                
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
                
                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    
                    String parsed;
                    try {
                        parsed = new String(response.data,
                              HttpHeaderParser.parseCharset(response.headers));
                    } catch (UnsupportedEncodingException e) {
                        parsed = new String(response.data);
                        onError(parsed);
                    }
                    return Response
                          .success(parsed, HttpHeaderParser.parseCacheHeaders(response));
                }
            };
            stringRequest.setRetryPolicy(retryPolicy);
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            onError(url + "\t" +
                  e.toString());
        }
    }
    
    public void setData() {
        final String requestBody = jsonBodyString;
        RequestQueue requestQueue = Volley.newRequestQueue(ctx);
        RetryPolicy retryPolicy = new DefaultRetryPolicy(
              5000,
              1,
              DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        );
        
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
              new Response.Listener<String>() {
                  @Override
                  public void onResponse(String s) {
                      onFinish(null);
                      System.out.println("Response: " + s);
                  }
              },
              new Response.ErrorListener() {
                  @Override
                  public void onErrorResponse(VolleyError error) {
                      Log.e("VOLLEY", error.toString());
                      onError(url + "\t" +
                            error.toString());
                  }
              }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog
                          .wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                                requestBody, "utf-8");
                    return null;
                }
            }
            
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
            
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                
                String parsed;
                try {
                    parsed = new String(response.data,
                          HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                    onError(parsed);
                }
                return Response
                      .success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        stringRequest.setRetryPolicy(retryPolicy);
        requestQueue.add(stringRequest);
        
    }
    
    public abstract void onFinish(List<T> tList);
    
    public abstract void onError(String error);
    
    public abstract static class CheckSubscription {
        Context ctx;
        long shopId;
        
        public CheckSubscription(Context ctx, long shopId) {
            this.ctx = ctx;
            this.shopId = shopId;
            checkSubscription();
        }
        
        private void checkSubscription() {
            try {
                final String requestBody = "";
                RequestQueue requestQueue = Volley
                      .newRequestQueue(ctx);
                RetryPolicy retryPolicy = new DefaultRetryPolicy(
                      5000,
                      3,
                      DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                );
                String url = "https://iid.googleapis.com/iid/info/" +
                      FirebaseInstanceId.getInstance().getToken() +
                      "?details=true";
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                      new Response.Listener<String>() {
                          @Override
                          public void onResponse(String s) {
                              try {
                                  JSONObject base = new JSONObject(s);
                                  JSONObject rel = base.getJSONObject("rel");
                                  JSONObject topics = rel.getJSONObject("topics");
                                  Iterator<String> iter = topics.keys();
                                  boolean b = false;
                                  while (iter.hasNext()) {
                                      String key = iter.next();
                                      System.out.println(key);
                                      if (key.equals("Shop" + shopId)) {
                                          b = true;
                                      }
                                  }
                                  status(b);
                              } catch (JSONException e) {
                                  e.printStackTrace();
                              }
                              
                          }
                      },
                      new Response.ErrorListener() {
                          @Override
                          public void onErrorResponse(VolleyError error) {
                              Log.e("VOLLEY", error.toString());
                              
                          }
                      }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }
                    
                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null
                                  : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog
                                  .wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                                        requestBody, "utf-8");
                            return null;
                        }
                    }
                    
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization",
                              "Key=AAAAkOfjRu0:APA91bGN8_9kUxAvbezkHcgsWemhgo7Q4eNRTGHnpfdjApNzYGaWIFn-bmVZpbTXgX2mLj-yxrDDImDBSwnjBrSzb0wOvB0YnJLOSWOXYQTp9Syc60cDIP3V95g33dtPL-Lc29arnju6");
                        headers.put("Content-Type", "application/json");
                        return headers;
                    }
                    
                    @Override
                    protected Response<String> parseNetworkResponse(
                          NetworkResponse response) {
                        
                        String parsed;
                        try {
                            parsed = new String(response.data,
                                  HttpHeaderParser.parseCharset(response.headers));
                        } catch (UnsupportedEncodingException e) {
                            parsed = new String(response.data);
                        }
                        return Response
                              .success(parsed,
                                    HttpHeaderParser.parseCacheHeaders(response));
                    }
                };
                stringRequest.setRetryPolicy(retryPolicy);
                requestQueue.add(stringRequest);
            } catch (Exception e) {
            
            }
            
        }
        
        public abstract void status(boolean subscribed);
    }
    
    public abstract static class MySubscriptions {
        
        Context ctx;
        public MySubscriptions(Context ctx) {
            this.ctx = ctx;
            getSubscriptions();
        }
        
        private void getSubscriptions() {
            try {
                final String requestBody = "";
                RequestQueue requestQueue = Volley
                      .newRequestQueue(ctx);
                RetryPolicy retryPolicy = new DefaultRetryPolicy(
                      5000,
                      3,
                      DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                );
                String url = "https://iid.googleapis.com/iid/info/" +
                      FirebaseInstanceId.getInstance().getToken() +
                      "?details=true";
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                      new Response.Listener<String>() {
                          @Override
                          public void onResponse(String s) {
                              try {
                                  JSONObject base = new JSONObject(s);
                                  JSONObject rel = base.getJSONObject("rel");
                                  JSONObject topics = rel.getJSONObject("topics");
                                  Iterator<String> iter = topics.keys();
                                  List<String> subscriptions = new ArrayList<>();
                                  
                                  while (iter.hasNext()) {
                                      String key = iter.next();
                                      System.out.println(key);
                                      if (key.contains("Shop")) {
                                          subscriptions.add(key);
                                      }
                                  }
                                  
                                  onSubscriptionsReceived(subscriptions);
                              } catch (JSONException e) {
                                  e.printStackTrace();
                              }
                              
                          }
                      },
                      new Response.ErrorListener() {
                          @Override
                          public void onErrorResponse(VolleyError error) {
                              Log.e("VOLLEY", error.toString());
                              
                          }
                      }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }
                    
                    @Override
                    public byte[] getBody() {
                        try {
                            return requestBody == null ? null
                                  : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog
                                  .wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                                        requestBody, "utf-8");
                            return null;
                        }
                    }
                    
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization",
                              "Key=AAAAkOfjRu0:APA91bGN8_9kUxAvbezkHcgsWemhgo7Q4eNRTGHnpfdjApNzYGaWIFn-bmVZpbTXgX2mLj-yxrDDImDBSwnjBrSzb0wOvB0YnJLOSWOXYQTp9Syc60cDIP3V95g33dtPL-Lc29arnju6");
                        headers.put("Content-Type", "application/json");
                        return headers;
                    }
                    
                    @Override
                    protected Response<String> parseNetworkResponse(
                          NetworkResponse response) {
                        
                        String parsed;
                        try {
                            parsed = new String(response.data,
                                  HttpHeaderParser.parseCharset(response.headers));
                        } catch (UnsupportedEncodingException e) {
                            parsed = new String(response.data);
                        }
                        return Response
                              .success(parsed,
                                    HttpHeaderParser.parseCacheHeaders(response));
                    }
                };
                stringRequest.setRetryPolicy(retryPolicy);
                requestQueue.add(stringRequest);
            } catch (Exception ignored) {
            
            }
            
        }
        
        public abstract void onSubscriptionsReceived(List<String> subscriptions);
    }
}


