package com.morabaa.team.morabaamarkiting.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import java.util.List;

/**
 * Created by eagle on 2/19/2018.
 */

public class CartPagerAdapter extends FragmentStatePagerAdapter {
      List<Fragment> fragments;
      public CartPagerAdapter(FragmentManager fm,
            List<Fragment> fragments) {
            super(fm);
            this.fragments=fragments;
      }
      
      @Override
      public Fragment getItem(int position) {
            return fragments.get(position);
      }
      
      @Override
      public int getCount() {
            return fragments.size();
      }
}