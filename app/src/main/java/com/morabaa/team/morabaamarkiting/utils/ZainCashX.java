package com.morabaa.team.morabaamarkiting.utils;

import android.content.Context;

import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class ZainCashX {


    public static void pay(Context ctx, long amount, long msisdn, long orderId, String serviceType,
                           String merchantId, String secret) {
        Map<String, Object> data = new HashMap<>();
        data.put("amount", amount);
        data.put("msisdn", msisdn);
        data.put("orderId", orderId);
        data.put("redirectUrl", "https://morabaa.com/");
        data.put("iat", new Date().getTime());
        data.put("exp", new Date().getTime() + 60 * 60 * 4);
        data.put("serviceType", serviceType);

        String newToken = generateJWT(secret, data);

        System.out.println("newToken: " + newToken);

        Map<String, String> parms = new HashMap<>();

        parms.put("token", newToken);
        parms.put("merchantId", merchantId);
        parms.put("lang", "ar");

        postToServer("https://api.zaincash.iq/transaction/init", parms);


    }

    private static void postToServer(final String url, final Map<String,
            String> parms) {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();
                        okhttp3.Request.Builder builder = new okhttp3.Request.Builder()
                                .url(url);
                        if (parms != null) {
                            okhttp3.FormBody.Builder postData = new okhttp3.FormBody.Builder();
                            for (String key : parms.keySet()) {
                                postData.add(key, parms.get(key));
                            }
                            builder.post(postData.build());
                        }
                        okhttp3.Request request = builder.build();
                        okhttp3.Response response;
                        try {
                            response = client.newCall(request).execute();
                            if (!response.isSuccessful()) {
                                System.out.println(response.message() + " " + response.toString());
                            } else {
                                if (response.body() != null) {
                                    System.out.println(response.body().string());
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).start();
    }

    private static String generateJWT(String secret, Map<String, Object> data) {
//            String secret = "$2y$10$wYDj3ZTfWAEQ/2EmyRMRj.RA3huRb3FkO5gJeHrFeasgtgY9xEBmi";

        secret = new String(Base64.encodeBase64(secret.getBytes()));

        return Jwts.builder()
                .addClaims(data)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

}
