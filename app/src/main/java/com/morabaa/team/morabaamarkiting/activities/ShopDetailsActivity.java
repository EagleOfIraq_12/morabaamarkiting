package com.morabaa.team.morabaamarkiting.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.adapters.AdsAdapter;
import com.morabaa.team.morabaamarkiting.adapters.ItemsAdapter;
import com.morabaa.team.morabaamarkiting.model.Ad;
import com.morabaa.team.morabaamarkiting.model.Item;
import com.morabaa.team.morabaamarkiting.model.Shop;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.API.AD;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.squareup.picasso.Picasso;

import java.util.List;

import org.json.JSONObject;

public class ShopDetailsActivity extends AppCompatActivity {

    RecyclerView topRatedShopItemsRecyclerView;
    RecyclerView mostRecentShopItemsRecyclerView;
    RecyclerView mostVisitedShopItemsRecyclerView;
    RecyclerView mostSoldShopItemsRecyclerView;
    ViewPager localOffersViewPager;
    private long shopId;
    private String shopName;
    private String shopImageUrl;
    private ImageButton btnSubscribe;
    private boolean subscribed = false;
    private TextView txtItemName;
    private TextView txtShopDescription;
    private TextView txtShopPhone;
    private ImageView imgShopImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_details);
        progress = ProgressDialog
                .show(new ContextThemeWrapper(ShopDetailsActivity.this, R.style.progressDialog), " ",
                        "يرجى الانتظار قليلاً ....", true);
        progress.setCancelable(true);
        progress.dismiss();
        getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        shopId = getIntent().getLongExtra("shopId", 0);
        shopName = getIntent().getStringExtra("shopName");
        shopImageUrl = getIntent().getStringExtra("shopImageUrl");
        imgShopImage = findViewById(R.id.imgShopImage);
        Picasso.with(this)
                .load(API.IMG_ROOT + shopImageUrl)
                .placeholder(this.getResources()
                        .getDrawable(R.drawable.default_item_image))
                .error(this.getResources()
                        .getDrawable(R.drawable.error_item_image))
                .into(imgShopImage);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(shopName);
        txtItemName = findViewById(R.id.txtItemName);
        txtShopDescription = findViewById(R.id.txtShopDescription);
        txtShopPhone = findViewById(R.id.txtShopPhone);
        txtItemName.setText(shopName);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fab.setVisibility(View.GONE);
        localOffersViewPager = findViewById(R.id.localOffersViewPager);

        new HttpRequest<Shop>(this, API.SHOP.GET_SHOP_BY_ID(shopId), new JSONObject().toString(),
                new TypeToken<List<Shop>>() {}.getType()) {
            @Override
            public void onFinish(List<Shop> shops) {
                setProperties(shops.get(0));

            }

            @Override
            public void onError(String error) {
                System.out.println(error);
            }
        };


        new HttpRequest<Ad>(ShopDetailsActivity.this, AD.GET_LOCAL_BY_SHOP_ID(5),
                new JSONObject().toString(),
                new TypeToken<List<Ad>>() {}.getType())

        {
            @Override
            public void onFinish(List<Ad> ads) {
                if (ads.size() < 1) {
                    ((LinearLayout) localOffersViewPager.getParent()).setVisibility(View.GONE);
                } else {
                    localOffersViewPager.setAdapter(
                            new AdsAdapter(ShopDetailsActivity.this, ads));
                }
            }

            @Override
            public void onError(String error) {

            }
        }

        ;

        btnSubscribe = findViewById(R.id.btnSubscribe);
        new HttpRequest.CheckSubscription(ShopDetailsActivity.this, shopId) {
            @Override
            public void status(boolean subscribed) {
                ShopDetailsActivity.this.subscribed = subscribed;
                if (subscribed) {
                    btnSubscribe.setImageDrawable(ShopDetailsActivity.this.getResources().getDrawable(R.drawable.mm_unfollow_but));
                    btnSubscribe.setBackgroundColor(ShopDetailsActivity.this.getResources().getColor(R.color.pugnotification_color_white));
                } else {
                    btnSubscribe.setImageDrawable(ShopDetailsActivity.this.getResources().getDrawable(R.drawable.mm_follow_but));
                    btnSubscribe.setBackgroundColor(ShopDetailsActivity.this.getResources().getColor(R.color.colorAccent));
                }
            }
        };

        //region Items
        topRatedShopItemsRecyclerView =
                findViewById(R.id.topRatedShopItemsRecyclerView);

        mostRecentShopItemsRecyclerView =

                findViewById(R.id.mostRecentShopItemsRecyclerView);

        mostVisitedShopItemsRecyclerView =

                findViewById(R.id.mostVisitedShopItemsRecyclerView);

        mostSoldShopItemsRecyclerView =

                findViewById(R.id.mostSoldShopItemsRecyclerView);

        topRatedShopItemsRecyclerView.setLayoutManager(
                new

                        LinearLayoutManager(ShopDetailsActivity.this, LinearLayoutManager.HORIZONTAL,
                        true));
        mostRecentShopItemsRecyclerView.setLayoutManager(
                new

                        LinearLayoutManager(ShopDetailsActivity.this, LinearLayoutManager.HORIZONTAL,
                        true));
        mostVisitedShopItemsRecyclerView.setLayoutManager(
                new

                        LinearLayoutManager(ShopDetailsActivity.this, LinearLayoutManager.HORIZONTAL,
                        true));
        mostSoldShopItemsRecyclerView.setLayoutManager(
                new

                        LinearLayoutManager(ShopDetailsActivity.this, LinearLayoutManager.HORIZONTAL,
                        true));
        new HttpRequest<Item>(ShopDetailsActivity.this, API.ITEM.GET_TOP_RATED(shopId),
                new JSONObject().toString(),
                new TypeToken<List<Item>>() {}.getType())

        {
            @Override
            public void onFinish(List<Item> items) {
                ItemsAdapter itemAdapter = new ItemsAdapter(ShopDetailsActivity.this, items,
                        R.layout.item_layout, ShopDetailsActivity.this);
                topRatedShopItemsRecyclerView.setAdapter(itemAdapter);

            }

            @Override
            public void onError(String error) {

            }
        }

        ;
        new HttpRequest<Item>(ShopDetailsActivity.this, API.ITEM.GET_MOST_RECENT(shopId),
                new JSONObject().toString(),
                new TypeToken<List<Item>>() {}.getType())

        {
            @Override
            public void onFinish(List<Item> items) {
                ItemsAdapter itemAdapter = new ItemsAdapter(ShopDetailsActivity.this, items,
                        R.layout.item_layout, ShopDetailsActivity.this);
                mostRecentShopItemsRecyclerView.setAdapter(itemAdapter);

            }

            @Override
            public void onError(String error) {

            }
        }

        ;
        new HttpRequest<Item>(ShopDetailsActivity.this, API.ITEM.GET_MOST_VISITED(shopId),
                new JSONObject().toString(),
                new TypeToken<List<Item>>() {}.getType())

        {
            @Override
            public void onFinish(List<Item> items) {
                ItemsAdapter itemAdapter = new ItemsAdapter(ShopDetailsActivity.this, items,
                        R.layout.item_layout, ShopDetailsActivity.this);
                mostVisitedShopItemsRecyclerView.setAdapter(itemAdapter);

            }

            @Override
            public void onError(String error) {

            }
        }

        ;
        new HttpRequest<Item>(ShopDetailsActivity.this, API.ITEM.GET_MOST_SOLD(shopId),
                new JSONObject().toString(),
                new TypeToken<List<Item>>() {}.getType())

        {
            @Override
            public void onFinish(List<Item> items) {
                ItemsAdapter itemAdapter = new ItemsAdapter(ShopDetailsActivity.this, items,
                        R.layout.item_layout, ShopDetailsActivity.this);
                mostSoldShopItemsRecyclerView.setAdapter(itemAdapter);

            }

            @Override
            public void onError(String error) {

            }
        };
        //endregion

    }

    private void setProperties(Shop shop) {
        Picasso.with(this)
                .load(API.IMG_ROOT + shop.getImageUrl())
                .placeholder(this.getResources()
                        .getDrawable(R.drawable.default_item_image))
                .error(this.getResources()
                        .getDrawable(R.drawable.error_item_image))
                .into(imgShopImage);
        txtShopDescription.setText(shop.getDescription());
//        txtShopPhone.setText(shop.getPhoneNumber());
        txtShopPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                String num = txtShopPhone.getText().toString();
                callIntent.setData(Uri.parse("tel:" + num));
                if (ActivityCompat.checkSelfPermission(ShopDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void openMap(View view) {
        double lat = 31.999394; //< latitude of the location you want to navigate to>

        double lng = 44.3516023; //< longitude of the location you want to navigate to>
        String shopName = "ما ادري";
        String format = "geo:0,0?q=" + lat + "," + lng + "(" + shopName + " )";

        Uri uri = Uri.parse(format);

        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void subscribe(final View view) {
        progress.show();
        if (subscribed) {
            FirebaseMessaging.getInstance().unsubscribeFromTopic("Shop" + shopId);
        } else {
            FirebaseMessaging.getInstance().subscribeToTopic("Shop" + shopId);

        }
        view.setEnabled(false);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new HttpRequest.CheckSubscription(ShopDetailsActivity.this, shopId) {
                    @Override
                    public void status(boolean subscribed) {
                        if (subscribed) {
                            btnSubscribe.setImageDrawable(ShopDetailsActivity.this.getResources().getDrawable(R.drawable.mm_unfollow_but));
                            btnSubscribe.setBackgroundColor(ShopDetailsActivity.this.getResources().getColor(R.color.pugnotification_color_white));
                        } else {
                            btnSubscribe.setImageDrawable(ShopDetailsActivity.this.getResources().getDrawable(R.drawable.mm_follow_but));
                            btnSubscribe.setBackgroundColor(ShopDetailsActivity.this.getResources().getColor(R.color.colorAccent));
                        }
                        ShopDetailsActivity.this.subscribed = subscribed;
                        progress.dismiss();
                        view.setEnabled(true);
                    }
                };
            }
        }, 2000);

    }

    ProgressDialog progress;

}
