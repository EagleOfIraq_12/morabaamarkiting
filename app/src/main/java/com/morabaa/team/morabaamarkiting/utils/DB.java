package com.morabaa.team.morabaamarkiting.utils;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import com.morabaa.team.morabaamarkiting.model.Ad;
import com.morabaa.team.morabaamarkiting.model.Ad.AdDao;
import com.morabaa.team.morabaamarkiting.model.Cart;
import com.morabaa.team.morabaamarkiting.model.Cart.CartDao;
import com.morabaa.team.morabaamarkiting.model.CartItem;
import com.morabaa.team.morabaamarkiting.model.CartItem.CartItemDao;
import com.morabaa.team.morabaamarkiting.model.Category;
import com.morabaa.team.morabaamarkiting.model.Category.CategoryDao;
import com.morabaa.team.morabaamarkiting.model.Comment;
import com.morabaa.team.morabaamarkiting.model.Comment.CommentDao;
import com.morabaa.team.morabaamarkiting.model.Detail;
import com.morabaa.team.morabaamarkiting.model.ImageUrl;
import com.morabaa.team.morabaamarkiting.model.ImageUrl.ImageUrlDao;
import com.morabaa.team.morabaamarkiting.model.Item;
import com.morabaa.team.morabaamarkiting.model.Item.ItemDao;
import com.morabaa.team.morabaamarkiting.model.Offer;
import com.morabaa.team.morabaamarkiting.model.Order;
import com.morabaa.team.morabaamarkiting.model.OrderItem;
import com.morabaa.team.morabaamarkiting.model.Shop;
import com.morabaa.team.morabaamarkiting.model.Shop.ShopDao;
import com.morabaa.team.morabaamarkiting.model.User;
import com.morabaa.team.morabaamarkiting.model.User.UserDao;

/**
 * Created by eagle on 2/6/2018.
 */
@Database(entities =
      {
            Cart.class,
            CartItem.class,
            Category.class,
            Comment.class,
            Detail.class,
            ImageUrl.class,
            Item.class,
            Offer.class,
            Shop.class,
            User.class,
            Order.class,
            OrderItem.class,
            Ad.class
      }, version = 16)
public abstract class DB extends RoomDatabase {
      
      public abstract CartDao cartDao();
      
      public abstract CartItemDao cartItemDao();
      
      public abstract CategoryDao categoryDao();
      
      public abstract CommentDao commentDao();
      
      public abstract ImageUrlDao imageUrlDao();
      
      public abstract ItemDao itemDao();
      
      public abstract ShopDao shopDao();
      
      public abstract UserDao userDao();
      
      public abstract AdDao adDao();
      
}
