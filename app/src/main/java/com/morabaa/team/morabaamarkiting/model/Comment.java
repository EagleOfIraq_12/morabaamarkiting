package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;
@Entity
public class Comment {
      @PrimaryKey
      private long id;
      private float rating;
      private String text;
      private String date;
      private long itemId;
      @Ignore
      private User user;
      private long userId;

      public Comment() {
      }

      public long getUserId() {
            return userId;
      }

      public void setUserId(long userId) {
            this.userId = userId;
      }

      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public float getRating() {
            return rating;
      }
      
      public void setRating(float rating) {
            this.rating = rating;
      }
      
      public String getText() {
            return text;
      }
      
      public void setText(String text) {
            this.text = text;
      }
      
      public String getDate() {
            return date;
      }
      
      public void setDate(String date) {
            this.date = date;
      }
      
      public long getItemId() {
            return itemId;
      }
      
      public void setItemId(long itemId) {
            this.itemId = itemId;
      }
      
      public User getUser() {
            return user;
      }
      
      public void setUser(User user) {
            this.user = user;
      }
      
      @Dao
      public interface CommentDao {
            
            
            @Query("SELECT * FROM Comment")
            List<Comment> COMMENTS();
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insertCategory(Comment... comments);
            
            
            @Delete
            void deleteCategory(Comment... comments);
            
            @Update
            void UpdateCategory(Comment... comments);
            
      }
}
