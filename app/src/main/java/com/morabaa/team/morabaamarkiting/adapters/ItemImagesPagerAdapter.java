package com.morabaa.team.morabaamarkiting.adapters;

/**
 * Created by eagle on 1/22/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.morabaa.team.morabaamarkiting.activities.ImageDetailsActivity;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.model.ImageUrl;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ItemImagesPagerAdapter extends PagerAdapter {

    Context ctx;
    LayoutInflater layoutInflater;
    List<ImageUrl> imageUrls;
    String title;

    public ItemImagesPagerAdapter(Context ctx, List<ImageUrl> imageUrls, String title) {
        this.ctx = ctx;
        this.imageUrls = imageUrls;
        this.title = title;
        layoutInflater = (LayoutInflater) this.ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return imageUrls.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        CardView page = (CardView) layoutInflater
                .inflate(R.layout.image_with_titel_layout, null);
        page.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, ImageDetailsActivity.class);

                intent.putExtra("path", API.IMG_ROOT + imageUrls.get(position).getUrl());
                intent.putExtra("title", title);

                ctx.startActivity(intent);

            }
        });
        ImageView imageView = page.findViewById(R.id.img);
        TextView textView = page.findViewById(R.id.txtTitle);
        textView.setText(imageUrls.get(position).getUrl());
        textView.setVisibility(View.GONE);
        Picasso.with(ctx)
                .load(API.IMG_ROOT + imageUrls.get(position).getUrl())
                .placeholder(ctx.getResources()
                        .getDrawable(R.drawable.default_item_image))
                .error(ctx.getResources()
                        .getDrawable(R.drawable.error_item_image))
                .into(imageView);

        container.addView(page);

        return page;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object page) {
        container.removeView((CardView) page);
    }
}