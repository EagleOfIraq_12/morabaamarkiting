package com.morabaa.team.morabaamarkiting.utils;

/**
 * Created by eagle on 2/10/2018.
 */

public class API {

    //    public static final String IMG_ROOT = "https://market.morabaa.com/";
    public static final String IMG_ROOT = "https://s3.us-east-2.amazonaws.com/morabaamarketimages/images/";
  public static final String ROOT = "https://market.morabaa.net/";
  //  public static final String ROOT = "http://192.168.10.123:56468/";

    public static final String API = ROOT + "Api/";

    public static class CATEGORY {

        static final String MAIN = API + "Category/";
        public static final String GET_ALL = MAIN + "getAll";

        public static String GET_CATEGORIES_BY_SHOP_ID(long shopId) {
            return MAIN + "getCategoriesByShopId?shopId=" + shopId;
        }
    }

    public static class COMMENT {

        static final String MAIN = API + "Comment/";
        public static final String ADD_COMMENT = MAIN + "ADD";

    }

    public static class DETAIL {

        public static final String MAIN = API + "Detail/";
    }

    public static class IMAGE_URL {

        static final String MAIN = API + "imageUrl/";
        public static final String ADD = MAIN + "ADD";
        public static final String ADD_USER_IMAGE = MAIN + "AddUserImage";
    }

    public static class ITEM {

        static final String MAIN = API + "Item/";

        public static final String GET_ITEM_BY_ID = MAIN + "getItemById?Id=";

        public static final String VISITED_ITEMS = MAIN + "VisitedItems";

        public static String GET_MOST_RECENT(long shopId) {
            return MAIN + "GetMostRecent?shopId=" + shopId;
        }

        public static String GET_MOST_VISITED(long shopId) {
            return MAIN + "getMostVisited?shopId=" + shopId;
        }

        public static String GET_MOST_SOLD(long shopId) {
            return MAIN + "getMostSold?shopId=" + shopId;
        }

        public static String GET_TOP_RATED(long shopId) {
            return MAIN + "getTopRated?shopId=" + shopId;
        }
          public static String GET_LAST_50_ITEM_NOT_IN = MAIN + "GetLast50ItemNotIn";

//        public static String GET_LAST_50_ITEM_NOT_IN(long governateId, long shopId,
//                                                     long categoryId,
//                                                     String searchKey, String SortType,
//              String orderType) {
//            if (searchKey.trim().equals("")) {
//                searchKey = "-";
//            }
//            return "";
//            return MAIN + "GetLast50ItemNotIn" +
//                    "?governateId=" + governateId +
//                    "&shopId=" + shopId +
//                    "&categoryId=" + categoryId +
//                    "&searchKey=" + searchKey +
//                    "&SortType=" + SortType +
//                    "&orderType=" + orderType;
//        }
    }

    public static class OFFER {

        static final String MAIN = API + "Offer/";

        public static String GET_BY_ID(long offerId) {
            return MAIN + "GetById?offerId=" + offerId;
        }

        public static String GET_OFFER_ITEMS(long offerId) {
            return MAIN + "GetOfferItemsByOfferId?offerId=" + offerId;
        }
    }

    public static class AD {
        static final String MAIN = API + "Ad/";
        public static final String GET_MAIN = MAIN + "GetMain";

        public static String GET_LOCAL_BY_SHOP_ID(long shopId) {
            return MAIN + "GetLocalByShopId?shopId=" + shopId;
        }
    }

    public static class SHOP {

        static final String MAIN = API + "Shop/";
        public static final String GET_TOP_RATED = MAIN + "getTopRated";
        public static final String GET_MOST_RECENT = MAIN + "GetMostRecent";
        public static final String GET_MOST_VISITED = MAIN + "getMostVisited";
        public static final String GET_MOST_SOLD = MAIN + "getMostSold";


        public static String GET_LAST_50_SHOP(long LastShopId,
                                              String searchKey) {
            if (searchKey.trim().equals("")) {
                searchKey = "-";
            }
            return MAIN + "GetLast50Shop" +
                    "?lastShopId=" + LastShopId +
                    "&searchKey=" + searchKey;
        }

        public static String GET_SHOP_BY_ID(long shopId) {
            return MAIN + "getShopById?shopId=" + shopId;
        }
    }

    public static class USER {

        static final String MAIN = API + "User/";
        public static final String LOGIN = MAIN + "LoginByPhoneNumber";
        //        public static final String LOGIN = MAIN + "LOGIN";
        public static final String UPDATE = MAIN + "Update";
    }

    public static class CART {

        static final String MAIN = API + "Cart/";
        public static String ADD_ORDER = MAIN + "addOrder";
        public static String CONFIRM_ORDER = MAIN + "confirmOrder?cartItemId=";
        public static String DELETE_ORDER = MAIN + "deleteOrder?cartItemId=";
        public static String DUPLICATE_ORDER = MAIN + "duplicateOrder?cartItemId=";

        public static String GET_USER_CART_ITEMS_BY_STATUS_TYPE(long userId, int StatusType) {
            return MAIN + "getUserCartItemsByStatusType?" +
                    "userId=" + userId +
                    "&StatusType=" + StatusType;

        }

    }

    public static class CART_ITEM {

        public static final String MAIN = API + "CartItem/";


    }

    public static class GOVERNATE {

        public static final String MAIN = API + "governate/";
        public static final String GET_ALL = MAIN + "getAll";

    }

    public static class ORDER {

        public static final String MAIN = API + "Order/";

        public static String GET_ALL(long userId, int status) {
            return MAIN + "getByUser" +
                    "?userId=" + userId +
                    "&status=" + status;
        }

        public static final String MAKE_ORDER = MAIN + "MakeOrder";
        public static final String HIDE_ORDER = MAIN + "HideOrder";

        public static String REMOVE_ORDER_ITEM(long orderItemId) {
            return MAIN + "RemoveOrderItem?orderItemId=" + orderItemId;
        }


        public static String GET_ORDER_ITEMS(long userId) {
            return MAIN + "getOrderItemsByUser" +
                    "?userId=" + userId;
        }

        public static final String ADD_ORDER_ITEM = MAIN + "AddOrderItem";
        public static final String REMOVE_ORDER_ITEM = MAIN + "RemoveOrderItem";
    }

    public static class ORDER_ITEM {
        public static final String MAIN = API + "OrderItem/";


    }

}
