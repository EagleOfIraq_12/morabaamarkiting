package com.morabaa.team.morabaamarkiting.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.model.Comment;
import java.util.List;

/**
 * Created by eagle on 1/22/2018.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsViewHolder> {
      
      private List<Comment> comments;
      private Context context;
      
      public CommentsAdapter(Context context, List<Comment> comments) {
            this.comments = comments;
            this.context = context;
      }
      
      public List<Comment> getComments() {
            return comments;
      }
      
      public void setComments(List<Comment> comments) {
            this.comments = comments;
            notifyDataSetChanged();
      }
      
      @Override
      public CommentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                  R.layout.comment_layout, parent, false);
            return new CommentsViewHolder(view);
      }
      
      @Override
      public void onBindViewHolder(CommentsViewHolder holder, final int position) {
            
            if (comments.get(position).getText() != null
                  && comments.get(position).getText().length() < 1) {
                  holder.view.setVisibility(View.GONE);
                  holder.view.getLayoutParams().height = 0;
                  
            }
            
            holder.txtUserComment.setText(
                  comments.get(position).getText()
            );
            
            if (comments.get(position).getUser() != null) {
                  holder.txtUserName.setText(
                        comments.get(position).getUser().getName()
                  );
            } else {
                  holder.txtUserName.setText("");
            }
            
            try {
                  holder.txtUserCommentDate.setText(
                        comments.get(position).getDate().substring(0, 16)
                  );
            } catch (Exception e) {
                  holder.txtUserCommentDate.setText("");
            }
            holder.ratingUserRating.setRating(
                  comments.get(position).getRating()
            );
      }
      
      @Override
      public int getItemCount() {
            return comments.size();
      }
      
      public void addComment(Comment comment) {
            comments.add(0, comment);
            notifyItemInserted(0);
      }
}

class CommentsViewHolder extends RecyclerView.ViewHolder {
      
      CardView view;
      TextView txtUserName;
      RatingBar ratingUserRating;
      TextView txtUserComment;
      TextView txtUserCommentDate;
      
      public CommentsViewHolder(View itemView) {
            super(itemView);
            view = (CardView) itemView;
            txtUserName = itemView.findViewById(R.id.txtUserName);
            ratingUserRating = itemView.findViewById(R.id.ratingUserRating);
            txtUserComment = itemView.findViewById(R.id.txtUserComment);
            txtUserCommentDate = itemView.findViewById(R.id.txtUserCommentDate);
            
            
      }
}
