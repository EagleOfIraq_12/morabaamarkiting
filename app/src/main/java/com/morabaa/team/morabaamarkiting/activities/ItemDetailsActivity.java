package com.morabaa.team.morabaamarkiting.activities;

import static com.morabaa.team.morabaamarkiting.activities.MainActivity.db;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderLayout.PresetIndicators;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.OnSliderClickListener;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.ScaleType;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.adapters.CommentsAdapter;
import com.morabaa.team.morabaamarkiting.fragments.BuyDialogFragment;
import com.morabaa.team.morabaamarkiting.model.CartItem;
import com.morabaa.team.morabaamarkiting.model.Category;
import com.morabaa.team.morabaamarkiting.model.Comment;
import com.morabaa.team.morabaamarkiting.model.ImageUrl;
import com.morabaa.team.morabaamarkiting.model.Item;
import com.morabaa.team.morabaamarkiting.model.OrderItem;
import com.morabaa.team.morabaamarkiting.model.User;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.DB;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.squareup.picasso.Picasso;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.json.JSONObject;

public class ItemDetailsActivity extends AppCompatActivity {
      
      
      // View name of the header title. Used for activity scene transitions
      public static final String VIEW_NAME_HEADER_TITLE = "detail:header:title";
      public static final String VIEW_NAME_HEADER_IMAGE = "detail:header:image";
      public static final String VIEW_PRICE_TEXT = "viewPriceText";
      public static final String VIEW_DISCOUNT_PRICE_TEXT = "viewDiscountPriceText";
      public static final String VIEW_RATING = "viewRating";
      
      RecyclerView commentsRecyclerView;
      Item item;
      Activity thisActivity;
      RatingBar ratingItemRating;
      TextView txtItemName;
      //    TextView txtItemNameHeader;
      TextView txtItemDescription;
      TextView txtItemPrice;
      TextView txtItemPriceDiscount;
      LinearLayout addContentContainer;
      LinearLayout layoutSignIn;
      LinearLayout layoutFooter;
      RatingBar ratingAdd;
      EditText txtAddComment;
      Button btnAddComment;
      ImageButton btnAddToCart;
      ImageView imgItemImage;
      SliderLayout sliderItemImages;
      CommentsAdapter commentsAdapter;
      TextView txtCategoryName;
      TextView txtShopName;
      ImageView imgShopImage;
      ImageView imgCategoryImage;
//      private ViewPager imagesPager;
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            thisActivity = this;
            setContentView(R.layout.activity_item_details);
            Intent intent = getIntent();
            item = new Gson().fromJson(intent.getStringExtra("itemString"), Item.class);
            NotificationManager notificationManager = (NotificationManager) getSystemService(
                  Context.NOTIFICATION_SERVICE);
            if (intent.getIntExtra("notiId", 0) != 0 && notificationManager != null) {
                  int notiId = intent.getIntExtra("notiId", 0);
                  notificationManager.cancel(notiId);
            }

//        imagesPager = findViewById(R.id.imagesPager);
//            imagesPager = new ViewPager(this);
            
            //region init views
            commentsRecyclerView = findViewById(R.id.commentsRecyclerView);
//            viewPagerItemImages = findViewById(R.id.viewPagerItemImages);
//            imagesCyclicView = findViewById(R.id.imagesCyclicView);
            ratingItemRating = findViewById(R.id.ratingItemRating);
            txtItemName = findViewById(R.id.txtItemName);
//        txtItemNameHeader = findViewById(R.id.txtItemNameHeader);
            txtItemDescription = findViewById(R.id.txtItemDescription);
            txtItemPrice = findViewById(R.id.txtItemPrice);
            txtItemPriceDiscount = findViewById(R.id.txtItemPriceDiscount);
            addContentContainer = findViewById(R.id.addContentContainer);
            layoutSignIn = findViewById(R.id.layoutSignIn);
            layoutFooter = findViewById(R.id.layoutFooter);
            ratingAdd = findViewById(R.id.ratingAdd);
            txtAddComment = findViewById(R.id.txtAddComment);
            btnAddComment = findViewById(R.id.btnAddComment);
            btnAddToCart = findViewById(R.id.btnAddToCart);
            imgItemImage = findViewById(R.id.imgItemImage);
            sliderItemImages = findViewById(R.id.sliderItemImages);
            
            txtCategoryName = findViewById(R.id.txtCategoryName);
            txtShopName = findViewById(R.id.txtShopName);
            imgShopImage = findViewById(R.id.imgShopImage);
            imgCategoryImage = findViewById(R.id.imgCategoryImage);
            //endregion
            try {
                  setInitProperties(item);
            } catch (Exception e) {
                  System.out.println(e.getMessage());
            }
            if (User.IsLoggedIn(this) && User.getCurrentUser(this).getName() != null) {
                  addContentContainer.setVisibility(View.VISIBLE);
                  layoutSignIn.setVisibility(View.GONE);
            } else {
                  addContentContainer.setVisibility(View.GONE);
                  layoutSignIn.setVisibility(View.VISIBLE);
                  layoutSignIn.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                              Intent intent1 = new Intent(ItemDetailsActivity.this,
                                    LoginActivity.class);
                              intent1.putExtra("TAG", true);
                              startActivity(intent1);
                        }
                  });
            }
            
            getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            
            if (getSupportActionBar() != null) {
                  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
            
            txtAddComment.addTextChangedListener(new TextWatcher() {
                  
                  public void afterTextChanged(Editable s) {
                        int c = (s.toString().split("\n").length) + 1;
                        txtAddComment.setLines(c);
                  }
                  
                  public void beforeTextChanged(CharSequence s, int start,
                        int count, int after) {
                        
                  }
                  
                  public void onTextChanged(CharSequence s, int start,
                        int before, int count) {
                  }
            });
            
//            setTitle(item.getName());
            
            ViewCompat.setTransitionName(imgItemImage, VIEW_NAME_HEADER_IMAGE);
            ViewCompat.setTransitionName(sliderItemImages, VIEW_NAME_HEADER_IMAGE);
            ViewCompat.setTransitionName(txtItemName, VIEW_NAME_HEADER_TITLE);
            ViewCompat.setTransitionName(txtItemPrice, VIEW_PRICE_TEXT);
            ViewCompat.setTransitionName(txtItemPriceDiscount, VIEW_DISCOUNT_PRICE_TEXT);
            ViewCompat.setTransitionName(ratingItemRating, VIEW_RATING);
            
            new HttpRequest<Item>(this, API.ITEM.GET_ITEM_BY_ID + item.getId(),
                  new JSONObject().toString(),
                  new TypeToken<List<Item>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Item> items) {
                        item = items.get(0);
                        setProperties(item);
                  }
                  
                  @Override
                  public void onError(String error) {
                        System.out.println(error);
                  }
            };
      }
      
      private void buy() {
            final BuyDialogFragment myDiag =
                  BuyDialogFragment.newInstance(1, item.getName());
            final String itemName = item.getName();
            Bundle bundle = new Bundle();
            bundle.putString("itemName", itemName);
            myDiag.show(getFragmentManager(), "Diag");
            myDiag.setOnOrderListener(new BuyDialogFragment.OnOrderListener() {
                  @Override
                  public void OnOrderListener(int count, String notes) {
                        final OrderItem orderItem = new OrderItem();
                        orderItem.setUserId(
                              User.getCurrentUser(ItemDetailsActivity.this).getId()
                        );
                        orderItem.setItemId(item.getId());
                        orderItem.setCount(count);
                        new HttpRequest<OrderItem>(ItemDetailsActivity.this,
                              API.ORDER.ADD_ORDER_ITEM,
                              new Gson().toJson(orderItem, OrderItem.class),
                              new TypeToken<List<OrderItem>>() {
                              }.getType()) {
                              @Override
                              public void onFinish(List<OrderItem> orderItems) {
                                    Toast.makeText(thisActivity,
                                          "Order " + orderItems.get(0).getId() + "\t" + itemName
                                                + " added", Toast.LENGTH_SHORT).show();
                              }
                              
                              @Override
                              public void onError(String error) {
                                    
                              }
                        };
                        
                        CartItem cartItem = new CartItem();
                        final Item item1 = new Item();
                        item1.setId(item.getId());
                        cartItem.setItem(item1);
                        cartItem.setNotes(notes);
                        cartItem.setCount(count);
                        cartItem.setUserId(User.getCurrentUser(ItemDetailsActivity.this).getId());
                        cartItem.setStatus(CartItem.Status.WAITING);
                        String data = new Gson().toJson(cartItem, CartItem.class);
                        new HttpRequest<CartItem>(ItemDetailsActivity.this, API.CART.ADD_ORDER,
                              data,
                              new TypeToken<List<CartItem>>() {
                              }.getType()) {
                              @Override
                              public void onFinish(List<CartItem> cartItems) {
                                    myDiag.dismiss();
                                    Snackbar
                                          .make(layoutFooter, " تمَ ارسال الطلب. " + " " + itemName,
                                                Snackbar.LENGTH_LONG)
                                          .setAction("السله", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                      Intent intent = new Intent(
                                                            ItemDetailsActivity.this,
                                                            CartActivity.class);
                                                      startActivity(intent);
                                                }
                                          }).show();
                              }
                              
                              @Override
                              public void onError(String error) {
                                    myDiag.dismiss();
                                    Snackbar.make(layoutFooter, "فشلَ ارسال الطلب.",
                                          Snackbar.LENGTH_LONG)
                                          .setAction("Action", null).show();
                              }
                        };
                  }
            });
      }
      
      @SuppressLint("SetTextI18n")
      private void setInitProperties(final Item item) {
            
            Picasso.with(ItemDetailsActivity.this)
                  .load(API.IMG_ROOT + item.getImageUrl())
                  .placeholder(ItemDetailsActivity.this.getResources()
                        .getDrawable(R.drawable.default_item_image))
                  .error(ItemDetailsActivity.this.getResources()
                        .getDrawable(R.drawable.error_item_image))
                  .into(imgItemImage);
            imgItemImage.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        Intent imageIntent = new Intent(ItemDetailsActivity.this,
                              ImageDetailsActivity.class);
                        imageIntent.putExtra("title", item.getName());
                        imageIntent.putExtra("path", item.getImageUrl());
                        startActivity(imageIntent);
                  }
            });
            
            btnAddToCart.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        if (User.IsLoggedIn(ItemDetailsActivity.this)) {
                              if (User.checkUserDetails(ItemDetailsActivity.this)) {
                                    buy();
                              } else {
                                    Intent intent = new Intent(ItemDetailsActivity.this,
                                          LoginActivity.class);
                                    intent.putExtra("TAG", true);
                                    startActivity(intent);
                              }
                        } else {
                              Intent intent = new Intent(ItemDetailsActivity.this,
                                    LoginActivity.class);
                              intent.putExtra("TAG", true);
                              startActivity(intent);
                        }
                  }
            });
            
            btnAddComment.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        final Comment comment = new Comment();
                        comment.setDate(new Date().toString());
                        comment.setText(txtAddComment.getText().toString());
                        comment.setRating(ratingAdd.getRating());
                        comment.setItemId(item.getId());
                        comment.setUser(User.getCurrentUser(ItemDetailsActivity.this));
                        String commentString = new Gson().toJson(comment, Comment.class);
                        btnAddComment.setEnabled(false);
                        txtAddComment.setEnabled(false);
                        new HttpRequest<Comment>(ItemDetailsActivity.this, API.COMMENT.ADD_COMMENT,
                              commentString) {
                              @Override
                              public void onFinish(List<Comment> c) {
                                    if (comment.getText().length() > 0) {
                                          commentsAdapter.addComment(comment);
                                          btnAddComment.setEnabled(true);
                                          txtAddComment.setEnabled(true);
                                    }
                                    Snackbar snackbar = Snackbar
                                          .make(layoutFooter, "تم اضافة تقييمك",
                                                Snackbar.LENGTH_LONG)
                                          .setAction("Action", null);
                                    snackbar.getView().setBackgroundColor(
                                          getResources().getColor(R.color.colorAccent));
                                    snackbar.show();
                                    txtAddComment.setText("");
                              }
                              
                              @Override
                              public void onError(String error) {
                                    btnAddComment.setEnabled(true);
                                    txtAddComment.setEnabled(true);
                                    Snackbar snackbar = Snackbar
                                          .make(layoutFooter, "فشل اضافة تقييمك",
                                                Snackbar.LENGTH_LONG)
                                          .setAction("Action", null);
                                    snackbar.getView().setBackgroundColor(Color.RED);
                                    snackbar.show();
                                    txtAddComment.setText("");
                              }
                        };
                  }
            });

//            imagesPager.setAdapter(
//                  new ItemImagesPagerAdapter(
//                        ItemDetailsActivity.this, item.getImageUrls(), item.getName()));
            setTitle(item.getName());
            
            ratingItemRating.setRating(item.getRating());
            txtItemName.setText(item.getName());
            txtItemDescription.setText(item.getDescription());
            
            txtItemPrice.setText(item.getPrice() + " د.ع ");
            txtItemPriceDiscount.setText("");
            
            if (item.getOfferItems() != null &&
                  item.getOfferItems().size() > 0) {
                  txtItemPrice.setTextColor(Color.RED);
                  txtItemPrice.setPaintFlags(txtItemPrice.getPaintFlags()
                        | Paint.STRIKE_THRU_TEXT_FLAG);
                  float priceDiscount = (float) (item.getPrice() -
                        item.getPrice() *
                              item.getOfferItems().get(0).getDiscount() *
                              0.01f);
                  txtItemPriceDiscount.setText(
                        String.format("%.2f", priceDiscount, Locale.US) + " د.ع"
                  );
            } else {
                  txtItemPriceDiscount.setText("");
                  txtItemPrice.setPaintFlags(Paint.HINTING_OFF);
                  txtItemPrice.setTextColor(Color.BLACK);
            }
            
            if (db == null) {
                  db = android.arch.persistence.room.Room.databaseBuilder(
                        this, DB.class,
                        "marketing.db"
                  ).allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build();
            }
            Category category = MainActivity.db.categoryDao().getCategoryByID(item.getCategoryId());
            
            if (category != null) {
                  txtCategoryName.setText(category.getName());
                  Picasso.with(ItemDetailsActivity.this)
                        .load(API.IMG_ROOT + category.getImageUrl())
                        .placeholder(ItemDetailsActivity.this.getResources()
                              .getDrawable(R.drawable.default_item_image))
                        .error(ItemDetailsActivity.this.getResources()
                              .getDrawable(R.drawable.error_item_image))
                        .into(imgCategoryImage);
            }
      }
      
      private void setProperties(final Item item) {
            if (item.getImageUrls() == null) {
                  Picasso.with(ItemDetailsActivity.this)
                        .load(API.IMG_ROOT + item.getImageUrl())
                        .placeholder(ItemDetailsActivity.this.getResources()
                              .getDrawable(R.drawable.default_item_image))
                        .error(ItemDetailsActivity.this.getResources()
                              .getDrawable(R.drawable.error_item_image))
                        .into(imgItemImage);
                  imgItemImage.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                              Intent imageIntent = new Intent(ItemDetailsActivity.this,
                                    ImageDetailsActivity.class);
                              imageIntent.putExtra("title", item.getName());
                              imageIntent.putExtra("path", item.getImageUrl());
                              startActivity(imageIntent);
                        }
                  });
            } else {
                  imgItemImage.setVisibility(View.GONE);
                  sliderItemImages.setVisibility(View.VISIBLE);
                  
                  for (final ImageUrl imageUrl : item.getImageUrls()) {
                        TextSliderView textSliderView = new TextSliderView(
                              ItemDetailsActivity.this);
                        // initialize a SliderLayout
                        textSliderView
                              .description(""/*imageUrl.getUrl()*/)
                              .image(API.IMG_ROOT + imageUrl.getUrl())
                              .setOnSliderClickListener(new OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {
                                          Intent intent = new Intent(ItemDetailsActivity.this,
                                                ImageDetailsActivity.class);
                                          intent.putExtra("title", item.getName());
                                          intent.putExtra("path", imageUrl.getUrl());
                                          startActivity(intent);
                                    }
                              })
                              .setScaleType(ScaleType.CenterInside);
                        
                        //add your extra information
                        textSliderView.getView().setVisibility(View.GONE);
                        textSliderView.bundle(new Bundle());
                        textSliderView.getBundle()
                              .putString("extra", ""/* imageUrl.getUrl()*/);
                        
                        sliderItemImages.addSlider(textSliderView);
                  }
                  
                  sliderItemImages.setPresetTransformer(SliderLayout.Transformer.Default);
                  sliderItemImages.setPresetIndicator(PresetIndicators.Center_Bottom);
                  sliderItemImages.getPagerIndicator().setDefaultIndicatorColor(
                        getResources().getColor(R.color.colorAccent),
                        getResources().getColor(R.color.colorPrimaryTrans)
                  );
                  sliderItemImages.getPagerIndicator().setBackground(null);
                  sliderItemImages.setCustomAnimation(new DescriptionAnimation());
                  sliderItemImages.setDuration(4000);
                  sliderItemImages.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset,
                              int positionOffsetPixels) {
                              
                        }
                        
                        @Override
                        public void onPageSelected(int position) {
                        
                        }
                        
                        @Override
                        public void onPageScrollStateChanged(int state) {
                        
                        }
                  });
            }
            btnAddToCart.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        if (User.IsLoggedIn(ItemDetailsActivity.this)) {
                              if (User.checkUserDetails(ItemDetailsActivity.this)) {
                                    buy();
                              } else {
                                    Intent intent = new Intent(ItemDetailsActivity.this,
                                          LoginActivity.class);
                                    intent.putExtra("TAG", true);
                                    startActivity(intent);
                              }
                        } else {
                              Intent intent = new Intent(ItemDetailsActivity.this,
                                    LoginActivity.class);
                              intent.putExtra("TAG", true);
                              startActivity(intent);
                        }
                  }
            });
            
            btnAddComment.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        final Comment comment = new Comment();
                        comment.setDate(new Date().toString());
                        comment.setText(txtAddComment.getText().toString());
                        comment.setRating(ratingAdd.getRating());
                        comment.setItemId(item.getId());
                        comment.setUser(User.getCurrentUser(ItemDetailsActivity.this));
                        String commentString = new Gson().toJson(comment, Comment.class);
                        btnAddComment.setEnabled(false);
                        txtAddComment.setEnabled(false);
                        new HttpRequest<Comment>(ItemDetailsActivity.this, API.COMMENT.ADD_COMMENT,
                              commentString) {
                              @Override
                              public void onFinish(List<Comment> c) {
                                    if (comment.getText().length() > 0) {
                                          commentsAdapter.addComment(comment);
                                          btnAddComment.setEnabled(true);
                                          txtAddComment.setEnabled(true);
                                    }
                                    Snackbar snackbar = Snackbar
                                          .make(layoutFooter, "تم اضافة تقييمك",
                                                Snackbar.LENGTH_LONG)
                                          .setAction("Action", null);
                                    snackbar.getView().setBackgroundColor(
                                          getResources().getColor(R.color.colorAccent));
                                    snackbar.show();
                                    txtAddComment.setText("");
                              }
                              
                              @Override
                              public void onError(String error) {
                                    btnAddComment.setEnabled(true);
                                    txtAddComment.setEnabled(true);
                                    Snackbar snackbar = Snackbar
                                          .make(layoutFooter, "فشل اضافة تقييمك",
                                                Snackbar.LENGTH_LONG)
                                          .setAction("Action", null);
                                    snackbar.getView().setBackgroundColor(Color.RED);
                                    snackbar.show();
                                    txtAddComment.setText("");
                              }
                        };
                  }
            });

//            imagesPager.setAdapter(
//                  new ItemImagesPagerAdapter(
//                        ItemDetailsActivity.this, item.getImageUrls(), item.getName()));
            setTitle(item.getName());
            
            ratingItemRating.setRating(item.getRating());
            txtItemName.setText(item.getName());
            txtItemDescription.setText(item.getDescription());
            
            txtItemPrice.setText(item.getPrice() + " د.ع ");
            txtItemPriceDiscount.setText("");
            
            if (item.getOfferItems() != null &&
                  item.getOfferItems().size() > 0) {
                  txtItemPrice.setTextColor(Color.RED);
                  txtItemPrice.setPaintFlags(txtItemPrice.getPaintFlags()
                        | Paint.STRIKE_THRU_TEXT_FLAG);
                  float priceDiscount = (float) (item.getPrice() -
                        item.getPrice() *
                              item.getOfferItems().get(0).getDiscount() *
                              0.01f);
                  txtItemPriceDiscount.setText(
                        String.format("%.2f", priceDiscount, Locale.US) + " د.ع"
                  );
            } else {
                  txtItemPriceDiscount.setText("");
                  txtItemPrice.setPaintFlags(Paint.HINTING_OFF);
                  txtItemPrice.setTextColor(Color.BLACK);
            }

//        if (item.getOfferItems() != null &&
//                item.getOfferItems().size() > 0) {
//            txtItemPrice.setTextColor(Color.RED);
//            txtItemPrice.setPaintFlags(txtItemPrice.getPaintFlags()
//                    | Paint.STRIKE_THRU_TEXT_FLAG);
//            float priceDiscount = (float) (item.getPrice() -
//                    item.getPrice() *
//                            item.getOfferItems().get(0).getDiscount() *
//                            0.01f);
//            txtItemPriceDiscount.setText(
//                    String.format("%.2f", priceDiscount, Locale.US) + " د.ع"
//            );
//        } else {
//            txtItemPriceDiscount.setVisibility(View.INVISIBLE);
//        }
            
            txtShopName.setText(item.getShop().getName());
            Picasso.with(ItemDetailsActivity.this)
                  .load(API.IMG_ROOT + item.getShop().getImageUrl())
                  .placeholder(ItemDetailsActivity.this.getResources()
                        .getDrawable(R.drawable.default_item_image))
                  .error(ItemDetailsActivity.this.getResources()
                        .getDrawable(R.drawable.error_item_image))
                  .into(imgShopImage);
            txtShopName.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        Intent intent = new Intent(ItemDetailsActivity.this,
                              ShopDetailsActivity.class);
                        intent.putExtra("shopId", item.getShop().getId());
                        intent.putExtra("shopName", item.getShop().getName());
                        intent.putExtra("shopImageUrl", item.getShop().getImageUrl());
                        startActivity(intent);
                  }
            });
            if (db == null) {
                  db = android.arch.persistence.room.Room.databaseBuilder(
                        this, DB.class,
                        "marketing.db"
                  ).allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build();
            }
            Category category = MainActivity.db.categoryDao().getCategoryByID(item.getCategoryId());
            
            if (category != null) {
                  txtCategoryName.setText(category.getName());
                  Picasso.with(ItemDetailsActivity.this)
                        .load(API.IMG_ROOT + category.getImageUrl())
                        .placeholder(ItemDetailsActivity.this.getResources()
                              .getDrawable(R.drawable.default_item_image))
                        .error(ItemDetailsActivity.this.getResources()
                              .getDrawable(R.drawable.error_item_image))
                        .into(imgCategoryImage);
            }

//            txtCategoryName.setOnClickListener(new OnClickListener() {
//                  @SuppressLint("RestrictedApi")
//                  @Override
//                  public void onClick(View view) {
//                        ((BottomNavigationItemView) mainActivity
//                              .findViewById(R.id.navigation_categories)).setChecked(false);
//                        ((BottomNavigationViewEx) mainActivity.findViewById(R.id.navMain))
//                              .setSelectedItemId(R.id.navigation_items);
//
//                        ((BottomNavigationItemView) mainActivity
//                              .findViewById(R.id.navigation_items)).setChecked(true);
//
//                        ItemsFragment itemsFragment = new ItemsFragment();
//                        Bundle bundle = new Bundle();
//                        SV.SHOP_ID = 0;
//                        SV.CATEGORY_ID = item.getCategoryId();
//                        SV.SEARCH_NAME = item.getCategoryId()+"";
//                        bundle.putLong("categoryId", item.getCategoryId());
//                        itemsFragment.setArguments(bundle);
//                        fragmentManager.beginTransaction()
//                              .replace(R.id.mainFrame, itemsFragment)
//                              .addToBackStack(null).commit();
//                  }
//            });
            commentsRecyclerView
                  .setLayoutManager(new LinearLayoutManager(ItemDetailsActivity.this));
            
            commentsAdapter = new CommentsAdapter(ItemDetailsActivity.this,
                  item.getComments());
            commentsRecyclerView.setAdapter(commentsAdapter);
      }
      
      @Override
      public void onBackPressed() {
            super.onBackPressed();
      }
}
