package com.morabaa.team.morabaamarkiting.fragments;


import static com.morabaa.team.morabaamarkiting.activities.MainActivity.db;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.activities.ItemDetailsActivity;
import com.morabaa.team.morabaamarkiting.activities.MainActivity;
import com.morabaa.team.morabaamarkiting.activities.OfferDetailsActivity;
import com.morabaa.team.morabaamarkiting.activities.ShopDetailsActivity;
import com.morabaa.team.morabaamarkiting.adapters.ItemsAdapter;
import com.morabaa.team.morabaamarkiting.adapters.ShopsAdapter;
import com.morabaa.team.morabaamarkiting.model.Ad;
import com.morabaa.team.morabaamarkiting.model.Item;
import com.morabaa.team.morabaamarkiting.model.Shop;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.API.AD;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.morabaa.team.morabaamarkiting.utils.SV;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
      
      RecyclerView topRatedItemsRecyclerView;
      RecyclerView mostRecentItemsRecyclerView;
      RecyclerView mostVisitedItemsRecyclerView;
      RecyclerView mostSoldItemsRecyclerView;
      ProgressBar topRatedItemsProgressBar;
      ProgressBar mostRecentItemsProgressBar;
      ProgressBar mostVisitedItemsProgressBar;
      ProgressBar mostSoldItemsProgressBar;
      RecyclerView topRatedShopsRecyclerView;
      RecyclerView mostRecentShopsRecyclerView;
      RecyclerView mostVisitedShopsRecyclerView;
      RecyclerView mostSoldShopsRecyclerView;
      LinearLayout btnCategories;
      LinearLayout btnItems;
      LinearLayout btnShops;
      FragmentManager fragmentManager;
      private SliderLayout mDemoSlider;
      
      public HomeFragment() {
            // Required empty public constructor
      }
      
      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            
            return inflater.inflate(R.layout.fragment_home, container, false);
      }
      
      @SuppressLint("ClickableViewAccessibility")
      @Override
      public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            mDemoSlider = view.findViewById(R.id.slider);
            
            btnCategories = view.findViewById(R.id.btnCategories);
            btnItems = view.findViewById(R.id.btnItems);
            btnShops = view.findViewById(R.id.btnShops);
            fragmentManager = getActivity().getSupportFragmentManager();
            MainActivity.homeFragment = true;
            btnCategories.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        CategoriesFragment categoriesFragment = new CategoriesFragment();
                        fragmentManager.beginTransaction()
                              .replace(R.id.mainFrame, categoriesFragment)
                              .addToBackStack(null).commit();
                  }
            });
            btnItems.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        ItemsFragment itemsFragment = new ItemsFragment();
                        SV.CATEGORY_ID = 0;
                        SV.SHOP_ID = 0;
                        fragmentManager.beginTransaction()
                              .replace(R.id.mainFrame, itemsFragment)
                              .addToBackStack(null).commit();
                  }
            });
            btnShops.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        ShopsFragment shopsFragment = new ShopsFragment();
                        fragmentManager.beginTransaction()
                              .replace(R.id.mainFrame, shopsFragment)
                              .addToBackStack(null).commit();
                  }
            });

//        adsViewPager = view.findViewById(R.id.adsViewPager);
            
            new HttpRequest<Ad>(getContext(), AD.GET_MAIN, new JSONObject().toString(),
                  new TypeToken<List<Ad>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(final List<Ad> ads) {
                        db.adDao().deleteAll();
                        showAds(ads, true);
                        for (Ad ad : ads) {
                              db.adDao().insert(ad);
                        }
//                adsViewPager.setAdapter(
//                        new AdsAdapter(getActivity(), ads));
//                timer = timer();
                  }
                  
                  @Override
                  public void onError(String error) {
                        if (db.adDao().COUNT() > 0) {
                              showAds(db.adDao().ADS(), false);
                        }
                  }
            };

//        new Ad.Data() {
//            @Override
//            public void onDataReceived(List<Ad> ads) {
//                adsViewPager.setAdapter(
//                        new AdsAdapter(getActivity(), ads));
//                timer = timer();
//            }
//        }.getData(getContext());

//        adsViewPager.setOnTouchListener(
//                new OnTouchListener() {
//                    public boolean onTouch(View view, MotionEvent event) {
//                        if (event.getAction() == MotionEvent.ACTION_UP) {
//                            timer.cancel();
//                            return false;
//                        } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                            timer = timer();
//                            return true;
//                        }
//                        return false;
//                    }
//                }
//        );
            
            //region Items
            topRatedItemsRecyclerView = view.findViewById(R.id.topRatedItemsRecyclerView);
            topRatedItemsProgressBar = view.findViewById(R.id.topRatedItemsProgressBar);
            
            mostRecentItemsRecyclerView = view.findViewById(R.id.mostRecentItemsRecyclerView);
            mostRecentItemsProgressBar = view.findViewById(R.id.mostRecentItemsProgressBar);
            
            mostVisitedItemsRecyclerView = view.findViewById(R.id.mostVisitedItemsRecyclerView);
            mostVisitedItemsProgressBar = view.findViewById(R.id.mostVisitedItemsProgressBar);
            
            mostSoldItemsRecyclerView = view.findViewById(R.id.mostSoldItemsRecyclerView);
            mostSoldItemsProgressBar = view.findViewById(R.id.mostSoldItemsProgressBar);
            
            topRatedItemsRecyclerView.setLayoutManager(
                  new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
            mostRecentItemsRecyclerView.setLayoutManager(
                  new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
            mostVisitedItemsRecyclerView.setLayoutManager(
                  new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
            mostSoldItemsRecyclerView.setLayoutManager(
                  new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
            new HttpRequest<Item>(getContext(), API.ITEM.GET_TOP_RATED(0),
                  new JSONObject().toString(),
                  new TypeToken<List<Item>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Item> items) {
                        topRatedItemsProgressBar.setVisibility(View.GONE);
                        topRatedItemsRecyclerView.setVisibility(View.VISIBLE);
                        ItemsAdapter itemAdapter = new ItemsAdapter(getContext(), items,
                              R.layout.item_layout, getActivity());
                        topRatedItemsRecyclerView.setAdapter(itemAdapter);
                        
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            
            new HttpRequest<Item>(getContext(), API.ITEM.GET_MOST_RECENT(0),
                  new JSONObject().toString(),
                  new TypeToken<List<Item>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Item> items) {
                        mostRecentItemsProgressBar.setVisibility(View.GONE);
                        mostRecentItemsRecyclerView.setVisibility(View.VISIBLE);
                        ItemsAdapter itemAdapter = new ItemsAdapter(getContext(), items,
                              R.layout.item_layout, getActivity());
                        mostRecentItemsRecyclerView.setAdapter(itemAdapter);
                        
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            new HttpRequest<Item>(getContext(), API.ITEM.GET_MOST_VISITED(0),
                  new JSONObject().toString(),
                  new TypeToken<List<Item>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Item> items) {
                        mostVisitedItemsProgressBar.setVisibility(View.GONE);
                        mostVisitedItemsRecyclerView.setVisibility(View.VISIBLE);
                        ItemsAdapter itemAdapter = new ItemsAdapter(getContext(), items,
                              R.layout.item_layout, getActivity());
                        mostVisitedItemsRecyclerView.setAdapter(itemAdapter);
                        
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            new HttpRequest<Item>(getContext(), API.ITEM.GET_MOST_SOLD(0),
                  new JSONObject().toString(),
                  new TypeToken<List<Item>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Item> items) {
                        mostSoldItemsProgressBar.setVisibility(View.GONE);
                        mostSoldItemsRecyclerView.setVisibility(View.VISIBLE);
                        ItemsAdapter itemAdapter = new ItemsAdapter(getContext(), items,
                              R.layout.item_layout, getActivity());
                        mostSoldItemsRecyclerView.setAdapter(itemAdapter);
                        
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            
            //endregion
            
            //region shops
            topRatedShopsRecyclerView = view.findViewById(R.id.topRatedShopsRecyclerView);
            mostRecentShopsRecyclerView = view.findViewById(R.id.mostRecentShopsRecyclerView);
            mostVisitedShopsRecyclerView = view.findViewById(R.id.mostVisitedShopsRecyclerView);
            mostSoldShopsRecyclerView = view.findViewById(R.id.mostSoldShopsRecyclerView);
            
            topRatedShopsRecyclerView.setLayoutManager(
                  new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
            mostRecentShopsRecyclerView.setLayoutManager(
                  new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
            mostVisitedShopsRecyclerView.setLayoutManager(
                  new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
            mostSoldShopsRecyclerView.setLayoutManager(
                  new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
            
            new HttpRequest<Shop>(getContext(), API.SHOP.GET_TOP_RATED,
                  new JSONObject().toString(),
                  new TypeToken<List<Shop>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Shop> shops) {
                        ShopsAdapter shopAdapter = new ShopsAdapter(getContext(), shops,
                              R.layout.shop_layout, getActivity());
                        topRatedShopsRecyclerView.setAdapter(shopAdapter);
                        
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            new HttpRequest<Shop>(getContext(), API.SHOP.GET_MOST_RECENT,
                  new JSONObject().toString(),
                  new TypeToken<List<Shop>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Shop> shops) {
                        ShopsAdapter shopAdapter = new ShopsAdapter(getContext(), shops,
                              R.layout.shop_layout, getActivity());
                        mostRecentShopsRecyclerView.setAdapter(shopAdapter);
                        
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            new HttpRequest<Shop>(getContext(), API.SHOP.GET_MOST_VISITED,
                  new JSONObject().toString(),
                  new TypeToken<List<Shop>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Shop> shops) {
                        ShopsAdapter shopAdapter = new ShopsAdapter(getContext(), shops,
                              R.layout.shop_layout, getActivity());
                        mostVisitedShopsRecyclerView.setAdapter(shopAdapter);
                        
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            new HttpRequest<Shop>(getContext(), API.SHOP.GET_MOST_SOLD,
                  new JSONObject().toString(),
                  new TypeToken<List<Shop>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Shop> shops) {
                        ShopsAdapter shopAdapter = new ShopsAdapter(getContext(), shops,
                              R.layout.shop_layout, getActivity());
                        mostSoldShopsRecyclerView.setAdapter(shopAdapter);
                        
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            //endregion
            
      }
      
      private void showAds(List<Ad> ads, final boolean online) {
            
            for (final Ad ad : ads) {
                  TextSliderView textSliderView = new TextSliderView(getContext());
                  // initialize a SliderLayout
                  textSliderView
                        .description(ad.getImageUrl())
                        .image(API.IMG_ROOT + ad.getImageUrl())
                        .setScaleType(BaseSliderView.ScaleType.Fit)
                        .setOnSliderClickListener(
                              new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {
                                          if (online) {
                                                switch (ad.getContentType()) {
                                                      case Ad.ITEM: {
                                                            Intent intent = new Intent(
                                                                  getContext(),
                                                                  ItemDetailsActivity.class);
                                                            intent.putExtra("itemId",
                                                                  ad.getIdOfType());
                                                            intent.putExtra("itemNameHeader",
                                                                  "");
                                                            
                                                            List<Pair<View, String>> pairs = new ArrayList<>();
                                                            
                                                            pairs.add(
                                                                  new Pair<View, String>(/*textView,*/
                                                                        new TextView(
                                                                              getContext()),
                                                                        ItemDetailsActivity.VIEW_NAME_HEADER_TITLE));
                                                            pairs.add(new Pair<View, String>(
                                                                  new ImageView(getContext()),
                                                                  ItemDetailsActivity.VIEW_NAME_HEADER_IMAGE));
                                                            
                                                            ActivityOptionsCompat activityOptions = ActivityOptionsCompat
                                                                  .makeSceneTransitionAnimation(
                                                                        (Activity) getContext(),
                                                                        pairs.get(0),
                                                                        pairs.get(1)
                                                                  );
                                                            startActivity(intent,
                                                                  activityOptions.toBundle());
                                                            break;
                                                      }
                                                      case Ad.SHOP: {
                                                            Intent intent = new Intent(
                                                                  getContext(),
                                                                  ShopDetailsActivity.class);
                                                            intent.putExtra("shopId",
                                                                  ad.getIdOfType());
                                                            getContext()
                                                                  .startActivity(intent);
                                                            break;
                                                      }
                                                      case Ad.OFFER: {
                                                            Intent intent = new Intent(
                                                                  getContext(),
                                                                  OfferDetailsActivity.class);
                                                            intent.putExtra("offerId",
                                                                  ad.getIdOfType());
                                                            intent.putExtra("offerImageUrl",
                                                                  ad.getImageUrl());
                                                            getContext()
                                                                  .startActivity(intent);
                                                            break;
                                                      }
                                                }
                                                
                                                Toast.makeText(getContext(), "donna",
                                                      Toast.LENGTH_SHORT).show();
                                          }
                                    }
                              });
                  
                  //add your extra information
                  textSliderView.bundle(new Bundle());
                  textSliderView.getBundle()
                        .putString("extra", ad.getImageUrl());
                  
                  mDemoSlider.addSlider(textSliderView);
            }
            
            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
            mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Left_Bottom);
            mDemoSlider.setCustomAnimation(new DescriptionAnimation());
            mDemoSlider.setDuration(4000);
            mDemoSlider.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
                  @Override
                  public void onPageScrolled(int position, float positionOffset,
                        int positionOffsetPixels) {
                        
                  }
                  
                  @Override
                  public void onPageSelected(int position) {
                  
                  }
                  
                  @Override
                  public void onPageScrollStateChanged(int state) {
                  
                  }
            });
            
      }

//    private Timer timer() {
//        Timer timer = new Timer();
//        final long DELAY_MS = 750;//delay in milliseconds before task is to be executed
//        final long PERIOD_MS = 3000; // time in milliseconds between successive task executions.
//
//        final Handler handler = new Handler();
//        final Runnable Update = new Runnable() {
//            public void run() {
//                if (adsViewPager != null && adsViewPager.getAdapter() != null) {
//                    if (currentPage == adsViewPager.getAdapter().getCount()) {
//                        currentPage = 0;
//                    }
//                    adsViewPager.setCurrentItem(currentPage++, true);
//                }
//            }
//        };
//
//        // This will create a new Thread
//        timer.schedule(new TimerTask() { // task to be scheduled
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, DELAY_MS, PERIOD_MS);
//        return timer;
//    }
}

