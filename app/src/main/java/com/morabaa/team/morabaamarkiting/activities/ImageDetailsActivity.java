package com.morabaa.team.morabaamarkiting.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.github.chrisbanes.photoview.PhotoView;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.squareup.picasso.Picasso;

public class ImageDetailsActivity extends AppCompatActivity {
      
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            
            setContentView(R.layout.activity_image_deatils);
            
            String title = getIntent().getStringExtra("title");
            String path = getIntent().getStringExtra("path");
            
            setTitle(title);
            PhotoView photoView = findViewById(R.id.photo_view);
            String url= API.IMG_ROOT + path;
//            Glide.with(this).load(url).into(photoView);
            Picasso.with(ImageDetailsActivity.this)
                  .load(url)
                  .placeholder(ImageDetailsActivity.this.getResources()
                        .getDrawable(R.drawable.default_item_image))
                  .error(ImageDetailsActivity.this.getResources()
                        .getDrawable(R.drawable.error_item_image))
                  .into(photoView);
      }
      
}