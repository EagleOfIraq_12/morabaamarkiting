package com.morabaa.team.morabaamarkiting.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.model.Order;
import com.morabaa.team.morabaamarkiting.model.OrderItem;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.morabaa.team.morabaamarkiting.utils.ZainCash;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by eagle on 1/22/2018.
 */

public class OrdersAdapter extends RecyclerView.Adapter<OrderViewHolder> /*implements OrderItemsWaitingAdapter.OnOrderItemsCountChangeListener*/ {

    private List<Order> orders;
    private int status;

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
        notifyDataSetChanged();
    }

    private Context ctx;
    private Activity activity;

    public OrdersAdapter(Context ctx, List<Order> orders,
                         Activity activity, int status) {
        this.orders = orders;
        this.ctx = ctx;
        this.activity = activity;
        this.status = status;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.cart_items_group_layout, parent, false);
        return new OrderViewHolder(view);
    }

    @SuppressLint({"StaticFieldLeak", "SetTextI18n"})
    @Override
    public void onBindViewHolder(final OrderViewHolder holder, final int position) {

        try {
            holder.txtShopName.setText(
                    orders.get(position).getShop().getName()
            );


            Picasso.with(ctx)
                    .load(API.IMG_ROOT + orders.get(position).getShop().getImageUrl())
                    .placeholder(ctx.getResources().getDrawable(R.drawable.default_item_image))
                    .error(ctx.getResources().getDrawable(R.drawable.error_item_image))
                    .into(holder.imgShopImage);
        } catch (Exception e) {
        }
        final OrderItemsWaitingAdapter orderItemsWaitingAdapter = new OrderItemsWaitingAdapter(
                ctx, orders.get(position).getOrderItems(),
                activity, status);
        holder.recyclerViewOrderItems.setLayoutManager(
                new LinearLayoutManager(ctx,
                        LinearLayoutManager.VERTICAL,
                        false));
        holder.recyclerViewOrderItems.setAdapter(orderItemsWaitingAdapter);
        orderItemsWaitingAdapter.setOnOrderItemsCountChangeListener(new OrderItemsWaitingAdapter.OnOrderItemsCountChangeListener() {
            @Override
            public void onOrderItemsCountChange(int ordersCount) {
                if (ordersCount > 0) {
                    orders.get(position).setOrderItems(
                            orderItemsWaitingAdapter.getOrderItems()
                    );
                    float tp = 0;
                    for (OrderItem orderItem : orders.get(position).getOrderItems()) {
                        tp += (orderItem.getItem().getPrice() * orderItem.getCount());
                    }
                    orders.get(position).setPrice(tp);
                    holder.txtItemPrice.setText(orders.get(position).getPrice() + " د.ع");
                } else {
                    orders.remove(orders.get(position));
                }
                onOrderChangedListener.onOrderChanged(orders.size());
//                if (orders.size() != 0) {
//                    orders.remove(position);
//                    notifyItemRemoved(position);
//                    notifyItemRangeChanged(position, orders.size());
//                }
            }
        });

        holder.txtItemPrice.setText(orders.get(position).getPrice() + " د.ع");
        holder.txtOrderDate.setText(orders.get(position).getDate());
        switch (status) {
            case Order.Status.WAITING: {
                holder.btnAddToCart.setVisibility(View.VISIBLE);
                holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Order order = orders.get(holder.getAdapterPosition());
                        order.setShopId(order.getShop().getId());
                        order.setUserId(order.getUser().getId());
                        order.setShop(null);
                        order.setUser(null);
                        float tp = 0;
                        for (OrderItem orderItem : order.getOrderItems()) {
                            tp += (orderItem.getItem().getPrice() * orderItem.getCount());
                            orderItem.setItem(null);
                        }
                        order.setPrice(tp);
                        String body = new Gson().toJson(order, Order.class);

                        new HttpRequest<Order>(ctx,
                                API.ORDER.MAKE_ORDER, body, new TypeToken<List<Order>>() {
                        }.getType()) {
                            @Override
                            public void onFinish(List<Order> c) {//
                                orders.remove(orders.get(holder.getAdapterPosition()));
//                            orderItemsWaitingAdapter.removeOrderItem(orderItem);
                                onOrderChangedListener.onOrderChanged(orders.size());
                                long amount = /*order.getPrice()*/ 1000L;
                                long msisdn =/*Long.parseLong(order.getShop().getPhoneNumber())*/
                                        9647814968474L;
                                long orderId = c.get(0).getId();
                                String serviceType = "ecommerce Cart";
                                String merchantId = /*order.getShop().getZainCashWallet()*/
                                        "5ae95054f2ac8b8992220ab2";
                                String secret = /*order.getShop().getZainCashSecret()*/
                                        "$2y$10$ro3vFEOYAsyg32WEDxEg8.bxomtaLPULrMhOLyyWxTuOJbUPWoFHC";


                                ZainCash.pay(ctx, amount, msisdn, orderId, serviceType, merchantId, secret);


//                            if (cartItems.size() != 0) {
//                                cartItems.remove(position);
//                                notifyItemRemoved(position);
//                                notifyItemRangeChanged(position, cartItems.size());
//                                onOrdersCountChangeListener
//                                        .onOrderItemsCountChange(cartItems.size());
//                            }
                            }

                            @Override
                            public void onError(String error) {

                            }
                        };

                    }
                });
                break;
            }
            case Order.Status.NOT_DELIVERED: {
                holder.btnAddToCart.setVisibility(View.GONE);
                break;
            }
            case Order.Status.DELIVERED: {
                holder.btnAddToCart.setVisibility(View.GONE);
                break;
            }

        }
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    private OnOrderChangedListener onOrderChangedListener;


    public void setOnOrderChangedListener(OnOrderChangedListener onOrderChangedListener) {
        this.onOrderChangedListener = onOrderChangedListener;
    }

    public interface OnOrderChangedListener {
        void onOrderChanged(int ordersCount);
    }


}

class OrderViewHolder extends RecyclerView.ViewHolder {

    CardView view;
    ImageView imgShopImage;
    TextView txtShopName;
    RecyclerView recyclerViewOrderItems;
    TextView txtItemPrice;
    TextView txtOrderDate;
    ImageButton btnAddToCart;


    public OrderViewHolder(View itemView) {
        super(itemView);
        view = (CardView) itemView;
        imgShopImage = itemView.findViewById(R.id.imgShopImage);
        txtShopName = itemView.findViewById(R.id.txtShopName);
        recyclerViewOrderItems = itemView.findViewById(R.id.recyclerViewOrderItems);
        txtItemPrice = itemView.findViewById(R.id.txtItemPrice);
        txtOrderDate = itemView.findViewById(R.id.txtOrderDate);
        btnAddToCart = itemView.findViewById(R.id.btnAddToCart);

    }
}