package com.morabaa.team.morabaamarkiting.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.model.ImageUrl;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.squareup.picasso.Picasso;
import java.util.List;

/**
 * Created by eagle on 3/7/2018.
 */

public class ImagePagerAdapter extends PagerAdapter {
      
      Context ctx;
      List<ImageUrl> imageUrls;
      LinearLayout linearLayout;
      
      public ImagePagerAdapter(Context ctx,
            List<ImageUrl> imageUrls, LinearLayout linearLayout) {
            this.ctx = ctx;
            this.imageUrls = imageUrls;
            this.linearLayout = linearLayout;
      }
      
      @Override
      public Object instantiateItem(ViewGroup collection, final int position) {
            LayoutInflater inflater = LayoutInflater.from(ctx);
            RelativeLayout relativeLayout = (RelativeLayout) inflater
                  .inflate(R.layout.image_with_titel_layout,
                        collection, false);
            
            relativeLayout.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        Toast.makeText(ctx, "" + position, Toast.LENGTH_SHORT).show();
                  }
            });
//            int w = SV.ScreenDimensions.getHeight((Activity) ctx);
//            w=400;
            ImageView img = relativeLayout.findViewById(R.id.img);
            TextView txtTitle = relativeLayout.findViewById(R.id.txtTitle);
//            img.setMaxWidth(w);
//            img.setMinimumWidth(w);
            txtTitle.setText(imageUrls.get(position).getUrl());
//            txtTitle.setMaxWidth(w);
//            txtTitle.setMinimumWidth(w);
//            txtTitle.setWidth(w);
            
            Picasso.with(ctx)
                  .load(API.IMG_ROOT + imageUrls.get(position).getUrl())
                  .placeholder(ctx.getResources()
                        .getDrawable(R.drawable.default_item_image))
                  .error(ctx.getResources()
                        .getDrawable(R.drawable.error_item_image))
                  .into(img);
            
            linearLayout.addView(relativeLayout);
//            collection.addView(relativeLayout);
            
            return relativeLayout;
      }
      
      @Override
      public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
      }
      
      @Override
      public int getCount() {
            return imageUrls.size();
      }
      
      @Override
      public boolean isViewFromObject(View view, Object object) {
            return false;
      }
}
