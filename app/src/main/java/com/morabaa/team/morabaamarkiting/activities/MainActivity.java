package com.morabaa.team.morabaamarkiting.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.fragments.CategoriesFragment;
import com.morabaa.team.morabaamarkiting.fragments.HomeFragment;
import com.morabaa.team.morabaamarkiting.fragments.ItemsFragment;
import com.morabaa.team.morabaamarkiting.fragments.ShopsFragment;
import com.morabaa.team.morabaamarkiting.model.Category;
import com.morabaa.team.morabaamarkiting.model.Item;
import com.morabaa.team.morabaamarkiting.model.User;
import com.morabaa.team.morabaamarkiting.services.NotificationActivity;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.DB;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.morabaa.team.morabaamarkiting.utils.SV;
import com.squareup.picasso.Picasso;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity
      implements NavigationView.OnNavigationItemSelectedListener {
      
      public static FragmentManager fragmentManager;
      public static Activity mainActivity;
      public static boolean homeFragment = false;
      public static List<Long> visitedItems = new ArrayList<>();
      public static LinearLayout btnLogOut;
      public static LinearLayout btnLogin;
      public static LinearLayout mainNavLayout;
      public static ImageView imageViewUserImage;
      public static DB db;
      public CardView btnUserEdit;
      public TextView txtUserName;
      public TextView txtUserInfo;
      public LinearLayout btnHomeMenu;
      public LinearLayout btnItemsMenu;
      public LinearLayout btnShopsMenu;
      public LinearLayout btnCategoriesMenu;
      public LinearLayout btnMySubscriptions;
      
      BottomNavigationViewEx navMain;
      private ImageButton btnOpenCart;
      private int backCount = 0;
      
      @Override
      protected void onDestroy() {
            super.onDestroy();
            JSONArray visitedItemsJson = new JSONArray();
            try {
                  visitedItemsJson = new JSONArray(new Gson().toJson(visitedItems));
            } catch (JSONException e) {
                  e.printStackTrace();
            }
            new HttpRequest<Item>(this, API.ITEM.VISITED_ITEMS,
                  visitedItemsJson.toString()) {
                  @Override
                  public void onFinish(List<Item> items) {
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
            
      }
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            fragmentManager = getSupportFragmentManager();
            db = android.arch.persistence.room.Room.databaseBuilder(
                  MainActivity.this, DB.class,
                  "marketing.db"
            ).allowMainThreadQueries()
                  .fallbackToDestructiveMigration()
                  .build();
            FirebaseMessaging.getInstance().subscribeToTopic("All");
            String token = FirebaseInstanceId.getInstance().getToken();
            System.out.println("token: " + token);
            
            NavigationView navigationView = findViewById(R.id.nav_view);
            View header = navigationView.getHeaderView(0);
            btnLogOut = header.findViewById(R.id.btnLogOut);
            btnLogin = header.findViewById(R.id.btnLogin);
            btnOpenCart = findViewById(R.id.btnOpenCart);
            mainNavLayout = header.findViewById(R.id.mainNavLayout);
            imageViewUserImage = header.findViewById(R.id.imageViewUserImage);
            btnUserEdit = header.findViewById(R.id.btnUserEdit);
            txtUserName = header.findViewById(R.id.txtUserName);
            txtUserInfo = header.findViewById(R.id.txtUserInfo);
            
            setNavContents(header);
            System.out.println("myToken\t" +
                  FirebaseInstanceId.getInstance().getToken()
            );
            if (!User.IsLoggedIn(MainActivity.this) ||
                  User.getCurrentUser(this).getName() == null) {
                  btnOpenCart.setVisibility(View.GONE);
                  mainNavLayout.setVisibility(View.GONE);
                  btnLogOut.setVisibility(View.GONE);
                  btnLogin.setVisibility(View.VISIBLE);
                  txtUserInfo.setVisibility(View.GONE);
                  txtUserName.setVisibility(View.GONE);
            } else {
                  btnOpenCart.setVisibility(View.VISIBLE);
                  btnLogOut.setVisibility(View.VISIBLE);
                  btnLogin.setVisibility(View.GONE);
                  mainNavLayout.setVisibility(View.VISIBLE);
                  txtUserInfo.setVisibility(View.VISIBLE);
                  txtUserName.setVisibility(View.VISIBLE);
                  fillNavWithUserInfo(User.getCurrentUser(MainActivity.this));
            }

//        if (User.getCurrentUser(MainActivity.this) == null) {
//            txtUserInfo.setVisibility(View.GONE);
//            txtUserName.setVisibility(View.GONE);
//            mainNavLayout.setVisibility(View.GONE);
//            btnLogOut.setVisibility(View.GONE);
//            btnLogin.setVisibility(View.VISIBLE);
//
//
//        } else {
//            txtUserInfo.setVisibility(View.VISIBLE);
//            txtUserName.setVisibility(View.VISIBLE);
//            mainNavLayout.setVisibility(View.VISIBLE);
//            btnLogOut.setVisibility(View.VISIBLE);
//            btnLogin.setVisibility(View.GONE);
//
//
//        }
            btnLogin.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        intent.putExtra("TAG", true);
                        startActivity(intent);
                  }
            });
            btnUserEdit.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        onBackPressed();
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        intent.putExtra("TAG", false);
                        startActivity(intent);
                  }
            });
            btnLogOut.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        User.signOut(MainActivity.this);
                        mainNavLayout.setVisibility(View.GONE);
                        btnLogOut.setVisibility(View.GONE);
                        btnLogin.setVisibility(View.VISIBLE);
                        btnOpenCart.setVisibility(View.GONE);
                        onBackPressed();
                  }
            });
            mainActivity = this;
            navMain = findViewById(R.id.navMain);
            navMain.enableAnimation(true);
            NotificationActivity.ctx = this;
            
            navMain.enableShiftingMode(true);
            navMain.enableItemShiftingMode(false);
            setTitle("المربع للتسوق");
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            navMain.setOnNavigationItemSelectedListener(
                  new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                              openFragment(item);
                              return false;
                        }
                  });
            
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                  this, drawer, toolbar, R.string.navigation_drawer_open,
                  R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            
            navigationView.setNavigationItemSelectedListener(this);
            
            HomeFragment homeFragment = new HomeFragment();
            fragmentManager.beginTransaction().add(R.id.mainFrame, homeFragment)
                  .addToBackStack(null).commit();
            
            JSONObject jsonObject = new JSONObject();
            new HttpRequest<Category>(this, API.CATEGORY.GET_ALL, jsonObject.toString(),
                  new TypeToken<List<Category>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<Category> categories) {
                        for (Category category : categories) {
                              db.categoryDao().insertCategory(category);
                        }
                  }
                  
                  @Override
                  public void onError(String error) {
                  
                  }
            };
      }
      
      private void setNavContents(View header) {
            btnHomeMenu = header.findViewById(R.id.btnHomeMenu);
            btnItemsMenu = header.findViewById(R.id.btnItemsMenu);
            btnShopsMenu = header.findViewById(R.id.btnShopsMenu);
            btnCategoriesMenu = header.findViewById(R.id.btnCategoriesMenu);
            btnMySubscriptions = header.findViewById(R.id.btnMySubscriptions);
            
            btnHomeMenu.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        HomeFragment homeFragment = new HomeFragment();
                        fragmentManager.beginTransaction()
                              .replace(R.id.mainFrame, homeFragment)
                              .addToBackStack(null).commit();
                        onBackPressed();
                  }
            });
            
            btnItemsMenu.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        ItemsFragment itemsFragment = new ItemsFragment();
                        fragmentManager.beginTransaction()
                              .replace(R.id.mainFrame, itemsFragment)
                              .addToBackStack(null).commit();
                        onBackPressed();
                  }
            });
            
            btnShopsMenu.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        ShopsFragment shopsFragment = new ShopsFragment();
                        fragmentManager.beginTransaction()
                              .replace(R.id.mainFrame, shopsFragment)
                              .addToBackStack(null).commit();
                        onBackPressed();
                  }
            });
            
            btnCategoriesMenu.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        CategoriesFragment categoriesFragment = new CategoriesFragment();
                        fragmentManager.beginTransaction()
                              .replace(R.id.mainFrame, categoriesFragment)
                              .addToBackStack(null).commit();
                        onBackPressed();
                  }
            });
            btnMySubscriptions.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        onBackPressed();
                        Intent intent = new Intent(MainActivity.this, SubscriptionsActivity.class);
                        startActivity(intent);
                  }
            });
      }
      
      @Override
      public void onBackPressed() {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                  drawer.closeDrawer(GravityCompat.START);
            } else if (homeFragment) {
                  if (backCount > 0) {
                        super.onBackPressed();
                        finish();
                  } else {
                        Toast.makeText(mainActivity, "اضغط مره اخرى للخروج.", Toast.LENGTH_SHORT)
                              .show();
                        backCount++;
                        new Handler().postDelayed(new Runnable() {
                              @Override
                              public void run() {
                                    backCount = 0;
                              }
                        }, 3000);
                  }
            } else {
                  HomeFragment homeFragment = new HomeFragment();
                  fragmentManager.beginTransaction().add(R.id.mainFrame, homeFragment)
                        .addToBackStack(null).commit();

//            super.onBackPressed();
            }
      }
      
      @Override
      public void onSaveInstanceState(Bundle savedInstanceState) {
            savedInstanceState.putString("CURRENT_FRAGMENT", SV.CURRENT_FRAGMENT);
            super.onSaveInstanceState(savedInstanceState);
      }
      
      public void openCart(View view) {
            Intent cartIntent = new Intent(MainActivity.this, CartActivity.class);
            startActivity(cartIntent);
      }
      
      
      public void homeMenu(View view) {
            HomeFragment homeFragment = new HomeFragment();
            fragmentManager.beginTransaction()
                  .replace(R.id.mainFrame, homeFragment)
                  .addToBackStack(null).commit();
            onBackPressed();
            
      }
      
      public void itemsMenu(View view) {
            SV.SHOP_ID = 0;
            SV.CATEGORY_ID = 0;
            ItemsFragment itemsFragment = new ItemsFragment();
            fragmentManager.beginTransaction()
                  .replace(R.id.mainFrame, itemsFragment)
                  .addToBackStack(null).commit();
            onBackPressed();
            
      }
      
      public void shopsMenu(View view) {
            
            ShopsFragment shopsFragment = new ShopsFragment();
            fragmentManager.beginTransaction()
                  .replace(R.id.mainFrame, shopsFragment)
                  .addToBackStack(null).commit();
            onBackPressed();
            
      }
      
      public void categoriesMenu(View view) {
            CategoriesFragment categoriesFragment = new CategoriesFragment();
            fragmentManager.beginTransaction()
                  .replace(R.id.mainFrame, categoriesFragment)
                  .addToBackStack(null).commit();

//            Intent mainIntent = new Intent(MainActivity.this, CategoriesActivity.class);
//            startActivity(mainIntent);
//            onBackPressed();
      }
      
      public void fillNavWithUserInfo(User user) {
            try {
                  txtUserName.setText(user.getName());
                  txtUserInfo.setText(
                        user.getPhoneNumber() + "\n" + user.getDistrict() + "\n" + user
                              .getGovernate().getGovernate()
                              + "\n" + user.getAddress());
                  
                  Picasso.with(MainActivity.this)
                        .load(user.getImageUrl())
                        .placeholder(
                              MainActivity.this.getResources()
                                    .getDrawable(R.drawable.default_item_image))
                        .error(MainActivity.this.getResources()
                              .getDrawable(R.drawable.error_item_image))
                        .into(imageViewUserImage);
            } catch (Exception ignored) {
            }
      }
      
      @SuppressWarnings("StatementWithEmptyBody")
      @Override
      public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            // Handle navigation view item clicks here.
            int id = item.getItemId();
            
            if (id == R.id.nav_camera) {
                  // Handle the camera action
            } else if (id == R.id.nav_gallery) {
            
            } else if (id == R.id.nav_slideshow) {
            
            } else if (id == R.id.nav_manage) {
            
            } else if (id == R.id.nav_share) {
            
            } else if (id == R.id.nav_send) {
            
            }
            
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
      }
      
      public void openFragment(MenuItem item) {
            int id = item.getItemId();
            item.setChecked(true);
            if (id == R.id.navigation_home) {
                  HomeFragment homeFragment = new HomeFragment();
                  fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, homeFragment)
                        .addToBackStack(null).commit();
            } else if (id == R.id.navigation_items) {
                  SV.SHOP_ID = 0;
                  SV.CATEGORY_ID = 0;
                  ItemsFragment itemsFragment = new ItemsFragment();
                  fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, itemsFragment)
                        .addToBackStack(null).commit();
            } else if (id == R.id.navigation_shops) {
                  
                  ShopsFragment shopsFragment = new ShopsFragment();
                  fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, shopsFragment)
                        .addToBackStack(null).commit();
            } else if (id == R.id.navigation_categories) {
                  CategoriesFragment categoriesFragment = new CategoriesFragment();
                  fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, categoriesFragment)
                        .addToBackStack(null).commit();
            }
      }
      
      public static class BG_send_notification extends AsyncTask<String, Void, String> {
            
            Context context;
            
            
            public BG_send_notification(Context context) {
                  this.context = context;
            }
            
            @SuppressLint("NewApi")
            @Override
            protected String doInBackground(String... params) {
                  String msg = params[0];
                  String UpdateUser_URL = "https://fcm.googleapis.com/fcm/send";
                  try {
                        URL object = new URL(UpdateUser_URL);
                        
                        HttpURLConnection con = (HttpURLConnection) object.openConnection();
                        
                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        con.setRequestProperty("Accept", "application/json");
                        con.setRequestProperty("Authorization",
                              "Key=AAAAVY0nIew:APA91bGzxm-JApgICrngWeC8zcofin8y-Mjl9qXnYszNy6jxekrc4zSJpMlyK2XD3WlAGbyLNUK72RTc-08mQM0z8FF7cBl_MRI-Bn5DImxJn4xaW4WMwBn9SMOPfKEKCSTvOMxuEKOp");
                        con.setConnectTimeout(3000);
                        con.setRequestMethod("POST");
                        
                        JSONObject json = new JSONObject();
                        json.put("to", "/topics/reports");
                        
                        JSONObject data = new JSONObject();
                        data.put("extra_information", "extra trext ......");
                        
                        json.put("data", data);
                        
                        JSONObject notification = new JSONObject();
                        notification.put("title", "TravSys");
                        notification.put("body", "New Report");
                        notification.put("sound", "default");
                        notification.put("click_action", "extra trext ......");
                        json.put("notification", notification);
                        OutputStream wr = con.getOutputStream();
                        wr.write(json.toString().getBytes("UTF-8"));
                        wr.flush();
                        StringBuilder result = new StringBuilder();
                        int HttpResult = con.getResponseCode();
                        
                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                              BufferedReader br = new BufferedReader(
                                    new InputStreamReader(con.getInputStream(), "UTF-8"));
                              String line = null;
                              while ((line = br.readLine()) != null) {
                                    result.append(line);
                              }
                              br.close();
                        }
                        Log.v("notify", result.toString());
                        return result.toString();
                  } catch (IOException | JSONException e) {
                        return "Error";
                  }
            }
            
            @Override
            protected void onPostExecute(String s) {
                  super.onPostExecute(s);
                  
                  
            }
            
            @Override
            protected void onPreExecute() {
                  super.onPreExecute();
                  
            }
      }
}
