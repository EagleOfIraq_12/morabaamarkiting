package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;

/**
 * Created by eagle on 2/12/2018.
 */
@Entity
public class Cart {
      
      @PrimaryKey
      private long id;
      private long userId;
      @Ignore
      private List<CartItem> cartItems;
      
      public Cart() {
      }
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public long getUserId() {
            return userId;
      }
      
      public void setUserId(long userId) {
            this.userId = userId;
      }
      
      public List<CartItem> getCartItems() {
            return cartItems;
      }
      
      public void setCartItems(List<CartItem> cartItems) {
            this.cartItems = cartItems;
      }
      
      @Dao
      public interface CartDao {
            
            @Query("SELECT * FROM Cart")
            List<Cart> CARTS();
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insertCategory(Cart... carts);
            
            
            @Delete
            void deleteCategory(Cart... carts);
            
            @Update
            void UpdateCategory(Cart... carts);
            
      }
}
