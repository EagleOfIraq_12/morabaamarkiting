package com.morabaa.team.morabaamarkiting.adapters;

import static android.support.v4.content.ContextCompat.startActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.activities.ItemDetailsActivity;
import com.morabaa.team.morabaamarkiting.activities.LoginActivity;
import com.morabaa.team.morabaamarkiting.activities.MainActivity;
import com.morabaa.team.morabaamarkiting.model.CartItem;
import com.morabaa.team.morabaamarkiting.model.CartItem.Status;
import com.morabaa.team.morabaamarkiting.model.Item;
import com.morabaa.team.morabaamarkiting.model.User;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.API.CART;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.morabaa.team.morabaamarkiting.utils.SV.ScreenDimensions;
import com.squareup.picasso.Picasso;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eagle on 1/22/2018.
 */

public class ItemsAdapter extends RecyclerView.Adapter<ItemsViewHolder> {
      
      private List<Item> items;
      private Context ctx;
      private Activity activity;
      private int layout;
      
      public ItemsAdapter(Context ctx, List<Item> items,
            int layout,
            Activity activity) {
            this.items = items;
            this.ctx = ctx;
            this.activity = activity;
            this.layout = layout;
      }
      
      public void clear() {
            if (items != null && items.size() > 0) {
                  items = new ArrayList<>();
                  notifyDataSetChanged();
            }
      }
      
      @Override
      public ItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                  layout, parent, false);
            return new ItemsViewHolder(view);
      }
      
      @SuppressLint({"StaticFieldLeak", "SetTextI18n"})
      @Override
      public void onBindViewHolder(final ItemsViewHolder holder, final int position) {
            
            int h = (int) (ScreenDimensions.getHeight(activity) / 5);
            
            if (layout == R.layout.item_layout_wide) {
                  holder.view.getLayoutParams().height = h;
            }
            
            holder.view.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        MainActivity.visitedItems.add(items.get(position).getId());
                        Intent intent = new Intent(ctx,
                              ItemDetailsActivity.class);
                        intent.putExtra("itemId", items.get(position).getId());
                        intent.putExtra("itemString",
                              new Gson().toJson(items.get(position), Item.class));
                        intent.putExtra("imageUrl", items.get(position).getImageUrl());
                        intent.putExtra("itemNameHeader", items.get(position).getName());
                        
                        ImageView imageView = holder.imgItemImage;
                        TextView textView = holder.txtItemName;
                        List<Pair<View, String>> pairs = new ArrayList<>();
                        
                        pairs.add(new Pair<View, String>(textView,
                              ItemDetailsActivity.VIEW_NAME_HEADER_TITLE));
                        pairs.add(new Pair<View, String>(imageView,
                              ItemDetailsActivity.VIEW_NAME_HEADER_IMAGE));
                        
                        pairs.add(new Pair<View, String>(holder.txtItemPrice,
                              ItemDetailsActivity.VIEW_PRICE_TEXT));
                        
                        pairs.add(new Pair<View, String>(holder.txtItemPriceDiscount,
                              ItemDetailsActivity.VIEW_DISCOUNT_PRICE_TEXT));
                        pairs.add(new Pair<View, String>(holder.txtItemRating,
                              ItemDetailsActivity.VIEW_RATING));
                        
                        Pair<View, String>[] pairs1 = new Pair[pairs.size()];
                        for (int i = 0; i < pairs.size(); i++) {
                              pairs1[i] = pairs.get(i);
                        }
                        ActivityOptionsCompat activityOptions = ActivityOptionsCompat
                              .makeSceneTransitionAnimation(
                                    activity, pairs1
                              );
                        startActivity(ctx, intent, activityOptions.toBundle());
                        
                  }
            });
            
            holder.txtItemName.setText(items.get(position).getName());
            holder.txtItemDescription.setText(items.get(position).getDescription());
            holder.txtItemPrice.setText(
                  String.format("%.2f", items.get(position).getPrice()) + " د.ع");
            holder.txtItemPriceDiscount.setText("");
            if (items.get(position).getOfferItems() != null &&
                  items.get(position).getOfferItems().size() > 0) {
                  holder.txtItemPrice.setTextColor(Color.RED);
                  holder.txtItemPrice.setPaintFlags(holder.txtItemPrice.getPaintFlags()
                        | Paint.STRIKE_THRU_TEXT_FLAG);
                  float priceDiscount = (float) (items.get(position).getPrice() -
                        items.get(position).getPrice() *
                              items.get(position).getOfferItems().get(0).getDiscount() *
                              0.01f);
                  holder.txtItemPriceDiscount.setText(
                        String.format("%.2f", priceDiscount, Locale.US) + " د.ع"
                  );
            } else {
                  holder.txtItemPriceDiscount.setText("");
                  holder.txtItemPrice.setPaintFlags(Paint.HINTING_OFF);
                  holder.txtItemPrice.setTextColor(Color.BLACK);
            }
            
            holder.btnBuy.setVisibility(View.GONE);
            NumberFormat numberFormat = NumberFormat.getNumberInstance();
            numberFormat.setMaximumFractionDigits(2);
            holder.txtItemRating.setText(
                  numberFormat.format(items.get(position).getRating()));
            Picasso.with(ctx)
                  .load(API.IMG_ROOT + items.get(position).getImageUrl())
                  .placeholder(ctx.getResources().getDrawable(R.drawable.default_item_image))
                  .error(ctx.getResources().getDrawable(R.drawable.error_item_image))
                  .into(holder.imgItemImage);
            
            holder.btnBuy.setOnClickListener(new OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        if (User.IsLoggedIn(ctx)) {
                              if (User.checkUserDetails(ctx)) {
                                    buy(position, holder);
                              } else {
                                    Intent intent = new Intent(ctx, LoginActivity.class);
                                    intent.putExtra("TAG", false);
                                    ctx.startActivity(intent);
                              }
                        } else {
                              Intent intent = new Intent(ctx, LoginActivity.class);
                              intent.putExtra("TAG", true);
                              ctx.startActivity(intent);
                        }
                  }
            });
      }
      
      private void pay(long orderId) {
            Intent intent = ctx.getPackageManager()
                  .getLaunchIntentForPackage("com.morabaa.team.zaincash");
            
            if (intent != null && ctx.getPackageManager().resolveActivity(intent, 0) != null) {
                  intent.setAction("android.intent.action.ZAIN_CASH_PAY");
                  intent.setComponent(
                        new ComponentName("com.morabaa.team.zaincash",
                              "com.morabaa.team.zaincash.MainActivity")
                  );
                  intent.putExtra("amount", 5000);
                  intent.putExtra("msisdn", 9647814968474L);
                  intent.putExtra("orderId", orderId);
                  intent.putExtra("serviceType", "ecommerce Cart");
                  intent.putExtra("merchantId", "5ab9135fc125b3f929317520");
                  intent.putExtra("packageName", ctx.getApplicationContext().getPackageName());
                  intent.putExtra("className", MainActivity.class.getCanonicalName());
                  intent.putExtra("secret",
                        "$2y$10$wYDj3ZTfWAEQ/2EmyRMRj.RA3huRb3FkO5gJeHrFeasgtgY9xEBmi");
                  
                  ctx.startActivity(intent);
                  ((Activity) ctx).finish();
            } else {
                  Toast.makeText(ctx,
                        "please install zainCash App",
                        Toast.LENGTH_SHORT).show();
                  final String appPackageName = "mobi.foo.zaincash"; // package name of the app
                  try {
                        ctx.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
                              "market://details?id=" + appPackageName)));
                  } catch (android.content.ActivityNotFoundException anfe) {
                        ctx.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
                              "https://play.google.com/store/apps/details?id="
                                    + appPackageName)));
                  }
            }
      }
      
      private void buy(int position, final ItemsViewHolder holder) {
            CartItem cartItem = new CartItem();
            Item item = new Item();
            item.setId(items.get(position).getId());
            cartItem.setItem(item);
            cartItem.setCount(1);
            cartItem.setUserId(User.getCurrentUser(ctx).getId());
            cartItem.setStatus(Status.WAITING);
            JSONObject jsonObject = null;
            try {
                  jsonObject = new JSONObject(new Gson().toJson(cartItem, CartItem.class));
            } catch (JSONException ignored) {
            
            }
            assert jsonObject != null;
            new HttpRequest<CartItem>(ctx, CART.ADD_ORDER,
                  jsonObject.toString(),
                  new TypeToken<List<CartItem>>() {
                  }.getType()) {
                  @Override
                  public void onFinish(List<CartItem> cartItems) {
                        Snackbar.make(holder.view,
                              "تمَ ارسال الطلب." + " " + cartItems.get(0).getId(),
                              Snackbar.LENGTH_LONG)
                              .setAction("Action", null).show();
                        pay(cartItems.get(0).getId());
                  }
                  
                  @Override
                  public void onError(String error) {
                        Snackbar.make(holder.view, "فشلَ ارسال الطلب.",
                              Snackbar.LENGTH_LONG)
                              .setAction("Action", null).show();
                  }
            };
      }
      
      @Override
      public int getItemCount() {
            return items.size();
      }
      
      public List<Item> getItems() {
            return items;
      }
      
      public void setItems(List<Item> items) {
            this.items = items;
            notifyDataSetChanged();
      }
}

class ItemsViewHolder extends RecyclerView.ViewHolder {
      
      CardView view;
      ImageView imgItemImage;
      TextView txtItemName;
      TextView txtItemDescription;
      TextView txtItemPrice;
      TextView txtItemPriceDiscount;
      TextView txtItemRating;
      
      Button btnBuy;
      
      public ItemsViewHolder(View itemView) {
            super(itemView);
            view = (CardView) itemView;
            imgItemImage = itemView.findViewById(R.id.imgItemImage);
            txtItemName = itemView.findViewById(R.id.txtItemName);
            txtItemDescription = itemView.findViewById(R.id.txtItemDescription);
            txtItemPrice = itemView.findViewById(R.id.txtItemPrice);
            txtItemPriceDiscount = itemView.findViewById(R.id.txtItemPriceDiscount);
            txtItemRating = itemView.findViewById(R.id.txtItemRating);
            btnBuy = itemView.findViewById(R.id.btnBuy);
      }
}
