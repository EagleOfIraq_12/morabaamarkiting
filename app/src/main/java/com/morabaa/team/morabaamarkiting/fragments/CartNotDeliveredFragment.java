package com.morabaa.team.morabaamarkiting.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.adapters.CartItemsNotDeliveredAdapter;
import com.morabaa.team.morabaamarkiting.adapters.OrdersAdapter;
import com.morabaa.team.morabaamarkiting.model.CartItem;
import com.morabaa.team.morabaamarkiting.model.CartItem.Status;
import com.morabaa.team.morabaamarkiting.model.Order;
import com.morabaa.team.morabaamarkiting.model.Shop;
import com.morabaa.team.morabaamarkiting.model.User;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.API.CART;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.morabaa.team.morabaamarkiting.utils.SV;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartNotDeliveredFragment extends Fragment {

    public static final String FRAGMENT_NAME = "CartNotDeliveredFragment";
    private RecyclerView recyclerViewNotDeliveredItems;
    private SwipeRefreshLayout swipeRefresh;
    private OrdersAdapter ordersAdapter;


    public CartNotDeliveredFragment() {
        // Required empty public constructor
        SV.CURRENT_FRAGMENT = FRAGMENT_NAME;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cart_not_delivered, container, false);
    }

    List<CartItem> cartItems;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerViewNotDeliveredItems = view.findViewById(R.id.recyclerViewNotDeliveredItems);
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        swipeRefresh.setRefreshing(true);
        new HttpRequest<Order>(getContext(),
                API.ORDER.GET_ALL(User.getCurrentUser(getContext()).getId(), Order.Status.NOT_DELIVERED)
                , new JSONObject().toString(), new TypeToken<List<Order>>() {
        }.getType()) {
            @Override
            public void onFinish(List<Order> newOrders) {
                swipeRefresh.setRefreshing(false);
                ordersAdapter = new OrdersAdapter(
                        getContext(), newOrders,
                        getActivity(),Order.Status.NOT_DELIVERED);
                recyclerViewNotDeliveredItems.setLayoutManager(
                        new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                recyclerViewNotDeliveredItems.setAdapter(ordersAdapter);
            }

            @Override
            public void onError(String error) {
                swipeRefresh.setRefreshing(false);
            }
        };

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                new HttpRequest<Order>(getContext(),
                        API.ORDER.GET_ALL(User.getCurrentUser(getContext()).getId(), Order.Status.NOT_DELIVERED)
                        , new JSONObject().toString(), new TypeToken<List<Order>>() {
                }.getType()) {
                    @Override
                    public void onFinish(List<Order> newOrders) {
                        swipeRefresh.setRefreshing(false);
                        ordersAdapter = new OrdersAdapter(
                                getContext(), newOrders,
                                getActivity(),Order.Status.NOT_DELIVERED);
                        recyclerViewNotDeliveredItems.setLayoutManager(
                                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerViewNotDeliveredItems.setAdapter(ordersAdapter);
                    }

                    @Override
                    public void onError(String error) {
                        swipeRefresh.setRefreshing(false);
                    }
                };


            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        SV.CURRENT_FRAGMENT = FRAGMENT_NAME;

    }
}
