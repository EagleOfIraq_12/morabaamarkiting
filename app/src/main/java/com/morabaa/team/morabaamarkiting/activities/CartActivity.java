package com.morabaa.team.morabaamarkiting.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.morabaa.team.morabaamarkiting.MyPageTransformer;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.adapters.CartPagerAdapter;
import com.morabaa.team.morabaamarkiting.fragments.CartDeliveredFragment;
import com.morabaa.team.morabaamarkiting.fragments.CartNotDeliveredFragment;
import com.morabaa.team.morabaamarkiting.fragments.CartWaitingFragment;
import com.morabaa.team.morabaamarkiting.utils.SV;

import java.util.ArrayList;
import java.util.List;

public class CartActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private CartPagerAdapter cartPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("سلة التسوق");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SV.CURRENT_FRAGMENT = CartWaitingFragment.FRAGMENT_NAME;

        TabLayout tabLayout = findViewById(R.id.tabs);


        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new CartWaitingFragment());
        fragments.add(new CartNotDeliveredFragment());
        fragments.add(new CartDeliveredFragment());

        cartPagerAdapter = new CartPagerAdapter(getSupportFragmentManager(), fragments);

        mViewPager = findViewById(R.id.container);
        mViewPager.setPageTransformer(true, new MyPageTransformer());
        mViewPager.setAdapter(cartPagerAdapter);
        mViewPager
                .addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void receiveOrder(View view) {
        IntentIntegrator integrator = new IntentIntegrator(CartActivity.this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("امسح رمز QR من البائع لاتمام عملية الاستلام");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(true);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult intentResult = IntentIntegrator
                .parseActivityResult(requestCode, resultCode, data);
        if (intentResult != null) {
            if (intentResult.getContents() == null) {
                Log.d("MainActivity", "Cancelled");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Log.d("MainActivity", "Scanned");
                String contents = intentResult.getContents();
                Toast.makeText(this, contents, Toast.LENGTH_SHORT).show();
            }
        }
    }

}
