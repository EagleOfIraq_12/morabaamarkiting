package com.morabaa.team.morabaamarkiting.fragments;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.morabaa.team.morabaamarkiting.R;

public class BuyDialogFragment extends DialogFragment {

    int mNum;
    private TextView txtQuantity;
    private TextView txtItemName;
    private ImageButton btnPlus;
    private ImageButton btnMinus;
    private TextInputEditText txtNote;
    private Button btnOk;
    String itemName;


    public static BuyDialogFragment newInstance(int num, String itemName) {
        BuyDialogFragment f = new BuyDialogFragment();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        args.putString("itemName", itemName);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments().getInt("num");
        itemName = getArguments().getString("itemName");

        // Pick a style based on the num.
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        switch ((mNum - 1) % 6) {
            case 1:
                style = DialogFragment.STYLE_NO_TITLE;
                break;
            case 2:
                style = DialogFragment.STYLE_NO_FRAME;
                break;
            case 3:
                style = DialogFragment.STYLE_NO_INPUT;
                break;
            case 4:
                style = DialogFragment.STYLE_NORMAL;
                break;
            case 5:
                style = DialogFragment.STYLE_NORMAL;
                break;
            case 6:
                style = DialogFragment.STYLE_NO_TITLE;
                break;
            case 7:
                style = DialogFragment.STYLE_NO_FRAME;
                break;
            case 8:
                style = DialogFragment.STYLE_NORMAL;
                break;
        }
        switch ((mNum - 1) % 6) {
            case 4:
                theme = android.R.style.Theme_Holo;
                break;
            case 5:
                theme = android.R.style.Theme_Holo_Light_Dialog;
                break;
            case 6:
                theme = android.R.style.Theme_Holo_Light;
                break;
            case 7:
                theme = android.R.style.Theme_Holo_Light_Panel;
                break;
            case 8:
                theme = android.R.style.Theme_Holo_Light;
                break;
        }
        setStyle(style, theme);
    }

    View root;
    OnOrderListener onOrderListener;

    public void setOnOrderListener(OnOrderListener onOrderListener) {
        this.onOrderListener = onOrderListener;
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View root = inflater.inflate(R.layout.order_dialog_layout, container, false);
        this.root = root;
        txtQuantity = root.findViewById(R.id.txtQuantity);
        txtItemName = root.findViewById(R.id.txtItemName);
        btnPlus = root.findViewById(R.id.btnPlus);
        btnMinus = root.findViewById(R.id.btnMinus);
        txtNote = root.findViewById(R.id.txtNote);
        btnOk = root.findViewById(R.id.btnOk);
        txtItemName.setText(itemName);
        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int q = Integer.parseInt(txtQuantity.getText().toString());
                txtQuantity.setText(++q + "");
            }
        });

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int q = Integer.parseInt(txtQuantity.getText().toString());
                if (q > 1) {
                    txtQuantity.setText(--q + "");
                }
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOrderListener.OnOrderListener(
                        Integer.parseInt(txtQuantity.getText().toString()),
                        txtNote.getText().toString()
                );
            }
        });
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        return root;
    }


    //    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//        LayoutInflater layoutInflater = (LayoutInflater) getContext()
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////        CardView root = (CardView) layoutInflater.inflate(R.layout.order_dialog_layout, null);
//        @SuppressLint("InflateParams") CardView root = (CardView) (layoutInflater != null ? layoutInflater
//                .inflate(R.layout.order_dialog_layout, null) : null);
//        Dialog dialog = new Dialog(getContext());
//        assert root != null;
//        dialog.setContentView(root);
//        int width = (int) (SV.ScreenDimensions.getWidth(getActivity()) * .8);
//        int height = (int) (SV.ScreenDimensions.getHeight(getActivity()) * .6);
//        root.setMinimumWidth(width);
//        root.setMinimumHeight(height);
//        root.setRadius(20.0f);
////        dialog.getWindow().setLayout(
////                SV.ScreenDimensions.getWidth(getActivity()),
////                SV.ScreenDimensions.getHeight(getActivity()));
//        return dialog;
////        return new AlertDialog.Builder(getActivity())
////                .setTitle("Dialog Title")
////                .setPositiveButton("OK", null)
////                .create();
//    }
    public interface OnOrderListener {
        void OnOrderListener(int count, String note);
    }
}