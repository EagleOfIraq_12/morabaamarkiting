package com.morabaa.team.morabaamarkiting.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.adapters.CartItemsDeliveredAdapter;
import com.morabaa.team.morabaamarkiting.adapters.OrdersAdapter;
import com.morabaa.team.morabaamarkiting.model.CartItem;
import com.morabaa.team.morabaamarkiting.model.CartItem.Status;
import com.morabaa.team.morabaamarkiting.model.Order;
import com.morabaa.team.morabaamarkiting.model.Shop;
import com.morabaa.team.morabaamarkiting.model.User;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.API.CART;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.morabaa.team.morabaamarkiting.utils.SV;

import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartDeliveredFragment extends Fragment {


    public static final String FRAGMENT_NAME = "CartDeliveredFragment";
    List<CartItem> cartItems;
    private RecyclerView recyclerViewDeliveredItems;
    private SwipeRefreshLayout swipeRefresh;
private OrdersAdapter ordersAdapter;
    public CartDeliveredFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cart_delivered, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerViewDeliveredItems = view.findViewById(R.id.recyclerViewDeliveredItems);
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        swipeRefresh.setRefreshing(true);
        new HttpRequest<Order>(getContext(),
                API.ORDER.GET_ALL(User.getCurrentUser(getContext()).getId(), Order.Status.DELIVERED)
                , new JSONObject().toString(), new TypeToken<List<Order>>() {
        }.getType()) {
            @Override
            public void onFinish(List<Order> newOrders) {
                swipeRefresh.setRefreshing(false);
                ordersAdapter = new OrdersAdapter(
                        getContext(), newOrders,
                        getActivity(),Order.Status.NOT_DELIVERED);
                recyclerViewDeliveredItems.setLayoutManager(
                        new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                recyclerViewDeliveredItems.setAdapter(ordersAdapter);
            }

            @Override
            public void onError(String error) {
                swipeRefresh.setRefreshing(false);
            }
        };


        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new HttpRequest<Order>(getContext(),
                        API.ORDER.GET_ALL(User.getCurrentUser(getContext()).getId(), Order.Status.DELIVERED)
                        , new JSONObject().toString(), new TypeToken<List<Order>>() {
                }.getType()) {
                    @Override
                    public void onFinish(List<Order> newOrders) {
                        swipeRefresh.setRefreshing(false);
                        ordersAdapter = new OrdersAdapter(
                                getContext(), newOrders,
                                getActivity(),Order.Status.NOT_DELIVERED);
                        recyclerViewDeliveredItems.setLayoutManager(
                                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerViewDeliveredItems.setAdapter(ordersAdapter);
                    }

                    @Override
                    public void onError(String error) {
                        swipeRefresh.setRefreshing(false);
                    }
                };

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        SV.CURRENT_FRAGMENT = FRAGMENT_NAME;

    }
}
