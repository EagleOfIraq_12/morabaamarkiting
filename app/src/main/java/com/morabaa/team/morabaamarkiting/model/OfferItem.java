package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by eagle on 3/19/2018.
 */
@Entity
public class OfferItem {
      @PrimaryKey
      private long id;
      private long itemId;
      private long offerId;
      @Ignore
      private Offer offer;
      @Ignore
      private Item item;
      private int discount;
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public long getItemId() {
            return itemId;
      }
      
      public void setItemId(long itemId) {
            this.itemId = itemId;
      }
      
      public long getOfferId() {
            return offerId;
      }
      
      public void setOfferId(long offerId) {
            this.offerId = offerId;
      }
      
      public Offer getOffer() {
            return offer;
      }
      
      public void setOffer(Offer offer) {
            this.offer = offer;
      }
      
      public Item getItem() {
            return item;
      }
      
      public void setItem(Item item) {
            this.item = item;
      }
      
      public int getDiscount() {
            return discount;
      }
      
      public void setDiscount(int discount) {
            this.discount = discount;
      }
}
