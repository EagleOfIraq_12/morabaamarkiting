package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;

/**
 * Created by eagle on 2/12/2018.
 */
@Entity
public class CartItem {
      
      @PrimaryKey
      private long id;
      @Ignore
      private Item item;
      private int count;
      private String notes;
      private int status;
      private long userId;
      
      public CartItem() {
      }
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public Item getItem() {
            return item;
      }
      
      public void setItem(Item item) {
            this.item = item;
      }
      
      public int getStatus() {
            return status;
      }
      
      public void setStatus(int status) {
            this.status = status;
      }
      
      public int getCount() {
            return count;
      }
      
      public void setCount(int count) {
            this.count = count;
      }
      
      public String getNotes() {
            return notes;
      }
      
      public void setNotes(String notes) {
            this.notes = notes;
      }
      
      public long getUserId() {
            return userId;
      }
      
      public void setUserId(long userId) {
            this.userId = userId;
      }
      
      @Dao
      public interface CartItemDao {
            
            @Query("SELECT * FROM CartItem")
            List<CartItem> CART_ITEMS();
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insertCategory(CartItem... cartItems);
            
            @Delete
            void deleteCategory(CartItem... cartItems);
            
            @Update
            void UpdateCategory(CartItem... cartItems);
            
      }
      
      public static class Status {
            
            public static final int WAITING = 0;
            public static final int NOT_DELIVERED = 1;
            public static final int DELIVERED = 2;
      }
      
}
