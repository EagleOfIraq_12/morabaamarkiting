package com.morabaa.team.morabaamarkiting.adapters;


import static com.morabaa.team.morabaamarkiting.activities.MainActivity.fragmentManager;
import static com.morabaa.team.morabaamarkiting.activities.MainActivity.mainActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.internal.BottomNavigationItemView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.fragments.ItemsFragment;
import com.morabaa.team.morabaamarkiting.model.Category;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.SV;
import com.squareup.picasso.Picasso;
import java.util.List;

/**
 * Created by eagle on 10/21/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryHolder> {
      
      private List<Category> categories;
      private Context ctx;
      
      public CategoryAdapter(Context ctx, List<Category> categories) {
            this.categories = categories;
            this.ctx = ctx;
            
      }
      
      @Override
      public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            CardView view = (CardView) LayoutInflater.from(parent.getContext()).inflate(
                  R.layout.category_layout_wide, parent, false);
            return new CategoryHolder(view);
      }
      
      @RequiresApi(api = VERSION_CODES.LOLLIPOP)
      @SuppressLint("SetTextI18n")
      @Override
      public void onBindViewHolder(final CategoryHolder holder,
            @SuppressLint("RecyclerView") final int position) {
      
            holder.txtCategoryName.setText(categories.get(position).getName());
            Picasso.with(ctx)
                  .load(API.IMG_ROOT +categories.get(position).getImageUrl())
                  .placeholder(ctx.getResources()
                        .getDrawable(R.drawable.default_item_image))
                  .error(ctx.getResources()
                        .getDrawable(R.drawable.error_item_image))
                  .into(holder.imgCategory);
            holder.view.setOnClickListener(new OnClickListener() {
                  @SuppressLint("RestrictedApi")
                  @Override
                  public void onClick(View view) {
                        ((BottomNavigationItemView) mainActivity
                              .findViewById(R.id.navigation_categories)).setChecked(false);
                        ((BottomNavigationViewEx) mainActivity.findViewById(R.id.navMain))
                              .setSelectedItemId(R.id.navigation_items);
                        
                        ((BottomNavigationItemView) mainActivity
                              .findViewById(R.id.navigation_items)).setChecked(true);
                        
                        ItemsFragment itemsFragment = new ItemsFragment();
                        Bundle bundle = new Bundle();
                        SV.SHOP_ID = 0;
                        SV.CATEGORY_ID = categories.get(position).getId();
                        SV.SEARCH_NAME = categories.get(position).getName();
                        bundle.putLong("categoryId", categories.get(position).getId());
                        itemsFragment.setArguments(bundle);
                        fragmentManager.beginTransaction()
                              .replace(R.id.mainFrame, itemsFragment)
                              .addToBackStack(null).commit();
                  }
            });
      }
      
      @Override
      public int getItemCount() {
            return categories.size();
      }
      
      public List<Category> getCategories() {
            return categories;
      }
      
      public void setCategories(List<Category> categories) {
            this.categories = categories;
            notifyDataSetChanged();
      }
}


class CategoryHolder extends RecyclerView.ViewHolder {
      
      CardView view;
      
      ImageView imgCategory;
      TextView txtCategoryName;
      
      CategoryHolder(View itemView) {
            super(itemView);
            this.view = (CardView) itemView;
      
            imgCategory=view.findViewById(R.id.imgCategory);
            txtCategoryName=view.findViewById(R.id.txtCategoryName);
            
      }
}


