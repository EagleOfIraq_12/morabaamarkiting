package com.morabaa.team.morabaamarkiting.fragments;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v17.leanback.app.DetailsFragment;
import android.support.v17.leanback.widget.Action;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.ClassPresenterSelector;
import android.support.v17.leanback.widget.DetailsOverviewRow;
import android.support.v17.leanback.widget.FullWidthDetailsOverviewRowPresenter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.util.Log;
import com.morabaa.team.morabaamarkiting.DdPresenter;
import com.morabaa.team.morabaamarkiting.DetailsDescriptionPresenter;

/**
 * Created by eagle on 2/11/2018.
 */

public class ItemDetailsFragment extends DetailsFragment {
      private static final String TAG = "ItemDetailsFragment";
      private ArrayObjectAdapter mRowsAdapter;
      
      @SuppressLint("LongLogTag")
      @Override
      public void onCreate(Bundle savedInstanceState) {
            Log.i(TAG, "onCreate");
            super.onCreate(savedInstanceState);
            
            buildDetails();
      }
      
      private void buildDetails() {
            ClassPresenterSelector selector = new ClassPresenterSelector();
            // Attach your media item details presenter to the row presenter:
            FullWidthDetailsOverviewRowPresenter rowPresenter =
                  new FullWidthDetailsOverviewRowPresenter(
                        new DetailsDescriptionPresenter());
            selector.addClassPresenter(DetailsOverviewRow.class, rowPresenter);
            selector.addClassPresenter(ListRow.class,
                  new ListRowPresenter());
            mRowsAdapter = new ArrayObjectAdapter(selector);
           
            Resources res = getActivity().getResources();
            DetailsOverviewRow detailsOverview = new DetailsOverviewRow(
                  "Media Item Details");
            
            // ADD images and action buttons to the details view
//            detailsOverview.setImageDrawable(res.getDrawable(R.drawable.lb_ic_loop_one));
            detailsOverview.addAction(new Action(1, "Buy $9.99"));
            detailsOverview.addAction(new Action(2, "discount  $0.99"));
            mRowsAdapter.add(detailsOverview);
            
            // ADD a Related items row
            ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(
                  new DdPresenter());
            listRowAdapter.add("Media Item 1");
            listRowAdapter.add("Media Item 2");
            listRowAdapter.add("Media Item 3");
            HeaderItem header = new HeaderItem(0, "Related Items");
            mRowsAdapter.add(new ListRow(header, listRowAdapter));
            
            setAdapter(mRowsAdapter);
      }
}
