package com.morabaa.team.morabaamarkiting.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.adapters.OrdersAdapter;
import com.morabaa.team.morabaamarkiting.model.Order;
import com.morabaa.team.morabaamarkiting.model.OrderItem;
import com.morabaa.team.morabaamarkiting.model.User;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import com.morabaa.team.morabaamarkiting.utils.SV;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class CartWaitingFragment extends Fragment {

    public static final String FRAGMENT_NAME = "CartWaitingFragment";

    RecyclerView recyclerViewWaitingItems;
    List<OrderItem> orderItems = new ArrayList<>();
    private SwipeRefreshLayout swipeRefresh;
    private TextView txtNoItems;
    private OrdersAdapter ordersAdapter;

    public CartWaitingFragment() {
        SV.CURRENT_FRAGMENT = FRAGMENT_NAME;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cart_waiting, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        txtNoItems = view.findViewById(R.id.txtNoItems);
        recyclerViewWaitingItems = view.findViewById(R.id.recyclerViewWaitingItems);

        recyclerViewWaitingItems.setLayoutManager(
                new LinearLayoutManager(
                        getContext(), LinearLayoutManager.VERTICAL, false
                ));
        ordersAdapter = new OrdersAdapter(
                getContext(), new ArrayList<Order>(),
                getActivity(), Order.Status.WAITING);
        recyclerViewWaitingItems.setAdapter(ordersAdapter);
        ordersAdapter.setOnOrderChangedListener(new OrdersAdapter.OnOrderChangedListener() {
            @Override
            public void onOrderChanged(int ordersCount) {
                checkAvailability(ordersCount == 0);
            }
        });

        fetchOrderItems();
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchOrderItems();
            }
        });
    }

    private void fetchOrderItems() {
        txtNoItems.setVisibility(View.GONE);
        swipeRefresh.setRefreshing(true);
        new HttpRequest<OrderItem>(getContext(),
                API.ORDER.GET_ORDER_ITEMS(User.getCurrentUser(getContext()).getId())
                , new JSONObject().toString(), new TypeToken<List<OrderItem>>() {
        }.getType()) {
            @Override
            public void onFinish(List<OrderItem> newOrderItems) {
                swipeRefresh.setRefreshing(false);
                orderItems = newOrderItems;
                ordersAdapter.setOrders(generateOrders(newOrderItems));
                checkAvailability(orderItems.size() == 0);
            }

            @Override
            public void onError(String error) {
                swipeRefresh.setRefreshing(false);
            }
        };
    }

    private void checkAvailability(boolean empty) {
        if (empty) {
            recyclerViewWaitingItems.setVisibility(View.GONE);
            txtNoItems.setVisibility(View.VISIBLE);
        } else {
            recyclerViewWaitingItems.setVisibility(View.VISIBLE);
            txtNoItems.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        SV.CURRENT_FRAGMENT = FRAGMENT_NAME;
    }

    public List<Order> generateOrders(List<OrderItem> orderItems) {
        List<Order> lists = new ArrayList<>();
        List<Long> ids = new ArrayList<>();
        for (OrderItem orderItem : orderItems) {
            if (!ids.contains(orderItem.getItem().getShopId())) {
                ids.add(orderItem.getItem().getShopId());
            }
        }
        for (final long id : ids) {
            Order order = new Order();
            order.setOrderItems(new ArrayList<OrderItem>());
            for (OrderItem orderItem : orderItems) {
                if (id == orderItem.getItem().getShopId()) {
                    order.getOrderItems().add(orderItem);
                    order.setShop(orderItem.getItem().getShop());
                    order.setPrice(
                            (float) (order.getPrice() + orderItem.getItem().getPrice() * orderItem.getCount())
                    );
                    order.setUser(User.getCurrentUser(getContext()));
                }
            }
            lists.add(order);
        }
        return lists;
    }
}