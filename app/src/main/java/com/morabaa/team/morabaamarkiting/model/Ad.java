package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.content.Context;
import com.google.gson.reflect.TypeToken;
import com.morabaa.team.morabaamarkiting.utils.API;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;
import java.util.List;
import org.json.JSONObject;

@Entity
public class Ad {
      
      public static final int SHOP = 1;
      public static final int OFFER = 2;
      public static final int ITEM = 3;
      public static final int SHOPS = 4;
      public static final int OFFERS = 5;
      public static final int ITEMS = 6;
      
      
      public static final int PUBLIC = 1;
      public static final int LOCAL = 2;
      public static final int OCCASION = 3;
      
      
      @PrimaryKey
      private long id;
      private long idOfType;
      private int contentType;
      private int type;
      private String imageUrl;
      
      public Ad() {
      }
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public long getIdOfType() {
            return idOfType;
      }
      
      public void setIdOfType(long idOfType) {
            this.idOfType = idOfType;
      }
      
      public int getContentType() {
            return contentType;
      }
      
      public void setContentType(int contentType) {
            this.contentType = contentType;
      }
      
      public int getType() {
            return type;
      }
      
      public void setType(int type) {
            this.type = type;
      }
      
      public String getImageUrl() {
            return imageUrl;
      }
      
      public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
      }
      
      @Dao
      public interface AdDao {
            
            @Query("SELECT * FROM Ad")
            List<Ad> ADS();
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insert(Ad... ads);
            
            @Delete
            void delete(Ad... ads);
            
            @Query("DELETE FROM Ad")
            void deleteAll();
            
            @Query("SELECT COUNT(id) FROM Ad")
            int COUNT();
            
            @Update
            void Update(Ad... ads);
            
      }
      
      public static abstract class Data {
            
            public Data() {
            }
            
            public void getData(Context ctx) {
                  new HttpRequest<Ad>(ctx, API.AD.GET_MAIN,
                        new JSONObject().toString(),
                        new TypeToken<List<Ad>>() {
                        }.getType()) {
                        @Override
                        public void onFinish(List<Ad> ads) {
                              onDataReceived(ads);
                        }
                        
                        @Override
                        public void onError(String error) {
                        
                        }
                  };
            }
            
            public abstract void onDataReceived(List<Ad> ads);
      }
}
