package com.morabaa.team.morabaamarkiting.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import com.morabaa.team.morabaamarkiting.R;
import java.util.ArrayList;
import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class WelcomeActivity extends AppCompatActivity {
      
      public static final int MULTIPLE_PERMISSIONS = 10; // code you want.
      String key;
      String[] permissions =
            {
                  Manifest.permission.CALL_PHONE,
                  Manifest.permission.INTERNET,
                  Manifest.permission.GET_ACCOUNTS,
                  Manifest.permission.READ_CONTACTS,
                  Manifest.permission.ACCESS_COARSE_LOCATION,
                  Manifest.permission.ACCESS_FINE_LOCATION,
            };
      
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                  WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.activity_welcome);
//            try {
//                  Intent intent = getIntent();
//                  key = intent.getExtras().getString("key");
//                  Toast.makeText(this, "key: " + key, Toast.LENGTH_SHORT).show();
//                  if (key != null) {
//                        Intent mainIntent = new Intent(WelcomeActivity.this, MainActivity.class);
//                        startActivity(mainIntent);
//                        finish();
//                  } else {
//                        pay();
//                  }
//            } catch (Exception e) {
//                  pay();
//            }
            
            //      User.signOut(getApplication());
            
            int ip = android.provider.Settings.System.getInt(getContentResolver(),
                  android.provider.Settings.System.WIFI_USE_STATIC_IP, 0);
            System.out.println("WIFI_USE_STATIC_IP "+ip);
            openMain();
//            DB db = Room.databaseBuilder(getApplicationContext(),
//                  DB.class, "market").allowMainThreadQueries().build();
//            db.categoryDao().insertCategory(
//                  new Category() {{
//                        setName("cat1");
//                  }}
//            );
//            Toast.makeText(this, "room: "+db.categoryDao().CATEGORIES().get(0).getName(), Toast.LENGTH_SHORT).show();
      }

      private void openMain() {
            new Handler().postDelayed(new Runnable() {
                  @Override
                  public void run() {
                        Intent mainIntent = new Intent(WelcomeActivity.this,
                              MainActivity.class);
                        startActivity(mainIntent);
                        finish();
                  }
            }, 200);
      }
      
      private boolean checkPermissions() {
            int result;
            List<String> listPermissionsNeeded = new ArrayList<>();
            for (String p : permissions) {
                  result = ContextCompat.checkSelfPermission(WelcomeActivity.this, p);
                  if (result != PackageManager.PERMISSION_GRANTED) {
                        listPermissionsNeeded.add(p);
                  }
            }
            if (!listPermissionsNeeded.isEmpty()) {
                  ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                        MULTIPLE_PERMISSIONS);
                  return false;
            }
            return true;
      }
      
      
      @Override
      public void onRequestPermissionsResult(int requestCode, String permissions[],
            int[] grantResults) {
            switch (requestCode) {
                  case MULTIPLE_PERMISSIONS: {
                        if (grantResults.length > 0
                              && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                              // permissions granted.
                        } else {
                              StringBuilder permission = new StringBuilder();
                              for (String per : permissions) {
                                    permission.append("\n").append(per);
                              }
                              // permissions list of don't granted permission
                        }
                        return;
                  }
            }
      }
}
