package com.morabaa.team.morabaamarkiting.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.morabaa.team.morabaamarkiting.R;
import com.morabaa.team.morabaamarkiting.adapters.SubscriptionsAdapter;
import com.morabaa.team.morabaamarkiting.utils.HttpRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SubscriptionsActivity extends AppCompatActivity {
    RecyclerView subscriptionsRecyclerView;
    LinearLayout layoutNoSubscriptions;
    SubscriptionsAdapter subscriptionsAdapter;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscriptions);
        setTitle("إشتراكاتي");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        subscriptionsRecyclerView = findViewById(R.id.subscriptionsRecyclerView);
        layoutNoSubscriptions = findViewById(R.id.layoutNoSubscriptions);
        subscriptionsRecyclerView.setLayoutManager(
                new LinearLayoutManager(SubscriptionsActivity.this/*, LinearLayoutManager.VERTICAL, true*/)
        );
        subscriptionsAdapter = new SubscriptionsAdapter(SubscriptionsActivity.this, new ArrayList<String>());
        subscriptionsRecyclerView.setAdapter(subscriptionsAdapter);
        new HttpRequest.MySubscriptions(SubscriptionsActivity.this) {
            @Override
            public void onSubscriptionsReceived(List<String> subscriptions) {
                subscriptionsAdapter.setSubscriptions(subscriptions);
                if (subscriptionsAdapter.getItemCount() < 1) {
                    subscriptionsRecyclerView.setVisibility(View.GONE);
                    layoutNoSubscriptions.setVisibility(View.VISIBLE);
                }
            }
        };

    }

}
