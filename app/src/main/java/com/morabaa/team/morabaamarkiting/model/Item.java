package com.morabaa.team.morabaamarkiting.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import com.google.gson.Gson;
import java.util.List;

@Entity
public class Item {
      
      @PrimaryKey
      private long id;
      private String name;
      @Ignore
      private List<ImageUrl> imageUrls;
      private String imageUrl;
      private double price;
      private long shopId;
      private long categoryId;
      @Ignore
      private List<Detail> details;
      private int quantity;
      private String unit;
      private float rating;
      private int rateCont;
      private String description;
      private String date;
      @Ignore
      private List<Comment> comments;
      @Ignore
      private Shop shop;
      @Ignore
      private List<OfferItem> offerItems;
      
      private int saleCount;
      
      public Item() {
      }
      
      public String getImageUrl() {
            return imageUrl;
      }
      
      public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
      }
      
      public long getId() {
            return id;
      }
      
      public void setId(long id) {
            this.id = id;
      }
      
      public String getName() {
            return name;
      }
      
      public void setName(String name) {
            this.name = name;
      }
      
      public List<ImageUrl> getImageUrls() {
            return imageUrls;
      }
      
      public void setImageUrls(List<ImageUrl> imageUrls) {
            this.imageUrls = imageUrls;
      }
      
      public double getPrice() {
            return price;
      }
      
      public void setPrice(double price) {
            this.price = price;
      }
      
      public long getShopId() {
            return shopId;
      }
      
      public void setShopId(long shopId) {
            this.shopId = shopId;
      }
      
      public long getCategoryId() {
            return categoryId;
      }
      
      public void setCategoryId(long categoryId) {
            this.categoryId = categoryId;
      }
      
      public List<Detail> getDetails() {
            return details;
      }
      
      public void setDetails(List<Detail> details) {
            this.details = details;
      }
      
      public int getQuantity() {
            return quantity;
      }
      
      public void setQuantity(int quantity) {
            this.quantity = quantity;
      }
      
      public String getUnit() {
            return unit;
      }
      
      public void setUnit(String unit) {
            this.unit = unit;
      }
      
      public float getRating() {
            return rating;
      }
      
      public void setRating(float rating) {
            this.rating = rating;
      }
      
      public int getRateCont() {
            return rateCont;
      }
      
      public void setRateCont(int rateCont) {
            this.rateCont = rateCont;
      }
      
      public String getDescription() {
            return description;
      }
      
      public void setDescription(String description) {
            this.description = description;
      }
      
      public String getDate() {
            return date;
      }
      
      public void setDate(String date) {
            this.date = date;
      }
      
      public List<Comment> getComments() {
            return comments;
      }
      
      public void setComments(List<Comment> comments) {
            this.comments = comments;
      }
      
      public Shop getShop() {
            return shop;
      }
      
      public void setShop(Shop shop) {
            this.shop = shop;
      }
      
      public List<OfferItem> getOfferItems() {
            return offerItems;
      }
      
      public void setOfferItems(List<OfferItem> offerItems) {
            this.offerItems = offerItems;
      }
      
      public int getSaleCount() {
            return saleCount;
      }
      
      public void setSaleCount(int saleCount) {
            this.saleCount = saleCount;
      }
      
      @Dao
      public interface ItemDao {
            
            @Query("SELECT * FROM Item")
            List<Item> ITEMS();
            
            @Insert(onConflict = OnConflictStrategy.REPLACE)
            void insert(Item... items);
            
            
            @Delete
            void delete(Item... items);
            
            @Update
            void Update(Item... items);
            
      }
      
      public static class SearchItems {
            
            long governateId;
            long shopId;
            long categoryId;
            String searchKey;
            String sortType;
            String orderType;
            List<Long> currentIds;
            
            
            public SearchItems() {
            }
      
            public SearchItems(long governateId, long shopId, long categoryId, String searchKey,
                  String sortType, String orderType, List<Long> currentIds) {
                  this.governateId = governateId;
                  this.shopId = shopId;
                  this.categoryId = categoryId;
                  this.searchKey = searchKey;
                  this.sortType = sortType;
                  this.orderType = orderType;
                  this.currentIds = currentIds;
            }
      
            public long getGovernateId() {
                  return governateId;
            }
            
            public void setGovernateId(long governateId) {
                  this.governateId = governateId;
            }
            
            public long getShopId() {
                  return shopId;
            }
            
            public void setShopId(long shopId) {
                  this.shopId = shopId;
            }
            
            public long getCategoryId() {
                  return categoryId;
            }
            
            public void setCategoryId(long categoryId) {
                  this.categoryId = categoryId;
            }
            
            public String getSearchKey() {
                  return searchKey;
            }
            
            public void setSearchKey(String searchKey) {
                  this.searchKey = searchKey;
            }
            
            public String getSortType() {
                  return sortType;
            }
            
            public void setSortType(String sortType) {
                  this.sortType = sortType;
            }
            
            public String getOrderType() {
                  return orderType;
            }
            
            public void setOrderType(String orderType) {
                  this.orderType = orderType;
            }
            
            public List<Long> getCurrentIds() {
                  return currentIds;
            }
            
            public void setCurrentIds(List<Long> currentIds) {
                  this.currentIds = currentIds;
            }
            
            @Override
            public String toString(){
                  return new Gson().toJson(this,SearchItems.class);
            }
      }
      
}
